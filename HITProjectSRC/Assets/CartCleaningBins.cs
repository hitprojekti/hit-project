﻿
using UnityEngine;

[System.Serializable]
public class CartCleaningBins
{
    public Transform Root;

    private Collider2D c;


    public CartCleaningEnum AcceptedCleanable;



    private MonoCartCleaningMinigame _cartClean;

    public void Initialize(MonoCartCleaningMinigame mono)
    {
        _cartClean = mono;

        c = Root.GetComponent<Collider2D>();
    }

    public bool TryToTake(CartCleaningPart part)
    {
        if(part.CleaningPartType != AcceptedCleanable)
        {
            return false;
        }

        return true;
    }

    public bool WithinBounds(Collider2D pos)
    {
        return c.bounds.Intersects(pos.bounds);
    }

}