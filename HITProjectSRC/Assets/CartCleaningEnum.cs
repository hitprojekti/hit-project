﻿public enum CartCleaningEnum
{
    Trash,
    Textiles,
    CleanYourSelf,
    Desinfection,
    Cart
}