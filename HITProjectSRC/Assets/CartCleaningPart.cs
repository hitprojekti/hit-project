﻿using UnityEngine;

[System.Serializable]
public class CartCleaningPart
{

    public string PartName;

    public Transform MovableObject;

    public CartCleaningEnum CleaningPartType;

    public CartCleaningPartStateEnum State;

    public int[] NeededDone;

    public int CurrentScore;

    public int MaxPoints = 1000;

    [HideInInspector]
    public Collider2D c;

    public PulseEffect Pulse;

    private Vector3 StartPos;

    private MonoCartCleaningMinigame _cartClean;

    public SpriteRenderer Ps;



    public void Initialize(MonoCartCleaningMinigame mono)
    {
        Pulse = new PulseEffect(MovableObject.GetChild(0));
        Pulse.Reset();
        c = MovableObject.GetComponent<Collider2D>();

        StartPos = MovableObject.localPosition;

        _cartClean = mono;

        Ps = MovableObject.GetChild(0).GetChild(0).GetComponent<SpriteRenderer>();
    }

    public void Reset()
    {
        Ps.enabled = false;

        CurrentScore = MaxPoints;
        Pulse.Reset();
    }

    public void SetToStartPosition()
    {
        State = CartCleaningPartStateEnum.Enabled;

        Rigidbody2D rb = MovableObject.GetComponent<Rigidbody2D>();

        rb.isKinematic = true;
        rb.velocity = Vector2.zero;

        MovableObject.localPosition = StartPos;
        MovableObject.localScale = Vector3.one;
        MovableObject.rotation = Quaternion.identity;
    }

    public bool PointWithinBounds(Vector2 v)
    {
        return c.bounds.Contains(v);
    }

    public Vector3 GetToStartPosition()
    {
        return StartPos;
    }

}