﻿using UnityEngine;
using UnityEngine.UI;


public class CleaningTool : Tool
{
    public Toggle Toggle;
    public Image Img;

    private ToolSelector _selector;

    private MonoTool _tool;

    private int _index;

    public CleaningTool(string name, bool selected, bool needed, GameObject go, int i, ToolSelector s) : base(name, selected, needed)
    {
        Toggle = go.transform.GetChild(0).GetComponent<Toggle>();

        _tool = go.GetComponent<MonoTool>();

        _selector = s;

        Img = go.GetComponent<Image>();

        _index = i;
    }

    public void SetSprite(Sprite s)
    {
        Img.sprite = s;
    }

    public void Select(bool b)
    {
        isSelected = b;
      
    }

    public void SelectToggle(bool b)
    {
        Toggle.isOn = b;
    }


    public void Update()
    {

        if (_tool.isOver && _selector.CurrentIndex != _index)
        {
            _selector.CurrentIndex = _index;
            return;
        }

       
    }



}