﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DebugTest : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Invoke("Change", 0.5f);
	}
	
	void Change()
    {
        SceneManager.LoadScene(0);
    }
}
