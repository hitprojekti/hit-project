﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ErgScoreScript : MonoBehaviour {

    public static int CalculateScore(float curPoint, float maxPoint, float minPointUp, float minPointDown)
    {
        int score;
        float floatScore = 0;
        int maxScore = 1000;
        if(curPoint > minPointUp || curPoint < minPointDown)
        {
            score = 0;
        } 
        else
        {
            float lerped = Mathf.Lerp(0, maxPoint, Mathf.Abs(maxPoint - curPoint));
            floatScore = (maxScore * 1.1f) * Mathf.Abs(lerped - 1);
            Debug.Log(floatScore);
            score = (int)floatScore;
        }
        Debug.Log(floatScore);
        score = Mathf.Clamp(score, 0, maxScore);

        return score;
    }

}
