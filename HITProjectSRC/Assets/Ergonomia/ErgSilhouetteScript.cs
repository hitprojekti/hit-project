﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ErgSilhouetteScript : MonoBehaviour {

    public float silhouetteTop;
    public Transform curSilhouette, silhouettePrefab, MaxScorePoint, MinScorePointUp, MinScorePointDown;
    private ErgSliderScript SliderScript;


    private void Awake()
    {
        SliderScript = GameObject.Find("Mop").GetComponent<ErgSliderScript>();
        curSilhouette = (Transform)Instantiate(silhouettePrefab);
        MaxScorePoint = curSilhouette.GetChild(0);
        MinScorePointDown = curSilhouette.GetChild(1);
        MinScorePointUp = curSilhouette.GetChild(2);
        silhouetteTop = curSilhouette.GetComponent<Renderer>().bounds.max.y;
        curSilhouette.localPosition = new Vector3(curSilhouette.position.x + 1, SliderScript.sliderBottom.position.y + (curSilhouette.GetComponent<Renderer>().bounds.size.y / 2) + 1, curSilhouette.position.z);
    }

    public void NewSilhouette()
    {
        Destroy(curSilhouette.gameObject);
        curSilhouette = (Transform)Instantiate(silhouettePrefab);
        float rand = Random.Range(.45f, .65f);
        curSilhouette.localScale = new Vector3(rand, rand, 1);
        curSilhouette.localPosition = new Vector3(curSilhouette.position.x + 1, SliderScript.sliderBottom.position.y + (curSilhouette.GetComponent<Renderer>().bounds.size.y / 2) + 1, curSilhouette.position.z);
        MaxScorePoint = curSilhouette.GetChild(0);
        MinScorePointDown = curSilhouette.GetChild(1);
        MinScorePointUp = curSilhouette.GetChild(2);
        silhouetteTop = curSilhouette.GetComponent<Renderer>().bounds.max.y;
        SliderScript.mopRod.localScale = new Vector3(curSilhouette.localScale.x, curSilhouette.localScale.y * .8f, curSilhouette.localScale.z);
        SliderScript.mopRodLower.localScale = new Vector3(curSilhouette.localScale.x, curSilhouette.localScale.y * 1.2f, curSilhouette.localScale.z);
        SliderScript.mopRag.localScale = curSilhouette.localScale;
        SliderScript.mopRag.transform.position = SliderScript.sliderBottom.position + new Vector3(0, (SliderScript.mopRag.GetComponent<Renderer>().bounds.size.y / 2) + 1, 0);
        SliderScript.mopRodLower.transform.position = new Vector3(SliderScript.mopRag.position.x, SliderScript.mopRag.position.y + (SliderScript.mopRodLower.GetComponent<Renderer>().bounds.size.y / 2), SliderScript.mopRag.position.z);
    }
}
