﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ErgSliderScript : MonoBehaviour {

    private float speed = 1f;
    float silhouetteTop;
    public GameObject SummaryScreen, Stage;
    public Transform mopRod, mopRodLower, mopRag, sliderTop, sliderBottom, doneMark, failMark;
    bool isGoingUp, isSliding, isNumerating, isTutorialPlaying;
    int score, totalScore, round = 0, maxRound = 5;
    int[] scoreArray = new int[5];
    public Transform[] StageIndicators = new Transform[5];
    private ScoreManagerMono ScrManagerMono;
    private Manager manager;
    private ErgSilhouetteScript SilhouetteScript;
    private ErgUIScoreTextScript ScoreTextScript;

    private UniversalTutorialScript Univtut;

    private void Start()
    {
        Univtut = GameObject.Find("TutorialButton").GetComponent<UniversalTutorialScript>();
        SummaryScreen.SetActive(false);
        ScrManagerMono = GameObject.Find("_ScoreManager_").GetComponent<ScoreManagerMono>();
        SilhouetteScript = GameObject.Find("Silhouette").GetComponent<ErgSilhouetteScript>();
        ScoreTextScript = GameObject.Find("ScoreTextRoot").GetComponent<ErgUIScoreTextScript>();
        manager = GameObject.Find("_ScoreManager_").GetComponent<Manager>();
        isSliding = true;
        isTutorialPlaying = true;
        
        mopRod.localScale = new Vector3(SilhouetteScript.curSilhouette.localScale.x, SilhouetteScript.curSilhouette.localScale.y * .8f, SilhouetteScript.curSilhouette.localScale.z);
        mopRodLower.localScale = new Vector3(SilhouetteScript.curSilhouette.localScale.x, SilhouetteScript.curSilhouette.localScale.y * 1.2f, SilhouetteScript.curSilhouette.localScale.z);

        mopRag.transform.position = sliderBottom.position + new Vector3(0, mopRag.GetComponent<Renderer>().bounds.size.y / 2 + 1, 0);
        mopRodLower.transform.position = new Vector3(mopRag.position.x, mopRag.position.y + (mopRodLower.GetComponent<Renderer>().bounds.size.y / 2), mopRag.position.z);

    }

    private void Update()
    {
      

        if (Input.GetMouseButtonDown(0))
        {
            if (Univtut.inTutorial)
            {      
                Univtut.Disable();
                return;
            }
        }
        if (ScrManagerMono.GetComponent<Manager>().Paused || ScrManagerMono.GetComponent<Manager>().Hovering || Univtut.inTutorial) { return; }
        MoveSlider();
        if (Input.GetMouseButtonDown(0) && isSliding)
        {
            if (!Univtut.inTutorial)
            {
                StopSlider();
            }
        }

        if (!isSliding && round < maxRound && !isNumerating)
        {
            StopCoroutine("StartSlider");
            StartCoroutine("StartSlider");
        }
    }

    void MoveSlider()
    {
        if (isGoingUp && isSliding)
        {
            sliderTop.transform.position += new Vector3(0, speed * Time.deltaTime, 0);
            if(sliderTop.transform.position.y > (SilhouetteScript.silhouetteTop))
            {
                isGoingUp = false;
            }
        }
        if(!isGoingUp && isSliding)
        {
            sliderTop.transform.position += new Vector3(0, -speed * Time.deltaTime, 0);
            if(sliderTop.transform.position.y < SilhouetteScript.silhouettePrefab.transform.position.y * SilhouetteScript.curSilhouette.localScale.y || mopRod.GetComponent<Renderer>().bounds.min.y < mopRag.position.y +.5f)
            {
                isGoingUp = true;
            }
        }
        mopRod.transform.position = sliderTop.transform.position - new Vector3(0, (mopRod.GetComponent<Renderer>().bounds.size.y / 2), 0);
    }

    void StopSlider()
    {
        isSliding = false;
        isNumerating = false;
        score = ErgScoreScript.CalculateScore((mopRod.position.y + mopRod.GetComponent<SpriteRenderer>().bounds.extents.y), SilhouetteScript.MaxScorePoint.position.y, SilhouetteScript.MinScorePointUp.position.y, SilhouetteScript.MinScorePointDown.position.y);
        scoreArray[round] = score;
        ScoreTextScript.PlayScoreAnimation(score.ToString());
        if (scoreArray[round] > 0)
        {
            Instantiate(doneMark, StageIndicators[round].position, Quaternion.identity).transform.parent = Stage.transform;
        }
        else if (scoreArray[round] == 0)
        {
            Instantiate(failMark, StageIndicators[round].position, Quaternion.identity).transform.parent = Stage.transform;
        }
        round++;
        if (round == maxRound)
        {
            for (int i = 0; i < scoreArray.Length; i++)
            {
                totalScore += scoreArray[i];
            }
            Debug.Log(totalScore);
            ScrManagerMono.GiveScore(totalScore, MiniGameEnum.ERGONOMY);
            CancelInvoke("OpenSummary");
            Invoke("OpenSummary", 2.0f);
        }

    }

    void StopGame()
    {
        SceneManagerHit.ChangeScene(0);
    }

    IEnumerator StartSlider()
    {
        isNumerating = true;
        yield return new WaitForSeconds(2f);
        isSliding = true;
        SilhouetteScript.NewSilhouette();
    }

    void OpenSummary()
    {
        manager.Pause.Play("PauseMenuOutInstant_Empty", true);
        ScoreTextScript.SummaryScreen(scoreArray);
        SummaryScreen.SetActive(true);
        Stage.SetActive(false);
    }
}
