﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ErgTutorialScript : MonoBehaviour {

    GameObject silhouette, mop, stage;

	// Use this for initialization
	void Start () {
        silhouette = GameObject.FindGameObjectWithTag("Silhouette");
        mop = GameObject.Find("Mop");
        stage = GameObject.Find("Stage");
        silhouette.SetActive(false);
        for (int i = 0; i < mop.transform.childCount; i++)
        {
            mop.transform.GetChild(i).gameObject.SetActive(false);
        }
        stage.SetActive(false);
    }
	
	public void Disable()
    {
        silhouette.SetActive(true);
        for (int i = 0; i < mop.transform.childCount; i++)
        {
            mop.transform.GetChild(i).gameObject.SetActive(true);
        }

        stage.SetActive(true);
        gameObject.SetActive(false);
    }
}
