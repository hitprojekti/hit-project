﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ErgUIScoreTextScript : MonoBehaviour {

    public Text ScoreText, SummaryText1, SummaryText2;
    public Animation ScoreAnim;

    public void PlayScoreAnimation(string score)
    {
        Vector2 vec = Input.mousePosition + new Vector3(0, 100, 0);
        ScoreText.text = score;
        transform.position = vec;
        ScoreAnim.Stop();
        ScoreAnim.Play("ScoreText");
    }

    public void SummaryScreen(int[] scores)
    {
        SummaryText1.text = string.Format("1st\n2nd\n3rd\n4th\n5th\n\nTotal");
        SummaryText2.text = string.Format("{0}\n{1}\n{2}\n{3}\n{4}\n\n{5}", scores[0], scores[1], scores[2], scores[3], scores[4], scores.Sum());
    }
}
