﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Flash
{
    public CanvasGroup flashWhite;

    public bool stillFading;
  
    float multiplier = 3;
    Image img;
    float toA;
    MonoObject mono;

    bool useTimeScale;

    IEnumerator Flashings;

    public Flash(CanvasGroup cvg, MonoObject monon)
    {
        stillFading = false;
        mono = monon;
        flashWhite = cvg;
        img = flashWhite.transform.GetChild(0).GetComponent<Image>();
        flashWhite.gameObject.SetActive(false);
    }

    public void StartFlash(float startAlpha, float multiplier2, float toAlpha, Color color, bool useTimeScale)
    {
        stillFading = true;
        flashWhite.gameObject.SetActive(true);
        this.useTimeScale = useTimeScale;
        multiplier = multiplier2;
        img.color = color;
        toA = toAlpha;
        flashWhite.alpha = startAlpha;
        StartFlashings();

    }

    IEnumerator Flashing()
    {
       

        while (true)
        {
            yield return null;
            if (flashWhite.alpha > toA)
            {
                if (useTimeScale) { 
                flashWhite.alpha -= Time.deltaTime * multiplier;
                }else
                {
                    flashWhite.alpha -= Time.unscaledDeltaTime * multiplier;
                }
                if (flashWhite.alpha <= toA)
                {
                    flashWhite.alpha = toA;
                    if (flashWhite.alpha <= 0)
                    {
                        flashWhite.gameObject.SetActive(false);
                    }
                    stillFading = false;
                    yield break;
                }
            }
            else if (flashWhite.alpha < toA)
            {
                if (useTimeScale) { 
                flashWhite.alpha += Time.deltaTime * multiplier;
                }else
                {
                    flashWhite.alpha += Time.unscaledDeltaTime* multiplier;

                }
                if (flashWhite.alpha >= toA)
                {
                    flashWhite.alpha = toA;

                    stillFading = false;
                    yield break;
                }
            }
        }

       
    }

    private void StopFlashig()
    {
        if(Flashings == null) { return; }

        mono.StopCoroutine(Flashings);

    }

    private void StartFlashings()
    {
        StopFlashig();
        Flashings = Flashing();

        mono.StartCoroutine(Flashings);
    }


}