﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoObject {

    public UnscaledTimeAnimator Pause;

    public ScoreManagerMono Managers;

    public bool Paused;

    public bool Hovering;

    void Awake()
    {
        SetHover(false);
        Paused = false;
        Pause.Play("PauseMenuOutInstant", true, true);
    }

    public void SetHover(bool b)
    {
        Hovering = b;
    }

	public void CloseGame()
    {

        Time.timeScale = 1;
        SceneManagerHit.ChangeScene(0);
    }

    public void PauseGame()
    {
        Paused = true;
        Time.timeScale = 0;
        Pause.Play("PauseMenuIn", false);
    }

    public void UnPauseGame()
    {
        Paused = false;
        Time.timeScale = 1;
        Pause.Play("PauseMenuOut_Anim", true);
    }

    public void RestartGame()
    {
        Time.timeScale = 1;

        UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
    }
}
