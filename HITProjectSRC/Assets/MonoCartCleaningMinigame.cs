﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MonoCartCleaningMinigame : MonoObject {

    public CartCleaningMiniGame Minigame;

    public CartCleaningPart[] CleanableParts;

    public CartCleaningBins[] CleaningBins;

    public Transform Root;

    public Text ScoreText;
    public Animation ScoreAnim;
    public Animation Buckets;

    public GameObject UI;


    public Text[] Texts;

    public Text Total;

    public CanvasGroup Cvg;

    public Transform[] BucketsObj;

    public Transform[] BucketRoots;

    private int[] _indexes;

    

    public override void Init()
    {

        _indexes = new int[BucketRoots.Length];


        for (int i = 0; i < _indexes.Length; i++)
        {
            _indexes[i] = i;
        }

        base.Init();


        for (int i = 0; i < CleanableParts.Length; i++)
        {
            CleanableParts[i].Initialize(this);
        }


        for (int i = 0; i < CleaningBins.Length; i++)
        {
            CleaningBins[i].Initialize(this);
        }
    }

    public void RandomizeBucketPos()
    {
        List<int> pos = _indexes.ToList();

        int index;
 
        for (int i = 0; i < BucketsObj.Length; i++)
        {
            index = Random.Range(0, pos.Count);

            BucketsObj[i].SetParent(BucketRoots[pos[index]]);
            BucketsObj[i].localPosition = Vector2.zero;
            pos.RemoveAt(index);
        }
    }

    public void PlayAnimation(string s)
    {
        Buckets.Stop();
        Buckets.Play(s);
    }

    public void Initialize()
    {
    

        Minigame = (CartCleaningMiniGame)GameManager.Instance.MiniGameManager.GetMiniGame(MiniGameEnum.CART_CLEANING);

    }

    public void Close()
    {
       
        Minigame.CloseGame();
    }


    public void SetScore(string str, Vector2 pos)
    {
        ScoreAnim.transform.position = pos;

        ScoreText.text = str;

        ScoreAnim.Stop();
        ScoreAnim.Play("ScoreText");
    }

    public bool IsCleanableGrappable(CartCleaningPart part)
    {
        if(part.NeededDone.Length <= 0)
        {
            return (part.State == CartCleaningPartStateEnum.Enabled && Minigame.CurrentCartCleaningPart != part) ? true : false;

        }

        for (int i = 0; i < part.NeededDone.Length; i++)
        {
            if(CleanableParts[part.NeededDone[i]].State != CartCleaningPartStateEnum.Done)
            {
                return false;
            }
        }


        return (part.State == CartCleaningPartStateEnum.Enabled && Minigame.CurrentCartCleaningPart != part) ? true : false;

    }

    public void MovePartToPlace(CartCleaningPart part)
    {
        if(!gameObject.activeSelf) { return; }

        StartCoroutine(MovePart(part));
    }

    public void MovePartToBin(CartCleaningPart part, CartCleaningBins bin)
    {
        if (!gameObject.activeSelf) { return; }

        StartCoroutine(MovePartToBins(part, bin));
    }


    private IEnumerator MovePartToBins(CartCleaningPart part, CartCleaningBins bin)
    {
        float multiplier = 4;
        float _t = 0;

        part.MovableObject.GetComponent<Rigidbody2D>().isKinematic = true;
        part.MovableObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;

        Transform tr = part.MovableObject;
        Transform ttr = bin.Root;
        Rigidbody2D rb = tr.GetComponent<Rigidbody2D>();

        _t = 0;
        rb.isKinematic = true;
        rb.velocity = Vector2.zero;

        tr.SetParent(Root);


        while (_t < 1)
        {

            tr.rotation = Quaternion.Lerp(tr.rotation, Quaternion.identity, _t);
            tr.position = Vector3.Lerp(tr.position, ttr.position, _t);
            tr.localScale = Vector3.Lerp(tr.localScale, Vector3.zero, _t);

            _t += Time.deltaTime * multiplier;
            yield return null;
        }

        yield return null;
     
    }

    private IEnumerator MovePart(CartCleaningPart part)
    {
        float multiplier = 3;
        float _t = 0;
        part.State = CartCleaningPartStateEnum.Disabled;


   
        Transform tr = part.MovableObject;
        Rigidbody2D rb = tr.GetComponent<Rigidbody2D>();

        _t = 0;
        rb.isKinematic = true;
        rb.velocity = Vector2.zero;

        tr.SetParent(Root);

        
        while(_t < 1)
        {

            tr.rotation = Quaternion.Lerp(tr.rotation, Quaternion.identity, _t);
            tr.localPosition = Vector3.Lerp(tr.localPosition, part.GetToStartPosition(), _t);
            tr.localScale = Vector3.Lerp(tr.localScale, Vector3.one, _t);

            _t += Time.deltaTime * multiplier;
            yield return null;
        }

        yield return null;
        part.SetToStartPosition();
    }


    public void ResetMinigame()
    {
        MoveAllPartsToStartPositions();

        for (int i = 0; i < CleanableParts.Length; i++)
        {
            CleanableParts[i].Reset();
        }


    }


    public void MoveAllPartsToStartPositions()
    {
        for (int i = 0; i < CleanableParts.Length; i++)
        {
            CleanableParts[i].SetToStartPosition();
        }

    }

    

}
