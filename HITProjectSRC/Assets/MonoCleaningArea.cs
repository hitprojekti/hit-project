﻿using UnityEngine;

public class MonoCleaningArea : MonoObject
{
    public MobileButton Button;

    public MonoFloorCleaning FloorClean;

    public int index;
    public bool IsDone
    {
        get
        {
            return _isDone;
        }

        set
        {
            _isDone = value;

            Button.Interactable = !IsDone;

            UpdateCheckMark();
        }
    }
    public Animation Animation;

    private bool _isDone;   

    public void ToggleCleaningArea(bool b)
    {
        IsDone = b;
    }

    public void Select()
    {
        FloorClean.SelectArea(index);
    }

    public void UpdateCheckMark()
    {
        if (IsDone)
        {
            Animation.Stop();
            Animation.Play("Checkmark_In");

            return;
        }

        Animation.Stop();
        Animation.Play("Checkmark_Out_Instant");
    }
}