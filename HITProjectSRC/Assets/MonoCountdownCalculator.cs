﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MonoCountdownCalculator : MonoObject {

    public Text CountdownText;

    public void SetText(string input)
    {
        CountdownText.text = input;
    }
}
