﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonoFloorCleaning : MonoObject {

    public MonoCleaningArea[] Buttons;
    public FloorCleaningMiniGame FloorCleaningMiniGame;

    public CanvasGroup MainCanvasGroup;


    public void Setup(FloorCleaningMiniGame mingame)
    {
        FloorCleaningMiniGame = mingame;
    }

    public void ToggleButton(bool b, int index)
    {
        Buttons[index].Button.Interactable = b;
    }
    
    public void Starting()
    {
        for (int i = 0; i < Buttons.Length; i++)
        {
            Buttons[i].ToggleCleaningArea(false);
        }
    }


    public void SelectArea(int index)
    {
        MainCanvasGroup.interactable = false;
        MainCanvasGroup.blocksRaycasts = false;

  
    }

    
}
