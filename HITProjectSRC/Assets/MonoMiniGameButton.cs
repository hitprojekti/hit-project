﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class MonoMiniGameButton : MonoObject {

    public MiniGameEnum MiniGameEnum;
    public Text ScoreText;
    public Text NameText;

    private MiniGame minigame;
    private GameManager Manager;

    public override void Init()
    {
        base.Init();
        Manager = GameManager.Instance;

    }

    public void InitialSetup()
    {
        minigame = Manager.MiniGameManager.GetMiniGame(MiniGameEnum);
        NameText.text = minigame.MiniGameName;
        UpdateScores();

    }

    public void UpdateScores()
    {
        NumberFormatInfo f = new NumberFormatInfo { NumberGroupSeparator = " " };
        string totScore = Manager.ProfileManager.Profile.TotalScore[(int)MiniGameEnum].ToString("n", f);

        totScore = totScore.Remove(totScore.LastIndexOf("."));

        ScoreText.text = totScore;
    }

}
