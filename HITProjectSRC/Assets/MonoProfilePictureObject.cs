﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MonoProfilePictureObject : MonoObject {

    public Image PictureImage;
    public Image Remove;

    public Button ButtonRemove;

    public Sprite Spr;
    public string Sprite;

    public GameManager Manager;

    public override void Init()
    {
        base.Init();
        Manager = GameManager.Instance;
  
    }

    public override void Dispose()
    {
        PictureImage.sprite = null;
        Sprite = "";
        Spr = null;
        base.Dispose();
    }

    public void SetSprite(ProfileImage img)
    {
        PictureImage.sprite = img.ActualSprite;
        Spr = img.ActualSprite;
        Sprite = img.Path;

        SetRemoving(img.CanBeRemoved);
    }

    public void SetRemoving(bool removable)
    {
        ButtonRemove.interactable = removable;
        Remove.enabled = removable;
    }

}
