﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonoTool : MonoObject {

    public bool IsUsed;

    public ToolSelector Selector;
    public bool isOver;
  
    public void Start()
    {
        Selector = GameObject.Find("OverviewPhase").GetComponent<ToolSelector>();
    }

    public void OnMouseOVer(bool b)
    {
        isOver = b;
    }

    public void Select(bool b)
    {
       if(Selector == null) { return; }
        Selector.Select(b);
    }
}
