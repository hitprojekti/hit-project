﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonoTrashTile : MonoObject {

    public SpriteRenderer Renderer;

    private Color c;
    private GameManager Manager;

    private MoppingTrash AttachedTile;

    public override void Init()
    {
        base.Init();
        Manager = GameManager.Instance;
        c = Color.white;
    }

    public void SetIntegrity()
    {
        if(AttachedTile == null) { return; }

        float alpha = Mathf.Round(AttachedTile.Integrity * 4) / 4;
        c.a = alpha;
        Renderer.color = c;

        if(c.a <= 0)
        {
            Renderer.enabled = false;
        }

    }

    public void RandomizeTransform()
    {
        float zRot = Mathf.Round(Random.Range(0, 360) * 8) / 8;

        bool flipX = Random.Range(0, 2) == 1;
        bool flipY = Random.Range(0, 2) == 1;

        Renderer.flipX = flipX;
        Renderer.flipY = flipY;

        transform.rotation = Quaternion.Euler(0, 0, zRot);
    }

    public void SetSprite(string atlas, string sprite)
    {
        Sprite sprt = Manager.SpriteBank.GetSprite(atlas, sprite);
        Renderer.sprite = sprt;

        Renderer.enabled = true;


    }

    public void Setup(MoppingTrash t)
    {
        AttachedTile = t;

        AttachedTile.OnIntegrityChange += SetIntegrity;

    }

    private void OnBecameInvisible()
    {
        Renderer.enabled = false;
    }

    private void OnBecameVisible()
    {
        if (c.a > 0)
        {
            Renderer.enabled = true;
        }

    }

    public override void Dispose()
    {

        if(AttachedTile != null)
        {
            AttachedTile.OnIntegrityChange -= SetIntegrity;
            AttachedTile = null;
        }
    

        c.a = 1;
        Renderer.color = c;

        Renderer.flipX = false;
        Renderer.flipY = false;

        base.Dispose();
    }
}
