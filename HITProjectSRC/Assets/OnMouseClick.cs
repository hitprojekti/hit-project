﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnMouseClick : MonoObject {

    public UnityEvent Event;

    public new void OnMouseDown()
    {
        Event.Invoke();
    }
}
