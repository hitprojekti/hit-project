﻿using UnityEngine;

public struct ProfileImage
{
    public Sprite ActualSprite;
    public string Path;

    public bool HasEntry;
    public bool CanBeRemoved;

    public ProfileImage(string p, Sprite s, bool b, bool bb)
    {
        HasEntry = b;
        ActualSprite = s;
        Path = p;
        CanBeRemoved = bb;
    }
}