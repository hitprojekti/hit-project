﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressItemMono : MonoObject {

	public ProgressStateEnum State
    {
        get
        {
            return _state;
        }

        set
        {
            _state = value;

            UpdateSprite();
        }
    }

    private ProgressStateEnum _state;

    public Image img;
    public Sprite[] sprts;

    public void UpdateSprite()
    {
        img.sprite = sprts[(int)State];
    }

}
