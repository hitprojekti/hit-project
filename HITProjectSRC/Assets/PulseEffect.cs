﻿using System.Collections;
using UnityEngine;

public class PulseEffect
{
    public Transform Root;
    public bool Pulsing;
    private Vector3 _size;

    private Vector3 defaultSize = Vector3.one;

    private GameManager _manager;

    private IEnumerator Fader;

    public PulseEffect(Transform t)
    {
        Root = t;
        _manager = GameManager.Instance;

        Reset();
    }

    public void FadeToDefault(float spd)
    {
        Pulsing = false;
        if (Fader != null)
        {
            _manager.MonoManager.StopCoroutine(Fader);
        }

        Fader = FadeDefault(spd);

        _manager.MonoManager.StartCoroutine(Fader);
    }

    public void Reset()
    {
        Pulsing = false;
        if (Fader != null)
        {
            _manager.MonoManager.StopCoroutine(Fader);
        }

        Root.localScale = defaultSize;
    }

    public void StartPulse(float max, float spd)
    {
        if (Pulsing) { return; }
        if (Fader != null)
        {
            _manager.MonoManager.StopCoroutine(Fader);
        }

        Fader = Pulse(max,spd);

        _manager.MonoManager.StartCoroutine(Fader);
    }

    public IEnumerator FadeDefault(float speed)
    {
        Vector3 st = Root.localScale;
        float t = 0;
        while(t < 1.0f)
        {
            Root.localScale = Vector3.Lerp(st, defaultSize, t);
            t += Time.deltaTime * speed;
            yield return null;
        }
        Root.localScale = defaultSize;
    }

    public IEnumerator Pulse(float size, float spd)
    {
        Pulsing = true;
        float siz = 0;
        float randomTime = Random.Range(0, 360.0f);
        _size = Vector3.one;

        while (true)
        {
            siz = Mathf.PingPong((Time.time + randomTime) * 0.5f, spd- 0.95f) + 0.95f;
            

            Root.localScale = Vector3.Lerp(Root.localScale, (_size * siz), Time.time);
            yield return null;
        }
    }
}