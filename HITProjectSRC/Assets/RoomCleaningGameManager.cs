﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class RoomCleaningGameManager : MonoObject {

    public float baseAlpha = 0.6f;
    private Color _Color;

    public int[] Points;

    public SpriteRenderer[] Done;

    public Collider2D[] Collision;

    public SpriteRenderer[] RendererOutline;

    public int[] Order;

    public int[] RequiredItem;

    public int currentIndex, Index;

    public Manager Manager;

    public GameObject Summary;

    public Text Total;

    public Text[] Summaries;

    public Text Selection;

    public ToolSelector Tools;

    public int CurrentItem
    {
        get
        {
            return _itm;
        }

        set
        {
            _itm = value;

            for (int i = 0; i < SelectedItems.Length; i++)
            {
                if (i == _itm)
                {
                    SelectedItems[i].enabled = true;
                    continue;
                }
                SelectedItems[i].enabled = false;
            }
        }
    }

    public Image[] SelectedItems;

    public ZoomScript WCZoom;

    private int _itm;

    public void Start()
    {

        _Color = Color.yellow;
        _Color.a = baseAlpha;

        if (RendererOutline != null)
        {
            for (int i = 0; i < RendererOutline.Length; i++)
            {
                SetColor(1, 1, 0, i);
            }
        }


        CurrentItem = 0;

        currentIndex = 0;


        if (WCZoom == null)
        {
            for (int i = 0; i < Done.Length; i++)
            {
                Done[i].enabled = false;
            }
        }
        Summary.SetActive(false);

    }

    public void SetColor(float r, float g, float b, int index, float overrideA = -1.0f)
    {

        _Color.r = r;
        _Color.g = g;
        _Color.b = b;

        _Color.a = (overrideA < 0.0f) ? baseAlpha : overrideA;

        RendererOutline[index].color = _Color;
    }

    public void SetCurrentItem(int index)
    {
        CurrentItem = index;
    }

    public void Toggle(int index)
    {
        if (Manager.Paused) { return; }
        Index = index;
        if (index != Order[currentIndex] || CurrentItem != RequiredItem[currentIndex])
        {
            if (!(WCZoom == null))
            {
                StopCoroutine("Wait");
                StartCoroutine("Wait");
            }
            else
            {
                StopCoroutine("BlinkOutline");
                StartCoroutine("BlinkOutline");
            }

            Points[currentIndex] /= 2;
            return;
        }
        if (WCZoom == null)
        {
            Done[index].enabled = true;
        }


        if (!CheckRecurringSpots(index))
        {
            Collision[index].enabled = false;
        }
    
        if (!(WCZoom == null))
        {
            if (CheckRecurringSpots(index))
            {
                ChangeColor(index, new Color(0, 0, 1, .5f));
            }
            else
            {
                ChangeColor(index, new Color(0, 1, 0, .5f));
            }
        } else
        {
            SetColor(0, 1, 0, Index);
        }

        currentIndex++;

        if (currentIndex >= Order.Length)
        {
            if (!(WCZoom == null))
            {
                WCZoom.ReturnToOverview(Points.Sum());
            }
            else
            {
                SummaryEnd();
            }
        }
    }

    public void SummaryEnd()
    {
        Total.text = (Points.Sum() + Tools.Points).ToString();

        for (int i = 0; i < Summaries.Length; i++)
        {
            Summaries[i].text = Points[i].ToString();
        }

        Selection.text = Tools.Points.ToString();

        Summary.SetActive(true);

        Manager.Managers.GiveScore(Points.Sum() + Tools.Points, MiniGameEnum.ROOM_CLEANING);
    }

    public void ChangeColor(int i, Color c)
    {
        Done[i].color = c;
    }

    IEnumerator Wait()
    {
        Done[Index].color = new Color(1, 0, 0, .5f);
        yield return new WaitForSeconds(.25f);
        Done[Index].color = new Color(1, 1, 0, .5f);

    }

    public bool CheckRecurringSpots(int index)
    {
        bool isClickable = false;
        for (int i = currentIndex + 1; i < Order.Length - 1; i++)
        {
            if (Order[i] == Order[index])
            {
                /*Debug.Log(Collision[i + 1]);
                Debug.Log(Collision[index]);*/
                isClickable = true;
            }
        }
        Debug.Log(isClickable);
        return isClickable;
    }

    IEnumerator BlinkOutline()
    {
            SetColor(1, 0, 0, Index);
            yield return new WaitForSeconds(.25f);
            SetColor(1, 1, 0, Index);
    }
}

