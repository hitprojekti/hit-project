﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;

public class Controls
{
    public Transform Host;
    public MonoObject Mono;

    private Camera _cam;

    public Controls(Transform t, MonoObject m)
    {
        Host = t;
        Mono = m;

        _cam = Camera.main;
    }

    public virtual void StartControls() { ValidateUpdate(); Mono.OnUpdate += Update; Host.rotation = Quaternion.identity; }
    public virtual void StopControls() { ValidateUpdate(); }

    public Vector3 GetPostition()
    {
        Vector3 temp = Input.mousePosition;
        temp = _cam.ScreenToWorldPoint(temp);
        temp.z = 0;

        return temp;

    }


    public Vector3 GetPlayerPostition()
    {

        return Host.position;

    }

    public void ValidateUpdate()
    {
        if(Mono.OnUpdate == null) { return; }
        Delegate[] Dels = Mono.OnUpdate.GetInvocationList();
        if (Dels.Contains((OnEmpty)Update))
        {
            Mono.OnUpdate -= Update;
        }
    }

    public virtual void Update()
    {
        Host.transform.position = GetPostition();
    }


    
}