﻿public abstract class Achievement
{
    public AchievementEnum AchievementType;

    public bool AchievementCompleted;

    private string AchievementName;

    private string AchievementDescription;

    protected AchievementManager Manager;

    public Achievement(string name, string desc, AchievementEnum type, AchievementManager _m)
    {
        Manager = _m;
        AchievementName = name;
        AchievementDescription = desc;

        AchievementType = type;
    }

    public virtual void CheckCompleted()
    {
        if (AchievementCompleted) { return; }


        AchievementCompleted = true;
    }

    public virtual void InitializeAchievement() { }

    public virtual string GetAchievementName()
    {
        return AchievementName;
    }

    public virtual string GetAchievementDescription()
    {
        return AchievementDescription;
    }



}