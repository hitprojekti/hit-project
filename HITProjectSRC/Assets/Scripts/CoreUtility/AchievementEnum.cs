﻿public enum AchievementEnum
{
    Total_5K = 0,
    Total_25K = 1,
    Total_100K = 2,
    Total_250K = 3,
    Total_500K = 4,
    Total_1M = 5,

    CompletedMinigames_1 = 6,
    CompletedMinigames_5 = 7,
    CompletedMinigames_15 = 8,

    TimePlayed_10M = 9,
    TimePlayed_30M = 10,
    TimePlayed_1H = 11,
    TimePlayed_10H = 12,
    TimePlayed_25H = 13,
    TimePlayed_50H = 14,


}