﻿public class AchievementManager
{

    public AchievementFinished OnAchievementFinished;

    public Achievement[] Achievements;


    public AchievementManager()
    {
        Achievements = new Achievement[]
        {
            new Totalscore5K_Achievement("Total 5K!", "Get 5 000 total score.", AchievementEnum.Total_5K, this),
        };
      
    }

    public void InitializeAchievements()
    {
        for (int i = 0; i < Achievements.Length; i++)
        {
            Achievements[i].InitializeAchievement();
        }
    }

    public void LoadAchievementsFromFile(bool[] achieved)
    {
        for (int i = 0; i < achieved.Length; i++)
        {
            Achievements[i].AchievementCompleted = achieved[i];
        } 
    }

    public void TriggerOnAchievementFinished(Achievement ach)
    {
        GameManager.Instance.ProfileManager.SaveProfileToFile();

        if (OnAchievementFinished == null) { return; }

        OnAchievementFinished(ach);


    }

}