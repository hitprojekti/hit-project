﻿using UnityEngine;

public struct AnimationObject
{
    public string Name;
    public Animation Animation;
    
    public AnimationObject(string name, Animation anim)
    {
        Animation = anim;
        Name = name;
    }
}