﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Atlas
{
    public string atlasName;
    public List<Sprite> sprites;

    private SpriteBank SB;


    public Atlas(string atlasName, string atlasPath, SpriteBank bank)
    {
        this.atlasName = atlasName;
        SB = bank;
        LoadAtlas(atlasPath);
    }

    public Sprite GetSprite(string name)
    {
        if (sprites.Count <= 0)
        {

            SB.State = BankState.FailedUpdating;
            return null;
        }
        for (int i = 0; i < sprites.Count; i++)
        {
            if (sprites[i].name == name)
            {
                return sprites[i];
            }
        }
        return sprites[0];
    }

    public Sprite[] GetSprites()
    {
        return sprites.ToArray();
    }

    public Sprite[] GetSprites(string[] names)
    {
        List<Sprite> temp = new List<Sprite>();

        for (int i = 0; i < names.Length; i++)
        {

            temp.Add(GetSprite(names[i]));
        }

        return temp.ToArray();
    }



    private void LoadAtlas(string path)
    {
        sprites = Resources.LoadAll<Sprite>(@"Sprites/Atlases/" + path).ToList();

        if (sprites.Count <= 0)
        {
            SB.State = BankState.FailedUpdating;
        }

        Debug.Log(string.Format("Atlas: Found {0} Sprites In Atlas '{1}'.", sprites.Count, atlasName));
    }

}