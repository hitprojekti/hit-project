﻿using System.Collections;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager
{
    public SoundBank Bank;
    public ObjectPool PrefabBank;
    public AudioMixer Mixer;

    public AudioManager(ObjectPool pB, SoundBank sB, AudioMixer sfxG)
    {
        Bank = sB;
        PrefabBank = pB;

        Mixer = sfxG;
   
    }

    public void PlaySoundEffect(float vol, string AudioClip)
    {
        AudioSource go;
        AudioClip clip;
        SFX sfx = Bank.GetAudioClipByName(AudioClip, StringMatchType.Fully, SoundType.SoundSFX);


        if (sfx == null) { return; }
        clip = sfx.clip;
        go = PrefabBank.GetPoolable("Audio").Spawn(Vector2.zero, Quaternion.identity).GetComponent<AudioSource>();
        go.Stop();

        IEnumerator i = Delay(clip.length, go.gameObject);

        go.volume = vol;
        go.clip = clip;
        go.Play();

        go.GetComponent<MonoObject>().StartCoroutine(i);
     
    }


    public void PlaySoundEffect(float vol, string AudioClip, AudioSource aS)
    {
        AudioSource go = aS;
        AudioClip clip;
        SFX sfx = Bank.GetAudioClipByName(AudioClip, StringMatchType.Fully, SoundType.SoundSFX);


        if (sfx == null) { return; }
        clip = sfx.clip;
        go.Stop();

        go.volume = vol;
        go.clip = clip;
        go.Play();


    }

    public void ToggleMixer(bool b, MixerType type)
    {
        float vol = b ? 0 : -80;

        switch (type)
        {
            case MixerType.MUSIC:

                Mixer.SetFloat("VolMusic", vol);
                break;


            case MixerType.SFX:

                Mixer.SetFloat("VolSFX", vol);
                break;
        }
    }

    private IEnumerator Delay(float f, GameObject go)
    {
        yield return new WaitForSeconds(f);
        go.SetActive(false);
    }
}