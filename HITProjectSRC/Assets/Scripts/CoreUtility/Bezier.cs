﻿using UnityEngine;

[System.Serializable]
public class Bezier
{
    public Transform StartPoint;
    public Transform MidPoint;
    public Transform EndPoint;

    public Vector3 StartP
    {
        get
        {
            if(StartPoint == null)
            {
                return _sTPos;
            }
            return StartPoint.position;
        }

        set
        {
            _sTPos = value;
        }
    }
    private Vector3 _sTPos;

    public Vector3 MidP
    {
        get
        {
            if (MidPoint == null)
            {
                return _mPos;
            }
            return MidPoint.position;
        }

        set
        {
            _mPos = value;
        }
    }
    private Vector3 _mPos;

    public Vector3 EndP
    {
        get
        {
            if (EndPoint == null)
            {
                return _ePos;
            }
            return EndPoint.position;
        }

        set
        {
            _ePos = value;
        }
    }
    private Vector3 _ePos;

    public Vector3 GetStartPos()
    {
        return StartP;
    }

    public Vector3 GetMidPos()
    {
        return MidP;
    }

    public Vector3 GetEndPos()
    {
        return EndP;
    }
}