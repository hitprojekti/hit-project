﻿using System.Collections.Generic;
using UnityEngine;

public class BezierCurveCalculator
{

    private Bezier[] KeyPoints;
    private int _segmentCount = 50;

    public BezierCurveCalculator(int sC, Bezier[] k)
    {
        _segmentCount = sC;
     
        KeyPoints = k;
       
    }

    public Vector3[] GetBezierPoints()
    {
        List<Vector3> TempVectors = new List<Vector3>();

        for (int i = 0; i < KeyPoints.Length; i++)
        {
            Vector3[] Points = new Vector3[3];
            Points[0] = KeyPoints[i].GetStartPos();
            Points[1] = KeyPoints[i].GetMidPos();
            Points[2] = KeyPoints[i].GetEndPos();

            for (int j = 0; j < _segmentCount; j++)
            {
                float t = j / (float)_segmentCount;
                int nodeIndex = i * 2;
                Vector3 temp = CalculateQuadraticBezierPoint(t, Points[0], Points[1], Points[2]);

                TempVectors.Add(temp);
            }
        }

        return TempVectors.ToArray();
    }

    private Vector3 CalculateQuadraticBezierPoint(float t, Vector3 p0, Vector3 p1, Vector3 p2)
    {

        Vector3 position = new Vector3();
    
            position = (1.0f - t) * (1.0f - t) * p0
            + 2.0f * (1.0f - t) * t * p1 + t * t * p2;
   
        return position;
    }
}