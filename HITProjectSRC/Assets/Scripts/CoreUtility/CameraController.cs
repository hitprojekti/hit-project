﻿using UnityEngine;

public class CameraController
{
    public CameraControls[] CameraControls;

    public CameraControls CurrentControls
    {
        get
        {
            return _controls;
        }

        set
        {
            if(CurrentControls != null)
            {
                CurrentControls.StopControls();
            }

            _controls = value;
        }
    }

    private CameraControls _controls;

    public CameraController()
    {
        Transform Root = GameObject.Find("CameraRoot").transform;

        CameraControls = new CameraControls[]
        {
            new OriginCameraControls(Root, CameraControlType.IDLE_WORLD_ORIGIN),
            new FollowYClampedCameraControls(Root, CameraControlType.FOLLOW_FINGER_Y_AXIS_CLAMPED),
            new ZoomToTargetCameraControls(Root, CameraControlType.ZOOM_IN_TARGET_POSITION),
            
        };
    }


    public void SetCurrentControls(CameraControlType type)
    {
        CurrentControls = GetControls(type);
    }

    public CameraControls GetControls(CameraControlType type)
    {
        for (int i = 0; i < CameraControls.Length; i++)
        {
            if(CameraControls[i].CameraControlType == type)
            {
                return CameraControls[i];
            }
        }

        return CameraControls[0];
    }
}