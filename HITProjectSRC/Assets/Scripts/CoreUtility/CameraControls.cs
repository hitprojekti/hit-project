﻿using UnityEngine;

public class CameraControls
{
    public CameraControlType CameraControlType;
    public Transform RootObject;
    private Camera cam;

    public CameraControls(Transform root, CameraControlType type)
    {
        CameraControlType = type;
        RootObject = root;
        cam = Camera.main;
    }

    public virtual void StartControls() { }
    public virtual void StopControls() { }
    public virtual void ResetControls() { }

    public void SetZoom(float zoom)
    {
        RootObject.transform.position = Vector3.zero;
        cam.orthographicSize = zoom;
    }
}