﻿using UnityEngine;

public class CartCleaningControls : Controls
{
    public GameManager Manager;

    public float LerpSpeed = 0.105f;

    public bool instant;

    private float _t;

    private float _maxXDistance = 2.0f;

    private float _distance;

    private float _distanceX;

    private float _distanceThreshold = 0.1f;

    private Vector3 _position;

    private Vector3 _positionX;

    private Quaternion _rotation;

    private float _rotationAngle = 5.0f;

    public CartCleaningControls(Transform t, MonoObject m) : base(t, m)
    {
        Manager = GameManager.Instance;
        _positionX = new Vector3(0, 0);
    }

    public override void StartControls()
    {
        base.StartControls();
        _t = 0;
    }

    public override void StopControls()
    {
        base.StopControls();
        _t = 0;
    }
    private Vector3 _zer;
    public override void Update()
    {

        _position = GetPostition();


        _rotation = Quaternion.Euler(0, 0, _rotationAngle * GetRotationMultiplier(Host.position, _position));


        Host.position = instant ? _position : Vector3.SmoothDamp(Host.position, _position, ref _zer, LerpSpeed);


        Host.rotation = _rotation;

        instant = false;
    }

    public float GetRotationMultiplier(Vector3 posA, Vector3 posB)
    {
        bool negative = (posA.x < posB.x);
        float val = Mathf.Abs(posA.x - posB.x);

        return val * (negative ? -1.0f : 1.0f);
    }
   
}