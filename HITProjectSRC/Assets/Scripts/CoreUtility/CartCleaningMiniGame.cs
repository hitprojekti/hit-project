﻿using System.Collections;
using UnityEngine;

public class CartCleaningMiniGame : MiniGame
{

    public MonoCartCleaningMinigame CartClean;

    public CartCleaningPart CurrentCartCleaningPart;

    private float _minYTillReturn = 2;

    private float _minYTillReturnReleased = -0.25f;

    private float _DistanceMinPickup = 0.2f;

    private bool _cartNext;

    private Transform _itemRoot;


    public CartCleaningMiniGame(string name, MiniGameEnum e) : base(name, e)
    {
        _itemRoot = Manager.MonoFinger.transform.GetChild(1);
    }

    public override void StartMiniGame()
    {
        base.StartMiniGame();

        CartClean = Manager.Pooler.GetPoolable("CleaningCart").Spawn(Vector3.zero, Quaternion.identity).GetComponent<MonoCartCleaningMinigame>();

        CartClean.ScoreText.text = "";
        CartClean.UI.SetActive(false);
        CartClean.Cvg.interactable = true;
        CartClean.Initialize();
        CartClean.ResetMinigame();

        _cartNext = false;

        CartClean.RandomizeBucketPos();

        CartClean.PlayAnimation("Cart_Buckets_Idle_In");
        Manager.CameraController.SetCurrentControls(CameraControlType.IDLE_WORLD_ORIGIN);
        Manager.CameraController.CurrentControls.SetZoom(12);
   
        Manager.Finger.CurrentItem = Manager.Finger.GetItem(ItemEnum.CLEANING_CART_HAND);

        Manager.ProfileManager.StopGamePlayTime();
        Manager.ProfileManager.PlayTimeCalculatorStart();

        Manager.MonoManager.AddUpdateListener(Update);
    }

    public override void ResetMiniGame()
    {

        CartClean.ScoreText.text = "";

        CartClean.RandomizeBucketPos();

        CartClean.UI.SetActive(false);
        CartClean.Cvg.interactable = true;
        _cartNext = false;
        CartClean.PlayAnimation("Cart_Buckets_Idle_In");
        Manager.MonoManager.RemoveUpdateListener(Update);
        CartClean.ResetMinigame();

        CartClean.Dispose();
        base.ResetMiniGame();
    }

    public override void CloseMiniGame()
    {
        CartClean.UI.SetActive(false);
        CartClean.Cvg.interactable = true;
        _cartNext = false;
        CartClean.PlayAnimation("Cart_Buckets_Idle_In");
        Manager.MonoManager.RemoveUpdateListener(Update);
        CartClean.ResetMinigame();

        CartClean.Dispose();
        base.CloseMiniGame();
    }

    public void Update()
    {
        CheckAll();
        GetTouch();
    }

    public void SetSummary()
    {
        for (int i = 0; i < CartClean.Texts.Length; i++)
        {
            CartClean.Texts[i].text = CartClean.CleanableParts[i].CurrentScore.ToString();
        }

        CartClean.Total.text = CurrentScoreAmount.ToString();
    }

    public void DropItem()
    {
        if(CurrentCartCleaningPart == null) { return; }

        CurrentCartCleaningPart.Ps.enabled = true;


        CurrentCartCleaningPart.Pulse.StartPulse(1.05f, 1.05f);
     
        if (CurrentCartCleaningPart.MovableObject.position.y > _minYTillReturn)
        {
            CartClean.MovePartToPlace(CurrentCartCleaningPart);

            CurrentCartCleaningPart = null;
            return;
        }


        Rigidbody2D rb = CurrentCartCleaningPart.MovableObject.GetComponent<Rigidbody2D>();

        rb.isKinematic = false;
        rb.velocity = Vector2.zero;


        CurrentCartCleaningPart.MovableObject.SetParent(CartClean.Root);
        CurrentCartCleaningPart = null;
    }

    private float _distance;

    private float Distance(float a, float b)
    {
        return Mathf.Abs(a - (b));
    }

    private IEnumerator End()
    {
        yield return new WaitForSeconds(1.25f);

        if (CartClean.CleanableParts[CartClean.CleanableParts.Length - 1].State == CartCleaningPartStateEnum.Done)
        {

            Manager.ProfileManager.Profile.ModifyTotalScore(ModifiactionType.ADD, CurrentScoreAmount, (int)MiniGameIndex);

            if (CurrentScoreAmount > Manager.ProfileManager.Profile.HiScore[(int)MiniGameIndex])
            {
                Manager.ProfileManager.Profile.ModifyHiScore(ModifiactionType.SET, CurrentScoreAmount, (int)MiniGameIndex);


            }

            Manager.ProfileManager.ProfilePage.UpdateInfoBox();
            Manager.MiniGameManager.UpdateMinigameScoreUI();

            Manager.ProfileManager.SaveProfileToFile();


            SetSummary();
            CartClean.Cvg.interactable = true;
            CartClean.UI.SetActive(true);

        }
    }

    public void CloseGame()
    {
    
        CartClean.Cvg.interactable = false;
        Manager.MonoManager.CloseMiniGame();
    }

    public void CheckAll()
    {
        if (CartClean.CleanableParts[CartClean.CleanableParts.Length - 1].State == CartCleaningPartStateEnum.Done && _cartNext)
        {
            _cartNext = false;
            Manager.MonoManager.StartCoroutine(End());
            return;
        }

            if (!_cartNext)
        {
            if(CartClean.IsCleanableGrappable(CartClean.CleanableParts[CartClean.CleanableParts.Length - 1]))
            {
                _cartNext = true;

                CartClean.PlayAnimation("Cart_Buckets_In");
            }
        
        }

        for (int i = 0; i < CartClean.CleanableParts.Length; i++)
        {
            if (!CartClean.IsCleanableGrappable(CartClean.CleanableParts[i])) { continue; }

            
                CartClean.CleanableParts[i].Ps.enabled = true;


            CartClean.CleanableParts[i].Pulse.StartPulse(1.05f, 1.05f);
           
            for (int j = 0; j < CartClean.CleaningBins.Length; j++)
            {
                if (CartClean.CleaningBins[j].WithinBounds(CartClean.CleanableParts[i].c))
                {
                   bool bb = CartClean.CleaningBins[j].TryToTake(CartClean.CleanableParts[i]);


                    if (bb)
                    {
                        CartClean.CleanableParts[i].Pulse.FadeToDefault(4.0f);
                        CartClean.CleanableParts[i].Ps.enabled = false;
                        CartClean.CleanableParts[i].State = CartCleaningPartStateEnum.Done;
                        CartClean.MovePartToBin(CartClean.CleanableParts[i], CartClean.CleaningBins[j]);
                        CurrentScoreAmount += CartClean.CleanableParts[i].CurrentScore;

                        CartClean.SetScore(CartClean.CleanableParts[i].CurrentScore.ToString(), ((Vector2)CartClean.CleaningBins[j].Root.localPosition));
                        return;
                    }

                    CartClean.CleanableParts[i].CurrentScore /= 2;

                    CartClean.MovePartToPlace(CartClean.CleanableParts[i]);
                
                    return;
                }
            }

            if(CartClean.CleanableParts[i].MovableObject.position.y > -15.0f) { continue; }
            CartClean.MovePartToPlace(CartClean.CleanableParts[i]);
        }
    }

    public void GetTouch()
    {
        if (Input.GetMouseButtonUp(0))
        {
            if(CurrentCartCleaningPart != null)
            {
                DropItem();
                return;
            }
        }

        if(CurrentCartCleaningPart != null) { return; }
        if (Input.GetMouseButton(0))
        {
            for (int i = 0; i < CartClean.CleanableParts.Length; i++)
            {

                if(!CartClean.IsCleanableGrappable(CartClean.CleanableParts[i])) { continue; }

     
                if (CartClean.CleanableParts[i].PointWithinBounds(Manager.Finger.CurrentItem.Controls.GetPostition()))
                {
                    (Manager.Finger.CurrentItem.Controls as CartCleaningControls).instant = true;
                    (Manager.Finger.CurrentItem.Controls as CartCleaningControls).Update();
                    CartClean.CleanableParts[i].Pulse.FadeToDefault(4.0f);
  
                    CurrentCartCleaningPart = CartClean.CleanableParts[i];

                    Rigidbody2D rb = CurrentCartCleaningPart.MovableObject.GetComponent<Rigidbody2D>();

                    rb.isKinematic = true;
                    rb.velocity = Vector2.zero;

                    CurrentCartCleaningPart.MovableObject.SetParent(_itemRoot);
        
                    CurrentCartCleaningPart.MovableObject.rotation = _itemRoot.rotation;


                    return;
                }
            }
        }
    }

    
}