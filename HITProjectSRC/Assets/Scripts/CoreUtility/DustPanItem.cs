﻿using UnityEngine;

public class DustPanItem : Item
{
    public DustPanItem(MonoObject _mono, string _itemName, string _animiationIdle, SpriteRenderer _rend, ItemEnum _index, Controls control) : base(_mono, _itemName, _animiationIdle, _rend, _index, control)
    {
    }

    public override void Visualize()
    {
        base.Visualize();
    }
}