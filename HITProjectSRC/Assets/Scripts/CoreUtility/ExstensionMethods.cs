﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using UnityEngine;

public static class ExtensionMethods
{

    #region String Management

    public static char[] GetDefaultSeparators(char[] additionalChars = null)
    {
        char[] chars = new char[] { ',', ';', ':', '*', '¨', '?', '!', '¤', '"', '´', '&', '%', '£', '€', '$', '<', '>', '|', '~', '^', '§', '½' };

        List<char> charList = chars.ToList();

        if(additionalChars != null)
        {
            for (int i = 0; i < additionalChars.Length; i++)
            {
                charList.Add(additionalChars[i]);
            }
        }
       

        return charList.ToArray();
    }

    public static string Replace(this string s, char[] separators, string newVal)
    {
        string[] temp;

        temp = s.Split(separators, StringSplitOptions.RemoveEmptyEntries);
        return String.Join(newVal, temp);
    }

    public static string Substring(string input, string indexOf, IndexOfType indexOfType)
    {
        switch (indexOfType)
        {
            case IndexOfType.IndexOf:
                return input.Substring(input.IndexOf(indexOf));

            case IndexOfType.LastIndexOf:
                return input.Substring(input.LastIndexOf(indexOf));

        }

        return input;
    }

    public static string Substring(string input, char indexOf, IndexOfType indexOfType)
    {
        switch (indexOfType)
        {
            case IndexOfType.IndexOf:
                return input.Substring(input.IndexOf(indexOf));

            case IndexOfType.LastIndexOf:
                return input.Substring(input.LastIndexOf(indexOf));

        }

        return input;
    }

    #endregion

    #region Unity Specific

    public static void SetTimeScale(float timeScale)
    {
        Time.timeScale = timeScale;
        Time.fixedDeltaTime = 0.02f * Time.timeScale;
    }

    #endregion

    #region Unity Asset Creating Functions

    public static Texture2D LoadTexture(string file)
    {
        Texture2D temp = new Texture2D(2, 2);
        byte[] bytes = null;
        bytes = ReadAllBytes(file);

        temp.LoadImage(bytes);

        return temp;
    }

    public static void SaveTexture(Texture2D tex, string output, bool threaded = false)
    {
        byte[] bytes = tex.EncodeToJPG();

        if (threaded)
        {
            Thread nT = new Thread(() => WriteAllBytesSafe(output, bytes, true));
            nT.Start();
            return;
        }
        WriteAllBytesSafe(output, bytes, true);
    }

    #endregion

    #region File Management

    #region File I/O

    public static void RemoveFile(string fileIn)
    {
        if (!FileExists(fileIn)) { return; }

        File.Delete(fileIn);
    }

    public static void WriteAllTextSafe(string outputLocation, string inputText, bool overWrite = true)
    {
        string dir = outputLocation.Remove(outputLocation.LastIndexOf("/"));

        ValidateDirectorysExistense(dir, true);

        if (FileExists(outputLocation) && !overWrite) { return; }


        File.WriteAllText(outputLocation, inputText, System.Text.Encoding.Default);
    }

    public static void WriteAllBytesSafe(string outputLocation, byte[] inputText, bool overWrite = true)
    {
        string dir = outputLocation.Remove(outputLocation.LastIndexOf("/"));

        ValidateDirectorysExistense(dir, true);

        if (FileExists(outputLocation) && !overWrite) { return; }


        File.WriteAllBytes(outputLocation, inputText);
    }

    public static byte[] ReadAllBytes(string location)
    {
        if (!FileExists(location)) { return new byte[0]; }

        return File.ReadAllBytes(location);
    }


    public static string ReadAllText(string location)
    {
        if (!FileExists(location)) { return ""; }

        return File.ReadAllText(location);
    }

    #region JSON Utility
    public static T FromJson<T>(string fileIn)
    {

        if (!FileExists(fileIn)) { return default(T); }

        T objects = JsonUtility.FromJson<T>(ReadAllText(fileIn));

        return objects;
    }

    public static string ToJson(object objectIn, string fileOut, bool prettyPrint = false)
    {
        string dir = fileOut.Remove(fileOut.LastIndexOf("/"));
        string content = JsonUtility.ToJson(objectIn, prettyPrint);


        ValidateDirectorysExistense(dir, true);

        WriteAllTextSafe(fileOut, content);

        return content;

    }

    #endregion

    #region Binary Utility

    public static void SaveWithFileStream(object input, string outPutFile)
    {
        BinaryFormatter BF = new BinaryFormatter();
        FileStream stream = new FileStream(outPutFile, FileMode.Create, FileAccess.Write);

        BF.Serialize(stream, input);

        stream.Close();
    }

    public static object LoadWithFileStream<T>(string outPutFile)
    {
        object temp;
        BinaryFormatter BF = new BinaryFormatter();
        FileStream stream = new FileStream(outPutFile, FileMode.Open, FileAccess.Read);

        temp = BF.Deserialize(stream);

        stream.Close();

        return temp;
    }

    public static void ToBinarySave(object input, string outputFile, bool overWrite = true)
    {
        if (FileExists(outputFile) && !overWrite) { return; }

        SaveWithFileStream(input, outputFile);
    }

    public static object FromBinarySave<T>(string outputFile)
    {
        if (!FileExists(outputFile)) { return null; }

        return LoadWithFileStream<T>(outputFile);
    }

    #endregion

    #endregion

    #region Directory Settings 

    public static DirectoryInfo[] GetDirectories(string input)
    {
        DirectoryInfo inf = new DirectoryInfo(input);

        if (!inf.Exists) { return null; }

        return inf.GetDirectories(input);
    }

    public static FileInfo[] GetFiles(string input, string extension = "")
    {

        
        DirectoryInfo inf = new DirectoryInfo(input);

        if (!inf.Exists) { return null; }

        return inf.GetFiles(extension);
    }

    public static FileInfo GetFirstFile(string input)
    {
        DirectoryInfo inf = new DirectoryInfo(input);

        if (!inf.Exists) { return null; }

        FileInfo infs;

        if(inf.GetFiles().Length <= 0) { return null; }

        infs = inf.GetFiles()[0];

        return infs;
    }

    public static FileInfo GetFile(string input)
    {
        FileInfo inf = new FileInfo(input);

        if (!inf.Exists) { return null; }

        return inf;
    }

    #region Directory Validation
    public static DirectoryInfo ValidateDirectorysExistense(string directoryIn, bool createIfNull)
    {
        DirectoryInfo inf = new DirectoryInfo(directoryIn);
        if (DirectoryExists(directoryIn)) { return inf; }
        if (!createIfNull) { return inf; }

        CreateDirectory(directoryIn);
        inf = new DirectoryInfo(directoryIn);

        return inf;
    }

    public static bool DirectoryExists(string directoryIn)
    {
        DirectoryInfo inf = new DirectoryInfo(directoryIn);

        return inf.Exists;
    }

    public static void CreateDirectory(string input)
    {
        Directory.CreateDirectory(input);
    }
    #endregion

    #endregion

    #region File Validation

    public static FileInfo ValidateFilesExistense(string fileIn)
    {
        FileInfo inf = new FileInfo(fileIn);

        if (FileExists(fileIn)) { return inf; }

        CreateDirectory(fileIn);

        inf = new FileInfo(fileIn);

        return inf;
    }

    public static bool FileExists(string fileIn)
    {
        FileInfo inf = new FileInfo(fileIn);

        return inf.Exists;
    }

    public static bool FileExists(FileInfo fileIn)
    {
        return fileIn.Exists;
    }

    #endregion

    #endregion

    #region Math Functions


    public static float AngleBetweenVector2(Vector2 vec1, Vector2 vec2)
    {
        Vector2 vec1Rotated90 = new Vector2(-vec1.y, vec1.x);
        float sign = (Vector2.Dot(vec1Rotated90, vec2) < 0) ? -1.0f : 1.0f;
        return Vector2.Angle(vec1, vec2) * sign;
    }


    public static float MultiplyVectors(Vector2 inputA, Vector2 inputB)
    {
        float temp = 0;
        float temp_B = 0;

        float aX = inputA.x;
        float aY = inputA.y;

        float bX = inputB.x;
        float bY = inputB.y;

        temp = (aX * aY) + (bX * bY);

        temp_B = inputA.sqrMagnitude * inputB.sqrMagnitude;

        temp /= temp_B;

        return Mathf.Cos(temp);

    }

    public static float GetDistanceFromDirection(Vector2 inputDirection, Vector2 inputPositionA, Vector2 inputPositionB)
    {
        Vector2 dir_1, dir_2;

        dir_1 = inputDirection;

        dir_2 = inputPositionA - inputPositionB;

        UnityEngine.Debug.DrawRay(inputPositionA, (dir_1) * 2, Color.red, 30);
        UnityEngine.Debug.DrawLine(inputPositionA,  inputPositionB, Color.blue, 30);
        float temp = 0;
        float temp_B = 0;

        float aX = dir_1.x;
        float aY = dir_1.y;

        float bX = dir_2.x;
        float bY = dir_2.y;

        temp = (aX * aY) + (bX * bY);

        temp_B = dir_1.sqrMagnitude * dir_2.sqrMagnitude;

        temp /= temp_B;

        return Mathf.Cos(temp);

    }

    #endregion
}
