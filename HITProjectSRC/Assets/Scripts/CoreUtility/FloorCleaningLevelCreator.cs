﻿using System.Collections.Generic;
using UnityEngine;

public class FloorCleaningLevelCreator
{
    public TrashGrid[] CleaningGrid;

    public TrashList[] Trash;

    public const int TRASH_PER_GRID = 55;

    public TrashTypeEnum[] types;

    public FloorCleaningLevelCreator()
    {
        types = new TrashTypeEnum[]
        {
            TrashTypeEnum.Vomit,
            TrashTypeEnum.Blood,
            TrashTypeEnum.Dust,
            TrashTypeEnum.Loose_Debris
        };
    }

    public void CreateLevel()
    {
        CleaningGrid = new TrashGrid[4];
        ReShuffle();
        Vector3[] vectors = new Vector3[]
        {
            new Vector3(-1,-1),
             new Vector3(-1,1),
              new Vector3(1,1),
               new Vector3(1,-1),
        };

        Trash = new TrashList[4];

        for (int i = 0; i < Trash.Length; i++)
        {
            Trash[i] = new TrashList();
        }

        for (int i = 0; i < CleaningGrid.Length; i++)
        {
            CleaningGrid[i] = new TrashGrid(vectors[i]);
            CleaningGrid[i].CreateGrid();
        }

        int currenTrashCount = 0;
        List<int> indexes = new List<int>();
        bool[] spawned;

        for (int i = 0; i < CleaningGrid.Length; i++)
        {
            spawned = new bool[CleaningGrid[i].NodeList.Count];
            currenTrashCount = 0;

            Trash[i].Initialize(CleaningGrid[i].NodeList.Count);

            while (currenTrashCount < TRASH_PER_GRID)
            {
                for (int j = 0; j < CleaningGrid[i].NodeList.Count; j++)
                {

                    if(!SetTrash(j, CleaningGrid[i].NodeList.Count * 2) || spawned[j]) { continue; }

                    spawned[j] = true;
                    Trash[i].CreateTrash(j, types[i], CleaningGrid[i].NodeList[j]);
                    currenTrashCount++;

                }
            }      
        }     
    }

    public Trash[] GetNearestTrashNodesAtPosition(int id, Vector3 position, float minDistance)
    {
        List<Trash> TrashList = new List<Trash>();
        for (int i = 0; i < Trash[id].Trash.Length; i++)
        {
            if (Trash[id].Trash[i].IsCleaned ||  Vector3.Distance(position, Trash[id].Trash[i].Position) < minDistance) { continue; }

           TrashList.Add(Trash[id].Trash[i]);
        }
        return TrashList.ToArray();
    }

    private bool SetTrash(int index, int maxIndex)
    {
        int rnd = Random.Range(0, maxIndex);

        return rnd == index;
    }

     private void ReShuffle()
    {
        for (int t = 0; t < types.Length; t++)
        {
            TrashTypeEnum tmp = types[t];
            int r = Random.Range(t, types.Length);
            types[t] = types[r];
            types[r] = tmp;
        }
    }
}