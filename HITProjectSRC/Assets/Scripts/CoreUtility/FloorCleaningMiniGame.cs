﻿using UnityEngine;

public class FloorCleaningMiniGame : MiniGame
{

    public FloorCleaningMiniGame(string name, MiniGameEnum e) : base(name, e)
    {
     
    }

    public override void StartMiniGame()
    {
        base.StartMiniGame();

        SceneManagerHit.ChangeScene(4);
    }

}