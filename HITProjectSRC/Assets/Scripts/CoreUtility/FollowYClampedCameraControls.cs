﻿using UnityEngine;

public class FollowYClampedCameraControls : CameraControls
{
    private float MinY, MaxY;

    private float dampTime = 64;

    private Transform target;
    private Vector3 position;
    private GameManager Manager;
    private Vector3 REF = Vector3.zero;

    public FollowYClampedCameraControls(Transform root, CameraControlType type) : base(root, type)
    {
        Manager = GameManager.Instance;
        position = new Vector3();
        position.x = 0;
        MinY = -0.0f;
        MaxY = 71.0f;
    }

    public override void ResetControls()
    {
        position = Vector3.zero;

        target = Manager.MonoFinger.transform;
        base.ResetControls();
        RootObject.position = Vector3.zero;
    }

    public override void StartControls()
    {
        ResetControls();

        target = Manager.MonoFinger.transform;
        base.StartControls();
        Manager.MonoManager.AddUpdateListener(Update);
    }

    public override void StopControls()
    {
        base.StopControls();
        Manager.MonoManager.RemoveUpdateListener(Update);
    }

    public void Update()
    {

        position = Vector3.SmoothDamp(position, target.position, ref REF, dampTime * Time.deltaTime);

        position.y = Mathf.Clamp(position.y, MinY, MaxY);

        position.x = 0;

        RootObject.position = position;
    }
}