﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class GameManager
{
    public static GameManager Instance;

    public MonoGameManager MonoManager;

    public GamePlayTimeCalculator GamePlayTimeCalculator;

    public SpriteBank SpriteBank;
    public SoundBank SoundBank;
    public PrefabBank PrefabBank;

    public ProfilePageManager ProfileManager;
    public UIManager UIManager;
    public MiniGameManager MiniGameManager;

    public CameraController CameraController;
    public AudioManager AudioManager;

    public MonoFinger MonoFinger;
    public Finger Finger;

    public AchievementManager AchievementManager;

    public ObjectPool Pooler;

    public Flash Flash;

    private string root;
    private string saveRoot;
    private string profilePicRoot;

    public GameManager(MonoGameManager _MonoManager, AudioMixer sfx)
    {
        Instance = this;
        MonoManager = _MonoManager;

        if(Screen.currentResolution.refreshRate > 60)
        {
            QualitySettings.vSyncCount = 1;
            Application.targetFrameRate = -1;
        }

        else
        {
            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = 60;
           
        }

      
        ValidateFileDirectories();

        StartBankLoading();

        LoadPooler();
      
        CreateFinger();

        CreateManagers(sfx);
    }


    public string GetSaveRoot()
    {
        return saveRoot+ "/";
    }

    public string GetProfilePicSaveRoot()
    {
        return profilePicRoot + "/";
    }


    public string GetFileRoot()
    {
        return root + "/";
    }

    private string GetInternalPath()
    {
        AndroidJavaClass activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject activity = activityClass.GetStatic<AndroidJavaObject>("currentActivity");
        return activity.Call<AndroidJavaObject>("getFilesDir").Call<string>("getPath");
    }

    private void ValidateFileDirectories()
    {
        Debug.Log("Gamemanager: Validating File Directories' Existense.");

        root = Application.persistentDataPath;

        saveRoot = root + "/Saves";

        profilePicRoot = saveRoot + "/ProfilePics";


        ExtensionMethods.ValidateDirectorysExistense(GetSaveRoot(), true);
        ExtensionMethods.ValidateDirectorysExistense(GetProfilePicSaveRoot(), true);

        Debug.Log("Gamemanager: Done Validating File Directories' Existense Successful.");
    }

    private void CreateManagers(AudioMixer gg)
    {
        Debug.Log("Gamemanager: Creating Managers.");

        Flash = new Flash(GameObject.Find("Flash").GetComponent<CanvasGroup>(), MonoManager);

        UIManager = new UIManager();

        ProfileManager = new ProfilePageManager();

        AchievementManager = new AchievementManager();

        MiniGameManager = new MiniGameManager(new MiniGame[] 
        {
            new MoppingMiniGame("Toilet Cleaning", MiniGameEnum.TOILET_CLEANING),
            new FloorCleaningMiniGame("Room Cleaning", MiniGameEnum.ROOM_CLEANING),
            new TowelPreparationMiniGame("Towel Preparation", MiniGameEnum.TOWEL_PREP),
            new ÉrgonomyMiniGame("Ergonomy", MiniGameEnum.ERGONOMY),
            new CartCleaningMiniGame("Cart Cleaning", MiniGameEnum.CART_CLEANING)

        });

        CameraController = new CameraController();


        AudioManager = new AudioManager(Pooler, SoundBank,gg);

        GamePlayTimeCalculator = new GamePlayTimeCalculator(ProfileManager.Profile, MonoManager);

    }


    private void CreateFinger()
    {
        Debug.Log("Gamemanager: Creating Finger.");
    
        MonoFinger = GameObject.Find("*Finger*").GetComponent<MonoFinger>();
        SpriteRenderer rend = MonoFinger.transform.GetChild(0).GetComponent<SpriteRenderer>();

        Finger = new Finger(MonoFinger, new Item[] 
        {
            new HandItem(MonoFinger, "Hand", "HandIdle", rend, ItemEnum.HAND, new HandControls(MonoFinger.transform, MonoFinger)),
            new MopItem(MonoFinger, "Mop", "MopIdle", rend, ItemEnum.MOP, new MopControls(MonoFinger.transform, MonoFinger)),
            new RagItem(MonoFinger, "Rag", "RagIdle", rend, ItemEnum.RAG, new RagControls(MonoFinger.transform, MonoFinger)),
            new DustPanItem(MonoFinger, "Dust Pan", "DustPanIdle", rend, ItemEnum.DUSTPAN, new DustPanControls(MonoFinger.transform, MonoFinger)),
            new HandItem(MonoFinger, "Cart cleaning hand", "HandIdle",rend, ItemEnum.CLEANING_CART_HAND, new CartCleaningControls(MonoFinger.transform, MonoFinger))

        });
        MonoFinger.Init();   
        MonoManager.AddUpdateListener(MonoFinger.TriggerOnUpdate);

        Finger.CurrentItem = Finger.GetItem(ItemEnum.HAND);
    }

    private void StartBankLoading()
    {
        Debug.Log("Gamemanager: Starting Bank Loading.");

        MonoManager.StartCoroutine(LoadBanks());
    }

    private void LoadPooler()
    {
        Debug.Log("Gamemanager: Creating Object Pooler.");

        Pooler = new ObjectPool();

        Pooler.Pools.Add(new Poolable(PrefabBank.LoadPrefab("UI", "ProfilePictureIObject"), 1));
        Pooler.Pools.Add(new Poolable(PrefabBank.LoadPrefab("Minigames", "CleaningCart"), 1));
        Pooler.Pools.Add(new Poolable(PrefabBank.LoadPrefab("Misc", "Audio"), 1));
        Pooler.Pools.Add(new Poolable(PrefabBank.LoadPrefab("Misc", "DebugText"), 1));
        Pooler.Pools.Add(new Poolable(PrefabBank.LoadPrefab("Props", "TrashTile"), 5));

        Pooler.CreateInitialPoolables();
    }

    private IEnumerator LoadBanks()
    {
        SpriteBank = new SpriteBank(this);

        while(SpriteBank.State != BankState.Idle)
        {
            yield return true;
        }

        Debug.Log("Gamemanager: Sprite Bank Loaded Successfully.");

        SoundBank = new SoundBank(this);

        while (SoundBank.State != BankState.Idle)
        {
            yield return true;
        }

        if(SoundBank != null)
        {
            Debug.Log("Gamemanager: Sound Bank Loaded Successfully.");
        }else
        {
            Debug.Log("Gamemanager: Sound Bank Load FAILED!");
        }
      

        PrefabBank = new PrefabBank(this);

        while (PrefabBank.State != BankState.Idle)
        {
            yield return true;
        }

        Debug.Log("Gamemanager: Prefab Bank Loaded Successfully.");

    }



}