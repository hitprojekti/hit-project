﻿using System;
using System.Collections;
using UnityEngine;

public class GamePlayTimeCalculator
{
    private GameManager Manager;

    private MonoObject Mono;

    private IEnumerator Calc, Int;

    private Profile Pro
    {
        get
        {
            if(_p == null)
            {
                if(Manager == null)
                {
                    return _p;
                }
                _p = Manager.ProfileManager.Profile;
            }

            return _p;
        }

        set
        {
            _p = value;
        }
    }


    private Profile _p;

    public GamePlayTimeCalculator(Profile pr, MonoObject m)
    {
        Mono = m;
        Pro = pr;
        Manager = GameManager.Instance;
        Int = WaitForRealSeconds(1);
    }

    public void StartClock()
    {
        StartC();
    }

    public void StartC()
    {
        StopC();
        Calc = Calculator();
        Mono.StartCoroutine(Calc);
    }

    public void StopC()
    {
        if(Calc == null) { return; }
        Mono.StopCoroutine(Calc);
    }

    private IEnumerator Calculator()
    {
        while (true)
        {
                Pro.PlayTime++;
                yield return Mono.StartCoroutine(WaitForRealSeconds(1));
        }

    }


    public IEnumerator WaitForRealSeconds(float time)
    {
        float start = Time.realtimeSinceStartup;
        while (Time.realtimeSinceStartup < start + time)
        {
            yield return null;
        }
    }
}