﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobileToggle : MobileButton {


    public ToggleVisualVariables[] VisualVariables;

    public MobileToggleEvents MobileToggleEvents;

    public bool isOn
    {
        get
        {
            return _isOn;
        }

        set
        {
            _isOn = value;

            UpdateSprites();
        }
    }

    private bool _isOn;

    public new void Awake()
    {
        base.Awake();

    }

    public new void Reset()
    {
        base.Reset();
    }

    public new void OnMouseUp()
    {
        base.OnMouseUp();
        isOn = !isOn;
        Manager.AudioManager.PlaySoundEffect(1, MobileToggleEvents.OnValueChange);
        MobileToggleEvents.OnValueChanged.Invoke(isOn);
    }

    private void UpdateSprites()
    {

        switch (isOn)
        {
            case true:

                GetIdleSprite = VisualVariables[0].idleSprite;
                GetHoverSprite = VisualVariables[0].hoveredSprite;
                GetPressedSprite = VisualVariables[0].pressedSprite;
                GetDisabledSprite = VisualVariables[0].disabledSprite;

                break;

            case false:

                GetIdleSprite = VisualVariables[1].idleSprite;
                GetHoverSprite = VisualVariables[1].hoveredSprite;
                GetPressedSprite = VisualVariables[1].pressedSprite;
                GetDisabledSprite = VisualVariables[1].disabledSprite;

                break;
        }

        UpdateGraphic();
    }
}
