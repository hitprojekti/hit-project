﻿using UnityEngine.Events;

[System.Serializable]
public class MobileToggleEvents
{
    [UnityEngine.SerializeField]
    public UnityEngine.UI.Toggle.ToggleEvent OnValueChanged;
    public string OnValueChange;
}