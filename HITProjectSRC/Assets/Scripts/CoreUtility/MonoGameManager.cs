﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class MonoGameManager : MonoObject {

    public GameManager GameManager;
    public AudioMixer Mixer;

    void Awake () {
        
        GameManager = new GameManager(this, Mixer);

        OnUpdate += CheckKeys;
    }

    public IEnumerator Start()
    {

        yield return StartCoroutine(GameManager.ProfileManager.GetSaveFiles());

        GameManager.AudioManager.ToggleMixer(GameManager.ProfileManager.Profile.SFX, MixerType.SFX);
        GameManager.AudioManager.ToggleMixer(GameManager.ProfileManager.Profile.Music, MixerType.MUSIC);

        GameManager.UIManager.InitializeUI();

        GameManager.AchievementManager.InitializeAchievements();
    }

    private void Update()
    {
        TriggerOnUpdate();
    }

    private void CheckKeys()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    public void ToggleMinigameMenu(bool b)
    {
        GameManager.UIManager.ToggleMinigameMenu(b);
    }

    public void ToggleMusic(bool b)
    {
        GameManager.ProfileManager.SetMusic(b);
        GameManager.AudioManager.ToggleMixer(b, MixerType.MUSIC);
    }

    public void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            SaveGame();
        }
    }
    public void ToggleSFX(bool b)
    {
        GameManager.ProfileManager.SetSFX(b);

        GameManager.AudioManager.ToggleMixer(b, MixerType.SFX);
    }

    public void PauseGame(bool b)
    {
        if (b)
        {
            Time.timeScale = 0;
            GameManager.UIManager.Score.text = "" + GameManager.MiniGameManager.CurrentMiniGame.CurrentScoreAmount;
            GameManager.UIManager.PauseMenu.Play("PauseMenuIn", false);
        }

        else
        {
            GameManager.UIManager.PauseMenu.Play("PauseMenuOut_Anim", false);
            StopCoroutine("PauseSeq");
            StartCoroutine("PauseSeq");
        }
    }

    public void RestartMinigame()
    {
        GameManager.MiniGameManager.CurrentMiniGame.ResetMiniGame();
          GameManager.MiniGameManager.CloseMiniGame();
        GameManager.MiniGameManager.StartMiniGame((int)GameManager.MiniGameManager.CurrentMiniGame.MiniGameIndex);
    }
    public void EndMinigame()
    {
        CloseMiniGame();
    }

    IEnumerator PauseSeq()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        Time.timeScale = 1;
    }

    public void OpenMiniGame(int index)
    {
        ToggleMinigameMenu(false);
        GameManager.Flash.StartFlash(0, 3.6f, 1, Color.black, true);
        StartCoroutine(MiniGameSequence(index));
    }

    private IEnumerator MiniGameSequence(int index)
    {
        while (GameManager.Flash.stillFading)
        {
            yield return null;
        }
        yield return new WaitForSeconds(0.75f);
        GameManager.MiniGameManager.StartMiniGame(index);
        yield return null;
        System.GC.Collect();
        GameManager.Flash.StartFlash(1, 4f, 0, Color.black, true);

    }

    public void CloseMiniGame()
    {
        GameManager.Flash.StartFlash(0, 4f, 1, Color.black, true);
        StartCoroutine(MiniGameCloseSequence());
    }

    private IEnumerator MiniGameCloseSequence()
    {
        while (GameManager.Flash.stillFading)
        {
            yield return null;
        }

        GameManager.MiniGameManager.CloseMiniGame();
        yield return new WaitForSeconds(0.75f);

        GameManager.Flash.StartFlash(1, 4f, 0, Color.black, true);

        GameManager.UIManager.ToggleMinigameMenuInstant(true);
    }

    public void ToggleMainMenu(bool b)
    {
        GameManager.UIManager.ToggleMainMenu(b);
    }

    public void ToggleProfilePage(bool b)
    {
        GameManager.ProfileManager.ToggleProfilePage(b);
    }

    public void ToggleProfielPictureSelect(bool b)
    {
        GameManager.ProfileManager.OpenProfileImageSelector(b);
    }

    public void SetProfileName(string input)
    {
        GameManager.ProfileManager.Profile.ProfileName = input;
    }

    public void SaveGame()
    {
        GameManager.ProfileManager.SaveProfileToFile();
    }

    public void ToggleCameraSelect(bool b)
    {
        GameManager.ProfileManager.ToggleCamera(b);
    }

}
