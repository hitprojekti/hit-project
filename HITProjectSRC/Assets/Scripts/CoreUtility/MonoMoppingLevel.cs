﻿using UnityEngine;

public class MonoMoppingLevel : MonoObject
{
    public MoppingLevelCreator Level;

    public Transform KeyPointParent;
    public Bezier[] Beziers;
    public Vector3[] BezierPositions;

    public LineRenderer Renderer;

    public Transform SliderRoot, SliderKnob;

    public SpriteRenderer[] Rends;

    private GameManager Manger;



    [ContextMenu("Initialize")]
    public override void Init()
    {
        base.Init();
        Manger = GameManager.Instance;
    }

    [ContextMenu("Start")]
    public void StartCreator()
    {
        Level = new MoppingLevelCreator(this, KeyPointParent);

        Level.SetKeyPoints(Beziers);

        Level.GenerateLevel();
    }

    public void SetVisuals(Vector3[] pos)
    {
        BezierPositions = pos;

        Renderer.positionCount = pos.Length;
        Renderer.SetPositions(pos);
    }

    public void SetAlpha(float alpha)
    {
        for (int i = 0; i < Rends.Length; i++)
        {
            Rends[i].color = new Color(1, 1, 1, alpha);
        }
    }
    
}