﻿using System.Collections;
using UnityEngine;

public class MopControls : Controls
{

    private IEnumerator Loop;

    public GameManager Manager;

    private float angle = 90;

    public MopControls(Transform t, MonoObject m) : base(t, m)
    {
        Manager = GameManager.Instance;
    }

    public override void Update()
    {
        if(Loop != null) { return; }
        if (Input.GetMouseButtonDown(0))
        {
            StartRotateLoop();
            return;
        }
    }

    public void StartRotateLoop()
    {
        StartC();
    }

    private IEnumerator RotateLoop()
    {
       
        Vector2 Cursor = GetPostition();
        Vector2 CursorPosStart = GetPostition();

        Vector2 norm = new Vector2();
        float mult = 3.5f;

        float sing = 1;

        Vector3 posStartCam = Manager.CameraController.CurrentControls.RootObject.position;
        Vector3 eulers = new Vector3();

        float normalizedPos = 0;

        eulers.x = 0;
        eulers.y = 0;
        float posTemp;

        while (true)
        {
          
            Cursor = GetPostition();

            posTemp = (Cursor.y - (CursorPosStart.y + (Manager.CameraController.CurrentControls.RootObject.position.y - posStartCam.y)));
            norm.y = (posTemp / mult) -1;

            normalizedPos = Mathf.Clamp(norm.y, -1, 1);
            eulers.z = (angle * (normalizedPos * sing));

            Host.transform.rotation = Quaternion.Euler(eulers);

            yield return null;
            if (Input.GetMouseButtonUp(0))
            {
                StartControls();
                break;
            }
        
        }
        StopC();
    }


    private void StartC()
    {
        StopC();

        Loop = RotateLoop();

        Mono.StartCoroutine(Loop);
    }

    private void StopC()
    {
        if (Loop == null) { return; }

        Mono.StopCoroutine(Loop);

        Loop = null;
    }
}