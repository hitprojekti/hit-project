﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MoppingLevelCreator
{
    public MonoMoppingLevel MoppingLevel;

    private Bezier[] KeyPositions;
    private Vector3[] BezierPositions;

    private GameManager Manager;
    private BezierCurveCalculator Bezier;
    private Transform MainObject;

    public MoppingLevelCreator(MonoMoppingLevel lvl, Transform tr)
    {
        MainObject = tr;
        Manager = GameManager.Instance;
        MoppingLevel = lvl;
    }

    public Vector2 GetBezierPosition(int index)
    {
        return BezierPositions[Mathf.Clamp(index, 0, BezierPositions.Length - 1)];
    }

    public bool IsLast(int i)
    {
        return i >= BezierPositions.Length - 1;
    }

    public void SetKeyPoints(Bezier[] beziers)
    {
        KeyPositions = beziers;
    }

    public void GenerateLevel()
    {
        MoppingLevel.StartCoroutine(CreateLevel());
    }

    private IEnumerator CreateLevel()
    {


        Bezier = new BezierCurveCalculator(16, KeyPositions);
        BezierPositions = Bezier.GetBezierPoints();

        yield return null;
        MoppingLevel.SetVisuals(BezierPositions);
     
    }

}