﻿using UnityEngine;

[System.Serializable]
public struct MoppingLevelData
{
    public float SpeedMultiplier;

    public float Speed;

    public MoppingLevelData(float _spdM, float _spd)
    {

        SpeedMultiplier = _spdM;
        Speed = _spd;
        
    }
}