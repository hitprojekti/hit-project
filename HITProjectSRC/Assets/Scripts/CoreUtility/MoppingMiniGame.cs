﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoppingMiniGame : MiniGame
{
    


    public MoppingMiniGame(string name, MiniGameEnum e) : base(name, e)
    {
       
    }

    public override void StartMiniGame()
    {
        base.StartMiniGame();

        SceneManagerHit.ChangeScene(3);
    }

}