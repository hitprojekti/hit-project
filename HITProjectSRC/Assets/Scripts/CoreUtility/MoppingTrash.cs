﻿using System;
using UnityEngine;

public class MoppingTrash
{

    public OnEmpty OnIntegrityChange;
    public OnOutputFloat OnClean;

    public Vector2 Position;
    public Vector2 Direction;

    public float Integrity
    {
        get
        {
            return _integrity;
        }

        set
        {
            _integrity = value;

            TriggerOnIntegrityChange();
        }
    }


    public bool IsCleaned
    {
        get
        {
            return Integrity <= 0;
        }
    }

    private float _integrity;

    public MoppingTrash(float _Integrity, Vector2 _pos, Vector2 _dir)
    {
        Integrity = _Integrity;
        Position = _pos;
        Direction = _dir;

    }

    public void CleanTrash(Vector2 dir)
    {
        float direct = Vector2.Angle(dir, (Direction)) / 2.3333333f;

        float removedTrash = Mathf.Floor((1 - (direct / 180)) * 8) / 8;

        TriggerOnClean(removedTrash);

        Integrity -= removedTrash;
    }

    private void TriggerOnIntegrityChange()
    {
        if(OnIntegrityChange == null) { return; }

        OnIntegrityChange();
    }

    private void TriggerOnClean(float f)
    {
        if (OnClean == null) { return; }

        OnClean(f);
    }
}