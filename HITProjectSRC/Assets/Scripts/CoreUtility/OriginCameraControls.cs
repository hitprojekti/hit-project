﻿using UnityEngine;

public class OriginCameraControls : CameraControls
{
  

    public OriginCameraControls(Transform root, CameraControlType type) : base(root, type)
    {
    
    }

    public override void StartControls()
    {
        base.StartControls();
        RootObject.transform.position = Vector3.zero;
    }

 
}