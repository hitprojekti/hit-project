﻿using UnityEngine;

public class PrefabGroup
{
    public string prefabGroupName;
    public GameObject[] prefab;

    public PrefabGroup(string _n, GameObject[] _go)
    {
        prefabGroupName = _n;
        prefab = _go;

    }

    public GameObject GetPrefab(string name)
    {

        if (prefab.Length <= 0) { return null; }

        for (int i = 0; i < prefab.Length; i++)
        {
            if (prefab[i].name == name)
            {
                return prefab[i];
            }
        }

        return prefab[0];
    }

}