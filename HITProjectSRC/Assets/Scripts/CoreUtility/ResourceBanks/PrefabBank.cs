﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabBank {

    public List<PrefabGroup> prefabs;
    public BankState State;

    private GameManager manager;

    public PrefabBank(GameManager _manager)
    {
        manager = _manager;

        State = BankState.Idle;

        manager.MonoManager.StartCoroutine(SequenceInit());
    }

    public void UpdatePrefabs()
    {
        State = BankState.Updating;

        prefabs = new List<PrefabGroup>();
        //TODO: Add prefab groups eg. prefabs.Add(new PrefabGroup("<Prefab group name>", LoadPrefab("<Path after "Prefab/">")));

        prefabs.Add(new PrefabGroup("Misc", LoadPrefabs("Misc/")));
        prefabs.Add(new PrefabGroup("UI", LoadPrefabs("UI/")));
        prefabs.Add(new PrefabGroup("Props", LoadPrefabs("Props/")));
        prefabs.Add(new PrefabGroup("Minigames", LoadPrefabs("Minigames/")));

        State = BankState.Idle;
    }




    public GameObject LoadPrefab(string prefabGroupName, string prefabName)
    {
        if(prefabs.Count <= 0) { return null; }

        for (int i = 0; i < prefabs.Count; i++)
        {
            if(prefabs[i].prefabGroupName == prefabGroupName)
            {
                return prefabs[i].GetPrefab(prefabName);
            }
        }
        return prefabs[0].GetPrefab(prefabName);
    }

    public GameObject Instantiate(GameObject go, Vector2 position = default(Vector2), Quaternion rotation = default(Quaternion))
    {
        if (go == null) { return null; }
        return MonoBehaviour.Instantiate(go, position, rotation);
    }

    private GameObject[] LoadPrefabs(string path)
    {
        return Resources.LoadAll<GameObject>(@"Prefabs/" + path);
    }

    private IEnumerator SequenceInit()
    {
        UpdatePrefabs();

        while (State == (BankState.Updating | BankState.FailedUpdating))
        {
            yield return null;
        }

        State = BankState.Idle;
    }
   
}
