﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SoundBank {

    public SFX defaultFX;

    public List<SFX> soundEffects;
    public List<SFX> musicEffects;

    public BankState State;

    private GameManager manager;

    public SoundBank(GameManager _manager)
    {
        soundEffects = new List<SFX>();
        musicEffects = new List<SFX>();

        manager = _manager;

        State = BankState.Idle;
        manager.MonoManager.StartCoroutine(SequenceInit());
    }

    public void UpdateSFX()
    {
        soundEffects = new List<SFX>();
        musicEffects = new List<SFX>();

        List<AudioClip> clipTemp = new List<AudioClip>();
        
        clipTemp = Resources.LoadAll<AudioClip>("Sound/Effects/").ToList();

        for (int i = 0; i < clipTemp.Count; i++)
        {
            soundEffects.Add(new SFX(clipTemp[i]));
        }

        clipTemp = Resources.LoadAll<AudioClip>("Sound/Music/").ToList();

        for (int i = 0; i < clipTemp.Count; i++)
        {
            musicEffects.Add(new SFX(clipTemp[i]));
        }

        Debug.Log(string.Format("Sound Bank: Found {0} Sound Effect(s) Files & {1} Music Files.", soundEffects.Count, musicEffects.Count));

    }

    private AudioClip GetAudioclipFromResources(string path)
    {

        AudioClip clipTemp = Resources.Load<AudioClip>(path);

        if (clipTemp == null) { throw new SoundNotFoundException(path.Remove(0, path.LastIndexOf("/")+1)); }

        return clipTemp;
        
    }

    public List<SFX> GetAudioClipsByName(string[] input, StringMatchType match)
    {
        List<SFX> tempList = new List<SFX>();

        for (int i = 0; i < soundEffects.Count; i++)
        {

            switch (match)
            {

                case StringMatchType.Fully:
                    for (int j = 0; j < input.Length; j++)
                    {
                        if (soundEffects[i].name == input[i])
                        {
                            tempList.Add(soundEffects[i]);
                        }
                    }
                   
                    break;

                case StringMatchType.Start:

                    for (int j = 0; j < input.Length; j++)
                    {
                        if (soundEffects[i].name.StartsWith(input[i]))
                        {
                            tempList.Add(soundEffects[i]);
                        }
                    }

                    break;

                case StringMatchType.End:

                    for (int j = 0; j < input.Length; j++)
                    {
                        if (soundEffects[i].name.EndsWith(input[i]))
                        {
                            tempList.Add(soundEffects[i]);
                        }
                    }

                    break;

                case StringMatchType.Contains:

                    for (int j = 0; j < input.Length; j++)
                    {
                        if (soundEffects[i].name.Contains(input[i]))
                        {
                            tempList.Add(soundEffects[i]);
                        }
                    }
                    break;
            }
        }

        if(tempList.Count <= 0)
        {
            tempList.Add(defaultFX);
        }
        return tempList;
    }


    public SFX GetAudioClipByName(string input, StringMatchType match, SoundType stype)
    {
        switch (stype)
        {

            case SoundType.MusicSFX:

                for (int i = 0; i < musicEffects.Count; i++)
                {

                    switch (match)
                    {

                        case StringMatchType.Fully:
                            if (musicEffects[i].name == input)
                            {
                                return musicEffects[i];
                            }
                            break;

                        case StringMatchType.Start:

                            if (musicEffects[i].name.StartsWith(input))
                            {
                                return musicEffects[i];
                            }

                            break;

                        case StringMatchType.End:

                            if (musicEffects[i].name.EndsWith(input))
                            {
                                return musicEffects[i];
                            }

                            break;

                        case StringMatchType.Contains:

                            if (musicEffects[i].name.Contains(input))
                            {
                                return musicEffects[i];
                            }

                            break;
                    }
                }

                if (input != "")
                {
                    Debug.LogWarning("Sound Bank: There is no music effect named: '" + input + "' in the sound bank!");
                }
          
                return null;

            case SoundType.SoundSFX:


                for (int i = 0; i < soundEffects.Count; i++)
                {

                    switch (match)
                    {

                        case StringMatchType.Fully:
                            if (soundEffects[i].name == input)
                            {
                                return soundEffects[i];
                            }
                            break;

                        case StringMatchType.Start:

                            if (soundEffects[i].name.StartsWith(input))
                            {
                                return soundEffects[i];
                            }

                            break;

                        case StringMatchType.End:

                            if (soundEffects[i].name.EndsWith(input))
                            {
                                return soundEffects[i];
                            }

                            break;

                        case StringMatchType.Contains:

                            if (soundEffects[i].name.Contains(input))
                            {
                                return soundEffects[i];
                            }

                            break;
                    }
                }
                if(input != "")
                {
                    Debug.LogWarning("Sound Bank: There is no sound effect named: '" + input + "' in the sound bank!");
                }

                return defaultFX;
               
        }
        Debug.LogWarning("Sound Bank: This shouldn't happen.");
        return null;

    }

    private IEnumerator SequenceInit()
    {
        defaultFX = null;
        yield return null;
        UpdateSFX();

        while (State == (BankState.Updating | BankState.FailedUpdating))
        {
            yield return null;
        }
        yield return null;
        State = BankState.Idle;
    }

}

