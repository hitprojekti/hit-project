﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteBank {

    public List<Atlas> atlases;
    public List<SpriteAnimation> Animations;
    public BankState State;

    private GameManager manager;

    public SpriteBank(GameManager _manager)
    {
        manager = _manager;

        State = BankState.Idle;
        manager.MonoManager.StartCoroutine(SequenceInit());
     
    }

    public void UpdateAtlases()
    {
        State = BankState.Updating;
        atlases = new List<Atlas>();
        //TODO: atlases.Add(new Atlas("<AtlasName>", "<SubFolder>", this));

        atlases.Add(new Atlas("UI", "UI/", this));
        atlases.Add(new Atlas("Objects", "Objects/", this));
        atlases.Add(new Atlas("ProfilePictures", "ProfilePictures/", this));

        UpdateAnimations();

        Debug.Log(string.Format("Sprite Bank: Created {0} Atlases", atlases.Count));



        State = BankState.Idle;
    }

    public void UpdateAnimations()
    {
        Sprite[] temp;

        Animations = new List<SpriteAnimation>();

        temp = new Sprite[]
     {
            GetSprite("Objects", "Null"),

     };

        Animations.Add(new SpriteAnimation("HandIdle", temp, 1, 0));

        temp = new Sprite[]
        {
            GetSprite("Objects", "Mop_2"),

        };

        Animations.Add(new SpriteAnimation("MopIdle", temp, 1, 0));
    }

    public SpriteAnimation GetAnimation(string name)
    {
        for (int i = 0; i < Animations.Count; i++)
        {
            if (Animations[i].animationName == name)
            {
                return Animations[i];
            }
        }

        return null;
    }

    public Atlas GetAtlas(string atlasName)
    {
        for (int i = 0; i < atlases.Count; i++)
        {
            if (atlases[i].atlasName == atlasName)
            {
                return atlases[i];
            }
        }

        return null;
    }


    public Sprite GetSprite(string atlasName, string spriteName)
    {
        Atlas t = GetAtlas(atlasName);

        return t.GetSprite(spriteName);
    }

    public Sprite[] GetSprites(string atlasName)
    {
        Atlas t = GetAtlas(atlasName);

        return t.GetSprites();
    }

 
    private IEnumerator SequenceInit()
    {
        UpdateAtlases();

        while (State == (BankState.Updating | BankState.FailedUpdating))
        {
            yield return null;
        }
        State = BankState.Idle;
    }

}
