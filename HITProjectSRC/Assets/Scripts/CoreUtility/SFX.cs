﻿using UnityEngine;

public class SFX
{
    public string name
    {
        get
        {
            return clip.name;
        }
    }
    public AudioClip clip;

    public SFX(AudioClip clip)
    {
        this.clip = clip;

    }
}