﻿using UnityEngine;

[System.Serializable]
public struct ToggleVisualVariables
{
    public Sprite hoveredSprite, pressedSprite, idleSprite, disabledSprite;
}