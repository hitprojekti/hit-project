﻿using UnityEngine;

public class Totalscore5K_Achievement : Achievement
{
    Profile p;

    public Totalscore5K_Achievement(string name, string desc, AchievementEnum type, AchievementManager _m) : base(name, desc, type, _m)
    {
    }


    public override void InitializeAchievement()
    {
        if(AchievementCompleted) { return; }

        p = GameManager.Instance.ProfileManager.Profile;

        p.Validate();

        p.OnScoreUpdated += CheckCompleted;
    }

    public override void CheckCompleted()
    {
 
        if (AchievementCompleted) { return; }

        if(p.GetTotalScore() < 5000)
        {
            return;
        }

        base.CheckCompleted();

        AchievementCompleted = true;

        p.OnScoreUpdated -= CheckCompleted;

        Manager.TriggerOnAchievementFinished(this);

        Debug.Log(string.Format("Achievement '{0}' completed!", GetAchievementName()));
    }
}