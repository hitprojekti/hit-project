﻿using UnityEngine.SceneManagement;

public class TowelPreparationMiniGame : MiniGame
{
    public TowelPreparationMiniGame(string name, MiniGameEnum e) : base(name, e)
    {
    }

    public override void StartMiniGame()
    {
        base.StartMiniGame();
        SceneManager.LoadScene(2);
    }
}