﻿using System.Collections.Generic;
using UnityEngine;

public class Trash
{
    public Vector3 Position;

    public TrashNode AttachedToNode;
    public float Integrity;

    public bool IsCleaned
    {
        get
        {
            return Integrity <= 0 ? true : false;
        }
    }

    public TrashTypeEnum TrashType;

    private MonoTrashTile TrashTileObject;


    private GameManager Manager
    {
        get
        {
            if(_mgr == null)
            {
                _mgr = GameManager.Instance;
            }

            return _mgr;
        }
    }

    private GameManager _mgr;

    private string GetSprite
    {
        get
        {
            switch (TrashType)
            {
                case TrashTypeEnum.Vomit:
                    return "Trash_3";
                case TrashTypeEnum.Blood:
                    return GetBloodTextureName();
                case TrashTypeEnum.Dust:
                    return "Trash_0";
                case TrashTypeEnum.Loose_Debris:
                    return GetDebrisTextureName();
            }

            return "Null";
        }
    }

    public void SetupTile(TrashTypeEnum type, TrashNode node)
    {
        AttachedToNode = node;
        TrashType = type;

        if(AttachedToNode == null) { return; }

        Position = AttachedToNode.Position + new Vector2(Random.Range(-0, 0), Random.Range(-0, 0));
        TrashTileObject = Manager.Pooler.GetPoolable("TrashTile").Spawn(Position, Quaternion.identity).GetComponent<MonoTrashTile>();

        TrashTileObject.SetSprite("Objects", GetSprite);
        TrashTileObject.RandomizeTransform();

    }

    public void Dispose()
    {
        if(TrashTileObject != null)
        {
            TrashTileObject.Dispose();
        }

        TrashTileObject = null;
    }

    private string GetBloodTextureName()
    {
        string[] str = new string[]
        {
            "Trash_1",
            "Trash_2"
        };

        return str[Random.Range(0, str.Length)];
    }

    private string GetDebrisTextureName()
    {
        string[] str = new string[]
        {
            "Sand",
            "Syringe",
            "Paper"
        };

        int[] weights = new int[]
        {
            25,
            6,
            4
        };

        List<string> strs = new List<string>();

        for (int i = 0; i < weights.Length; i++)
        {
            for (int j = 0; j < weights[i]; j++)
            {
                strs.Add(str[i]);
            }
        }

        return strs[Random.Range(0, strs.Count)];
    }
}