﻿using System.Collections.Generic;
using UnityEngine;

public class TrashGrid
{
    public TrashNode[,] NodeGrid;

    public List<TrashNode> NodeList;

    private const int WIDTH = 20;
    private const int HEIGHT = 40;
    private const float NODE_SIZE = 2;
    private const float GRID_SPACING = 0;

    private GameManager Manager;
    private Vector2 Offset;

    public TrashGrid(Vector2 index)
    {
        Offset = index;
        Manager = GameManager.Instance;
    }


    public void CreateGrid()
    {
     
        float offset = WIDTH % 2 != 0 ?  0 : 0.5f;

        NodeList = new List<TrashNode>();
        NodeGrid = new TrashNode[WIDTH,HEIGHT];

        Vector2 posTemp = Vector2.zero;
        Vector2 v = new Vector2();

        for (int x = 0; x < WIDTH; x++)
        {
            for (int y = 0; y < HEIGHT; y++)
            {
                float sizeX = (NODE_SIZE  * x) / 2 - (NODE_SIZE * 2);
                float sizeY = (NODE_SIZE * y) / 2 - (NODE_SIZE * 4);

                posTemp.x = sizeX - offset;
                posTemp.y = sizeY;

                v.x = (Offset.x * (WIDTH/2)) * (NODE_SIZE / 2) + (GRID_SPACING * Offset.x);
                v.y = (Offset.y * (HEIGHT/2)) * (NODE_SIZE / 2) + (GRID_SPACING * (Offset.y/2));

                NodeGrid[x, y] = new TrashNode((posTemp + v)- new Vector2(WIDTH/2, HEIGHT/2) / NODE_SIZE, false);

      
                NodeList.Add(NodeGrid[x, y]);


            }
        }

    }
}