﻿public class TrashList
{
    public Trash[] Trash;

    public void Dispose()
    {
        for (int i = 0; i < Trash.Length; i++)
        {
            if(Trash[i] == null) { continue; }
            Trash[i].Dispose();
        }
    }

    public void Initialize(int length)
    {
        Trash = new Trash[length]; 
        
    }

    public void CreateTrash(int index, TrashTypeEnum type, TrashNode node)
    {
        Trash[index] = new Trash();
        Trash[index].SetupTile(type, node);
    }
}