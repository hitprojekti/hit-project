﻿using UnityEngine;

public class TrashNode
{
    public Vector2 Position;

    public bool Occupied;


    public TrashNode(Vector2 pos, bool _b)
    {
        Position = pos;
        Occupied = _b;
    }
}