﻿using UnityEngine;
using UnityEngine.UI;

public class UIManager
{

    public AnimationObject[] AnimationObjects;

    public MonoMiniGameButton[] MinigameButtons;

    public UnscaledTimeAnimator PauseMenu;

    public CanvasGroup MainCanvasGroup;

    public Text Score;

    public Transform PreviewImage;

    public UIManager()
    {
        MainCanvasGroup = GameObject.Find("MainCanvas").GetComponent<CanvasGroup>();

        PauseMenu = GameObject.Find("PauseMenu").GetComponent<UnscaledTimeAnimator>();
        Score = GameObject.Find("Score Amount_Pause").GetComponent<Text>();
        PauseMenu.Play("PauseMenuOutInstant_Empty", true, true);

        MinigameButtons = new MonoMiniGameButton[]
        {
            GameObject.Find("Minigame#1").GetComponent<MonoMiniGameButton>(),
            GameObject.Find("Minigame#2").GetComponent<MonoMiniGameButton>(),
            GameObject.Find("Minigame#3").GetComponent<MonoMiniGameButton>(),
            GameObject.Find("Minigame#4").GetComponent<MonoMiniGameButton>(),
            GameObject.Find("Minigame#5").GetComponent<MonoMiniGameButton>(),
        };

        AnimationObjects = new AnimationObject[]
        {
            new AnimationObject("ProfilePage" , GameObject.Find("ProfilePage").GetComponent<Animation>()),
            new AnimationObject("MainMenu" , GameObject.Find("MainMenu_Canvas").GetComponent<Animation>()),
            new AnimationObject("MiniGameMenu" , GameObject.Find("MinigameSelectScreen").GetComponent<Animation>()),
            new AnimationObject("CountDown", GameObject.Find("CountDown").GetComponent<Animation>())
        };

        ToggleMinigameMenuInstant(false);
    }

    public void InitializeUI()
    {
        Input.multiTouchEnabled = false;
        MainCanvasGroup.interactable = true;
        for (int i = 0; i < MinigameButtons.Length; i++)
        {
            MinigameButtons[i].Init();
            MinigameButtons[i].InitialSetup();
        }
    }

    public void UpdateMinigameUI()
    {
        for (int i = 0; i < MinigameButtons.Length; i++)
        {
            MinigameButtons[i].UpdateScores();
        }
        
    }

    public void PlayAnimation(string inp, string animationName)
    {
        Animation anim = GetAnimation(inp);
        if (anim == null) { return; }

        anim.Stop();
        anim.Play(animationName);
    }

    public void StopAnimation(string inp)
    {
        Animation anim = GetAnimation(inp);

        if(anim == null) { return; }

        anim.Stop();
    }

    public void StopAnimation(Animation anim)
    {

        if (anim == null) { return; }

        anim.Stop();
    }

    public Animation GetAnimation(string n)
    {
        for (int i = 0; i < AnimationObjects.Length; i++)
        {
            if(AnimationObjects[i].Name == n)
            {
                return AnimationObjects[i].Animation;
            }
        }

        return null;
    }

    public void ToggleMinigameMenuInstant(bool b)
    {
        if (!b)
        {
            PlayAnimation("MiniGameMenu", "MinigameMenu_Out_Instant");
       
            return;
        }

        PlayAnimation("MiniGameMenu", "MinigameMenu_In");
    }

    public void  ToggleMainMenu(bool b)
    {
        if (!b)
        {
            PlayAnimation("MainMenu", "MainMenu_Out");
            return;
        }


        PlayAnimation("MainMenu", "MainMenu_In");
    }

    public void ToggleMinigameMenu(bool b)
    {
        if (!b)
        {
            PlayAnimation("MiniGameMenu", "MinigameMenu_Out");
            return;
        }


        PlayAnimation("MiniGameMenu", "MinigameMenu_In");
    }
    public void ToggleMainMenuInstant(bool b)
    {
        if (!b)
        {
            PlayAnimation("MainMenu", "MainMenu_Out_Instant");

            return;
        }


        PlayAnimation("MainMenu", "MainMenu_In_Instant");
    }
}