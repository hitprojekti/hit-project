﻿using System.Collections;
using UnityEngine;

public class ZoomToTargetCameraControls : CameraControls
{

    public Camera Cam;

    private IEnumerator LerpIen;

    private GameManager Manager;

    public ZoomToTargetCameraControls(Transform root, CameraControlType type) : base(root, type)
    {
        Cam = Camera.main;
        Manager = GameManager.Instance;
    }

    public override void ResetControls()
    {
        base.ResetControls();
        StopC();
    }


    public override void StartControls()
    {
        ZoomToTargetInstant(Vector2.zero, 20);
        base.StartControls();
    }

    public override void StopControls()
    {
        ZoomToTargetInstant(Vector2.zero, 20);
        base.StopControls();
    }
    

    public void ZoomToTarget(Vector2 vector, float zoom)
    {
        StartC(zoom, vector);
    }

    public void ZoomToTargetInstant(Vector2 vector, float zoom)
    {
        StopC();

        RootObject.position = vector;
        Cam.orthographicSize = zoom;
    }

    private IEnumerator LerpZoom(float to, Vector2 pos)
    {

        float t = 0;
        float lerpTime = 10;

        Vector3 refZer = Vector3.zero;

        while (t < 1)
        {
            t += Time.deltaTime * lerpTime;

            RootObject.position = Vector3.SmoothDamp(RootObject.position, pos, ref refZer, t);
            Cam.orthographicSize = Mathf.SmoothDamp(Cam.orthographicSize, to, t);

            yield return null;
        }
    }


    private void StartC(float to, Vector2 pos)
    {
        StopC();

        LerpIen = LerpZoom(to, pos);
        Manager.MonoManager.StartCoroutine(LerpIen);
    }

    private void StopC()
    {
       if(LerpIen == null) { return; }

        Manager.MonoManager.StopCoroutine(LerpIen);
    }
}