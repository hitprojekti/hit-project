﻿public delegate void OnEmpty();
public delegate void AchievementFinished(Achievement Achievement);
public delegate void OnOutputFloat(float f);
