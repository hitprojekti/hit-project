﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


public class EditorMenu {

    [MenuItem("Assets/Create/ScoreManager")]
    private static void Create()
    {
        if (GameObject.Find("_ScoreManager_")) { return; }

       GameObject go = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Create/_ScoreManager_"));

        go.name = go.name.Replace("(Clone)", "");
    }
}
