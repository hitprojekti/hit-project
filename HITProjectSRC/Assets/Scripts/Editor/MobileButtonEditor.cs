﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

[CustomEditor(typeof(MobileButton))]
[CanEditMultipleObjects]
public class MobileButtonEditor : Editor{

    public override void OnInspectorGUI()
    {
         serializedObject.Update();
           
        EditorGUILayout.PropertyField(serializedObject.FindProperty("image"), true);
        EditorGUILayout.Space();
        EditorGUILayout.PropertyField(serializedObject.FindProperty("interactable"), true);
       

        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.PropertyField(serializedObject.FindProperty("TransitionType"), true);
        EditorGUILayout.Space();
        EditorGUILayout.PropertyField(serializedObject.FindProperty("idleSprite"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("hoveredSprite"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("pressedSprite"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("disabledSprite"), true);

        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.PropertyField(serializedObject.FindProperty("MobileButtonEvents"), true);

        EditorGUILayout.Space();
        EditorGUILayout.PropertyField(serializedObject.FindProperty("PlaySound"), true);

        serializedObject.ApplyModifiedProperties();

        
    }


}
