﻿public enum BankState
{
    Idle,
    Updating,
    FailedUpdating
}