﻿public class AtlasEmptyException : System.Exception
{
    string input;
    public AtlasEmptyException(string inp)
    {
        input = inp;
    }

    public override string Message
    {
        get
        {
            return "Atlas '" + input + "' has 0 spites in it!";
        }
    }
}
