﻿public class AtlasNotFoundException : System.Exception
{
    string input;
    public AtlasNotFoundException(string inp)
    {
        input = inp;
    }

    public override string Message
    {
        get
        {
            return "Atlas '" + input + "' was not found in resources!";
        }
    }
}
