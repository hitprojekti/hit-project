﻿public class PrefabGroupEmptyException : System.Exception
{
    string input;
    public PrefabGroupEmptyException(string inp)
    {
        input = inp;
    }

    public override string Message
    {
        get
        {
            return "Prefab '" + input + "' has 0 prefabs in it!";
        }
    }
}
