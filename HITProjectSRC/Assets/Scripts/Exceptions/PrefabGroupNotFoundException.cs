﻿public class PrefabGroupNotFoundException : System.Exception
{
    string input;
    public PrefabGroupNotFoundException(string inp)
    {
        input = inp;
    }

    public override string Message
    {
        get
        {
            return "Prefab group '" + input + "' was not found in resources!";
        }
    }
}
