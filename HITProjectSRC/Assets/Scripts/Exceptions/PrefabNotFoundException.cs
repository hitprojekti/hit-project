﻿public class PrefabNotFoundException : System.Exception
{
    string input;
    public PrefabNotFoundException(string inp)
    {
        input = inp;
    }

    public override string Message
    {
        get
        {
            return "Prefab '" + input + "' was not found in resources!";
        }
    }
}
