﻿public class SaveFileNotFoundException : System.Exception
{
    string input;
    string saveLocation;

    public SaveFileNotFoundException(string inp, string inp_2)
    {
        input = inp;

        saveLocation = inp_2;
    }

    public override string Message
    {
        get
        {
            return "Save File '" + input + "' was not found in '"+ saveLocation + "'!";
        }
    }
}
