﻿public class SoundNotFoundException : System.Exception
{
    string input;
    public SoundNotFoundException(string inp)
    {
        input = inp;
    }

    public override string Message
    {
        get
        {
            return "Sound '" + input + "' was not found in resources!";
        }
    }
}
