﻿public class SpriteNotFoundException : System.Exception
{
    string input;
    public SpriteNotFoundException(string inp)
    {
        input = inp;
    }

    public override string Message
    {
        get
        {
            return "Sprite '" + input + "' was not found in resources!";
        }
    }
}
