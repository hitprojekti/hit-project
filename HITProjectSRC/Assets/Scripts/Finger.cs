﻿using UnityEngine;

public class Finger
{
    public event OnEmpty OnItemChange;
    public event OnEmpty OnUpdate;

    public MonoFinger Mono;

    public Transform FingerTransform;

    public Item[] Items;

    public Item CurrentItem
    {
        get
        {
            return _Item;
        }

        set
        {
            if(_Item != null)
            {
                _Item.Controls.StopControls();
                _Item.Dispose();
            }

            _Item = value;

            _Item.Visualize();

            _Item.Controls.StartControls();

            _Item.Init();

            TriggerOnItemChange();
        }
    }

    public GameManager Manager
    {
        get
        {
            if(_Manager  == null)
            {
                _Manager = GameManager.Instance;
            }
            return _Manager;
        }
    }

    private Item _Item;
    private GameManager _Manager;

    public Finger(MonoFinger mono, Item[] _Items)
    {
        Mono = mono;
        Items = _Items;

    }

    private void TriggerOnItemChange()
    {
        if(OnItemChange == null) { return; }

        OnItemChange();
    }

    public void StartUpdateLoop()
    {
        Manager.MonoManager.AddUpdateListener(Update);  
    }

    public void Update()
    {

    }

    public Item GetItem(ItemEnum Index)
    {
        for (int i = 0; i < Items.Length; i++)
        {
            if(Items[i].ItemIndex == Index)
            {
                return Items[i];
            }
        }

        return Items[0];
    }
    private void TriggerOnUpdate()
    {
        if(OnUpdate == null) { return; }

        OnUpdate();
    }
}