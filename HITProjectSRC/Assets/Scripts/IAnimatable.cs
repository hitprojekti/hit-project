﻿
public interface IAnimatable {

    void PlayAnimation(string anim);
    void StopAnimation();
    void ImplementSpriteAnimator();

}
