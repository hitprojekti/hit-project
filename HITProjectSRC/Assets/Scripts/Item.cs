﻿using System;
using UnityEngine;

public class Item : IAnimatable
{
    public string ItemName;
    public ItemEnum ItemIndex;

    public GameManager Manager;

    public Controls Controls;

    public SpriteAnimator Animator;

    private string IdleAnimation;
    private SpriteRenderer Renderer;
    private MonoObject Mono;

    public Item(MonoObject _mono,string _itemName, string _animiationIdle, SpriteRenderer _rend, ItemEnum _index, Controls control)
    {
        Controls = control;
        Mono = _mono;
        ItemIndex = _index;
        ItemName = _itemName;
        IdleAnimation = _animiationIdle;
        Renderer = _rend;
        Manager = GameManager.Instance;

        ImplementSpriteAnimator();
    }

    public virtual void Init() { }
    public virtual void Dispose() { }

    public virtual void Visualize() { PlayAnimation(IdleAnimation); }

    public virtual void Use() { }

    public virtual void PlayAnimation(string anim)
    {
        StopAnimation();

        SpriteAnimation sprt = Manager.SpriteBank.GetAnimation(anim);
        if(sprt == null) { Debug.Log("Item: Returning! Reason: SpriteAnimation '" + anim +"' is null"); return; }
        Animator.PlayAnimation(sprt);
    }

    public virtual void StopAnimation()
    {
        Animator.StopAnimation();
    }

    public virtual void ImplementSpriteAnimator()
    {
        Animator = new SpriteAnimator(Mono, Renderer, true);
    }
}