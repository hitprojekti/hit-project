﻿public enum ItemEnum
{
    HAND = 0,
    MOP = 1,
    RAG = 2,
    DUSTPAN = 3,
    CLEANING_CART_HAND = 4
}