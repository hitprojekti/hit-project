﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonoFinger : MonoObject {

    public event OnEmpty OnTrigger;
    public Finger Finger;
    public GameManager Manager;

    public override void Init()
    {
        base.Init();
        Manager = GameManager.Instance;

        Finger = Manager.Finger;
    }



}
