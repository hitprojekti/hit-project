﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Analytics;

public class ScoreManagerMono : MonoObject {

    private string root;
    private string saveRoot;

    public Profile ProfileTemp;

    private GamePlayTimeCalculator Time;

    private void Awake()
    {
        ValidateFileDirectories();
        OnUpdate += CheckKeys;
    }

    // Use this for initialization
    void Start () {

        LoadProfile();

    }

    private void Update()
    {
        TriggerOnUpdate();
    }

    private void CheckKeys()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }


    private void ValidateFileDirectories()
    {
        Debug.Log("ScoreManager: Validating File Directories' Existense.");

        root = Application.persistentDataPath;

        saveRoot = root + "/Saves";

        ExtensionMethods.ValidateDirectorysExistense(GetSaveRoot(), true);

        Debug.Log("ScoreManager: Done Validating File Directories' Existense Successful.");
    }

    private string GetInternalPath()
    {
        AndroidJavaClass activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject activity = activityClass.GetStatic<AndroidJavaObject>("currentActivity");
        return activity.Call<AndroidJavaObject>("getFilesDir").Call<string>("getPath");
    }

    public void GiveScore(int score, MiniGameEnum minigame)
    {
        ProfileTemp.Validate();

      
            if (ProfileTemp.HiScore[(int)minigame] < score)
            {
                ProfileTemp.HiScore[(int)minigame] = score;

                ProfileTemp.ModifyHiScore(ModifiactionType.SET, score, (int)minigame);

            }

            ProfileTemp.ModifyTotalScore(ModifiactionType.ADD, score, (int)minigame);

            ProfileTemp.ModifyTimesPlayed(ModifiactionType.ADD, 1, (int)minigame);
        
      
        SaveProfileToFileTemp();
    }

    public void SaveProfileToFileTemp()
    {
        ExtensionMethods.ToJson(ProfileTemp, GetSaveRoot() + "Profile.json", true);
    }

    public string GetSaveRoot()
    {
        return saveRoot + "/";
    }

    public string GetFileRoot()
    {
        return root + "/";
    }

    public void LoadProfile()
    {
       StartCoroutine(LoadFile());
    }

    IEnumerator LoadFile()
    {

        FileInfo inf = ExtensionMethods.GetFirstFile(GetSaveRoot());

        ProfileTemp = ExtensionMethods.FromJson<Profile>(inf.FullName);

        if(ProfileTemp == null)
        {
            ProfileTemp = new Profile();
            ProfileTemp.Validate();
            Time = new GamePlayTimeCalculator(ProfileTemp, this);
            Time.StartClock();

            yield break;
        }

        yield return null;

        ProfileTemp.Validate();

        yield return null;

        Time = new GamePlayTimeCalculator(ProfileTemp, this);

        yield return null;

        Time.StartClock();
    }

}
