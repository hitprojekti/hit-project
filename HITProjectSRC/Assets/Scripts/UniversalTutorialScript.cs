﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UniversalTutorialScript : MonoBehaviour {

    public bool inTutorial;
    bool _doStuff;

    public GameObject TutorialObject;

    public TutorialType TutType;

    // Use this for initialization
    void Start () {
        inTutorial = true;
        _doStuff = true;

        switch (TutType)
        {
            case TutorialType.Ergonomy:

                break;
        }
    }

 
    public void Disable()
    {
        TutorialObject.SetActive(false);

            Debug.Log("Disable");

        inTutorial = false;
        _doStuff = false;
    }


    public void TutorialButton()
    {
        TutorialObject.SetActive(true);
        inTutorial = true;
    }
}
