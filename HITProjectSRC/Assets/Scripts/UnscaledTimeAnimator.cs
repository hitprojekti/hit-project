﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UnscaledTimeAnimator : MonoObject
{

    public bool useTimeScale;

    public Animation Animation;

    private IEnumerator Ien;

    public void Play(string name, bool b, bool instant = false)
    {

        if (instant)
        {
            Animation.Stop();
            Animation.Play(name);
            return;
        }
        useTimeScale = b;
        if (Ien != null)
        {
            StopCoroutine(Ien);
        }

        Ien = PlayIen(name);

        StartCoroutine(Ien);
    }

    public IEnumerator PlayIen(string clipName)
    {
        yield return null;
        if (!useTimeScale)
        {

            AnimationState _currState = Animation[clipName];
            bool isPlaying = true;
            float _progressTime = 0F;
            float _timeAtLastFrame = 0F;
            float _timeAtCurrentFrame = 0F;
            float deltaTime = 0F;

            Animation.Stop();
            Animation.Play(clipName);

            _timeAtLastFrame = Time.realtimeSinceStartup;
            while (isPlaying)
            {
                _timeAtCurrentFrame = Time.realtimeSinceStartup;
                deltaTime = _timeAtCurrentFrame - _timeAtLastFrame;
                _timeAtLastFrame = _timeAtCurrentFrame;

                _progressTime += deltaTime;
           
                _currState.normalizedTime = _progressTime / _currState.length;
                Animation.Sample();

                //Debug.Log(_progressTime);

                if (_progressTime >= _currState.length)
                {
                    //Debug.Log(&quot;Bam! Done animating&quot;);
                    if (_currState.wrapMode != WrapMode.Loop)
                    {
                        //Debug.Log(&quot;Animation is not a loop anim, kill it.&quot;);
                        //_currState.enabled = false;
                        isPlaying = false;
                    }
                    else
                    {
                        //Debug.Log(&quot;Loop anim, continue.&quot;);
                        _progressTime = 0.0f;
                    }
                }

                yield return new WaitForEndOfFrame();
            }
            yield return null;
         
        }
        else
        {
            Animation.Stop();
            Animation.Play(clipName);
        }
    }
}
