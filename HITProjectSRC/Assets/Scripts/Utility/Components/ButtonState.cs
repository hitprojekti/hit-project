﻿public enum ButtonState
{
    IDLE,
    HOVER,
    DISABLED,
    PRESSED
}