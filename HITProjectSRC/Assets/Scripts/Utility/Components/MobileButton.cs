﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This is a button specifically meant for mobile devices.
/// </summary>
[AddComponentMenu("UI/Mobile Button")]
[ExecuteInEditMode]
public class MobileButton : MonoObject {


    /// <summary>
    /// This is 'MobileButton's' current state.
    /// </summary>
    public ButtonState CurrentButtonState
    {
        get
        {
            return _currentButtonState;
        }

        set
        {
            LastButtonState = CurrentButtonState;

            _currentButtonState = value;


            if(LastButtonState != _currentButtonState)
            {
                UpdateGraphic();
            }
           
        }

      
    }

    public MobileButtonTransitionType TransitionType;

    /// <summary>
    /// This is 'MobileButton's' last state.
    /// </summary>
    public ButtonState LastButtonState;

    /// <summary>
    /// This contains the 'UnityEvents' for 'MobileButton'.
    /// </summary>
    public MobileButtonEvents MobileButtonEvents;

    /// <summary>
    /// While this is false, 'MobileButton' doesn't react to any input.
    /// </summary>
    public bool Interactable
    {
        get
        {
            return _interactable;
        }

        set
        {
            _interactable = value;
           // UpdateInteractableState(_interactable);
        }
    }

    /// <summary>
    /// This is the 'MobileButton's' graphic.
    /// </summary>
    public MaskableGraphic Image
    {

        get
        {
            if(image != null) { return image; }

            Image ImgTemp = GetComponent<Image>();
            Text TextTemp = GetComponent<Text>();
            RawImage RawImgTemp = GetComponent<RawImage>();
            image = null;

            if (ImgTemp != null)
            {
                image = ImgTemp;
            }

            if (TextTemp != null)
            {
                image = TextTemp;
            }

            if (RawImgTemp != null)
            {
                image = RawImgTemp;
            }

            return image;
        }

        set
        {
            image = value;
        }
    }
    [SerializeField]
    private ButtonState _currentButtonState;

    /// <summary>
    /// Serialized Field of MaskableGraphic 'Image'.
    /// </summary>
    [SerializeField]
    private MaskableGraphic image;

    /// <summary>
    /// Private field of boolean 'Interactable'.
    /// </summary>
    private bool _interactable;


    /// <summary>
    /// Serialized field of boolean 'Interactable'.
    /// </summary>
    [SerializeField]
    private bool interactable;

    public bool PlaySound = true;

    public GameManager Manager
    {
        get
        {
            if (_M == null)
            {
                _M = GameManager.Instance;
            }

            return _M;
        }
    }

    private GameManager _M;

    /// <summary>
    /// A boolean that checks if 'MobileButton' can be entered.
    /// </summary>
    private bool CanEnter
    {
        get
        {
            return CurrentButtonState != ButtonState.DISABLED && CurrentButtonState != ButtonState.PRESSED && CurrentButtonState == ButtonState.IDLE;
        }
    }


    /// <summary>
    /// A boolean that checks if 'MobileButton' can be left.
    /// </summary>
    private bool CanLeave
    {
        get
        {
            return CurrentButtonState != ButtonState.DISABLED && CurrentButtonState != ButtonState.IDLE;
        }
    }

    /// <summary>
    /// A boolean that checks if 'MobileButton' can be released.
    /// </summary>
    private bool CanRelease
    {
        get
        {
            return CurrentButtonState == ButtonState.PRESSED;
        }
    }

    /// <summary>
    /// A boolean that checks if 'MobileButton' can be pressed down.
    /// </summary>
    private bool CanPress
    {
        get
        {
            return CurrentButtonState != ButtonState.DISABLED && CurrentButtonState != ButtonState.PRESSED;
        }
    }

    [SerializeField]
    private Sprite hoveredSprite, pressedSprite, idleSprite, disabledSprite;



    /// <summary>
    /// Use this to transition to a 'ButtonState' from current 'MobileButton's' state.
    /// </summary>
    /// <param name="state"></param>
    public void TransitionToState(ButtonState state)
    {
        CurrentButtonState = state;
    }

    /// <summary>
    /// This is triggered when the 'MobileButton' is pressed down.
    /// </summary>
    public new void OnMouseDown()
    {
       
        if (!CanPress) { return; }

        if (PlaySound)
        {
            Manager.AudioManager.PlaySoundEffect(1f, MobileButtonEvents.OnPressedSound);
        }
     
        MobileButtonEvents.OnPressed.Invoke();
        TransitionToState(ButtonState.PRESSED);
    }

    /// <summary>
    /// This is triggered when the 'MobileButton' is released.
    /// </summary>
    public new void OnMouseUp()
    {
      
        if(!CanRelease) { return; }

        if (PlaySound)
        {
            Manager.AudioManager.PlaySoundEffect(1f, MobileButtonEvents.OnReleasedSound);
        }
        MobileButtonEvents.OnReleased.Invoke();
        TransitionToState(LastButtonState);
    }

    /// <summary>
    /// This is triggered when the 'MobileButton' is entered.
    /// </summary>
    public new void OnMouseEnter()
    {
        if (!CanEnter) { return; }
        if (PlaySound)
        {
            if(Manager != null && Manager.AudioManager != null)
            {
                Manager.AudioManager.PlaySoundEffect(1f, MobileButtonEvents.OnEnterSound);
            }
    
        }
        MobileButtonEvents.OnEnter.Invoke();
        TransitionToState(ButtonState.HOVER);
    }

    /// <summary>
    /// This is triggered when the 'MobileButton' is exited.
    /// </summary>
    public new void OnMouseExit()
    {
       
        if (!CanLeave) { return; }

        if (PlaySound)
        {
            Manager.AudioManager.PlaySoundEffect(1f, MobileButtonEvents.OnLeaveSound);
        }

        MobileButtonEvents.OnExit.Invoke();
        TransitionToState(ButtonState.IDLE);
    }

    /// <summary>
    /// This is triggered when the 'MobileButton' is left.
    /// </summary>
    public void OnMouseLeave()
    {
   
        if (!CanLeave) { return; }

        if (PlaySound)
        {
            Manager.AudioManager.PlaySoundEffect(1f, MobileButtonEvents.OnLeaveSound);
        }

        MobileButtonEvents.OnLeave.Invoke();
        TransitionToState(ButtonState.IDLE);
    }


    public Sprite GetIdleSprite
    {
        get
        {
            return idleSprite;
        }

        set
        {
            idleSprite = value;
        }

    }

    public Sprite GetHoverSprite
    {
        get
        {
            return hoveredSprite;
        }

        set
        {
            hoveredSprite = value;
        }
    }

    public Sprite GetPressedSprite
    {
        get
        {
            return pressedSprite;
        }

        set
        {
            pressedSprite = value;
        }
    }

    public Sprite GetDisabledSprite
    {
        get
        {
            return disabledSprite;
        }

        set
        {
            disabledSprite = value;
        }
    }

    /// <summary>
    /// Initializes 'MobileButton' values.
    /// </summary>
    public void Awake()
    {
        CurrentButtonState = ButtonState.IDLE;
        UpdateInteractableState(interactable);
    }

    /// <summary>
    /// Resets 'MobileButton' values.
    /// </summary>
    public void Reset()
    {

        image = Image;
        _interactable = Interactable;
        CurrentButtonState = ButtonState.IDLE;
    }

    public void UpdateGraphic()
    {
        if (Image == null || TransitionType == MobileButtonTransitionType.NONE) { return; }

        Image imgTemp = Image.GetComponent<Image>();

        if (imgTemp == null) { return; }

        switch (CurrentButtonState)
        {
            case ButtonState.DISABLED:
                if (disabledSprite == null) { return; }
                imgTemp.sprite = disabledSprite;

                return;

            case ButtonState.HOVER:
                if (hoveredSprite == null) { return; }
                imgTemp.sprite = hoveredSprite;

                return;

            case ButtonState.PRESSED:
                if (pressedSprite == null) { return; }
                imgTemp.sprite = pressedSprite;

                return;

        }
        if (idleSprite == null) { return; }
        imgTemp.sprite = idleSprite;

    }

    ///////////////////
    /*Private Methods*/
    ///////////////////



    private void LateUpdate()
    {
        CheckInteractable();
    }

    private void CheckInteractable()
    {

        if (Interactable != interactable)
        {     
            Interactable = interactable;
            UpdateInteractableState(Interactable);
        }
    }

    private void UpdateInteractableState(bool b)
    {
        if (!b)
        {
            if(CurrentButtonState == ButtonState.DISABLED) { return; }

            CurrentButtonState = ButtonState.DISABLED;
            return;
        }

        if (CurrentButtonState != ButtonState.DISABLED) { return; }

        CurrentButtonState = ButtonState.IDLE;
    }

 

}
