﻿using UnityEngine.Events;


[System.Serializable]
public class MobileButtonEvents
{
    public UnityEvent OnEnter;
    public string OnEnterSound = "";

    public UnityEvent OnExit;
    public string OnExitSound = "";

    public UnityEvent OnPressed;
    public string OnPressedSound = "";

    public UnityEvent OnReleased;
    public string OnReleasedSound = "";

    public UnityEvent OnLeave;
    public string OnLeaveSound = "";


}