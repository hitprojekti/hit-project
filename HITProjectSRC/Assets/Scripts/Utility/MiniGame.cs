﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class MiniGame {

    public event OnEmpty OnHiScoreChange;

    public string MiniGameName;

    public int CurrentScoreAmount;

    public MiniGameEnum MiniGameIndex;

    public GameManager Manager;

    public MiniGame(string name, MiniGameEnum e)
    {
        Manager = GameManager.Instance;

        MiniGameName = name;
        MiniGameIndex = e;      
    }

    public virtual void StartMiniGame()
    {
        Analytics.CustomEvent(string.Format("Minigame Started: {0}", MiniGameIndex.ToString()));

        Manager.UIManager.PauseMenu.Play("PauseMenuOutInstant", true);
        CurrentScoreAmount = 0;
    }
    public virtual void ResetMiniGame() { CurrentScoreAmount = 0; }
    public virtual void CloseMiniGame() { Manager.UIManager.PauseMenu.Play("PauseMenuOutInstant_Empty", true); Manager.ProfileManager.StopGamePlayTime(); Manager.ProfileManager.Profile.ModifyTimesPlayed(ModifiactionType.ADD, 1, (int)MiniGameIndex); Manager.ProfileManager.SaveProfileToFile(); Analytics.CustomEvent(string.Format("Minigame Stopped: {0} | Times played: {1}", MiniGameIndex.ToString(), Manager.ProfileManager.Profile.TimesPlayed[(int)MiniGameIndex])); }


    private void TriggerOnHiScoreChange()
    {
        if(OnHiScoreChange == null) { return; }

        OnHiScoreChange();

    }
}
