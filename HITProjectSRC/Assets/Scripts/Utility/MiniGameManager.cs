﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniGameManager {

    public GameManager Manager;


    public MiniGame[] MiniGames;

    public MiniGame CurrentMiniGame;

    public MiniGameManager(MiniGame[] _Ms)
    {
        CurrentMiniGame = null;
        Manager = GameManager.Instance;

        MiniGames = _Ms;

   
    }

    public void UpdateMinigameScoreUI()
    {
        Manager.UIManager.UpdateMinigameUI();
    }

    public MiniGame GetMiniGame(MiniGameEnum en)
    {
        if(MiniGames == null) { return null; }

        for (int i = 0; i < MiniGames.Length; i++)
        {
            if(MiniGames[i].MiniGameIndex == en)
            {
                return MiniGames[i];
            }
        }

        return MiniGames[0];
    }

    public void StartMiniGame(int index)
    {
        CurrentMiniGame = GetMiniGame((MiniGameEnum)index);
        CurrentMiniGame.StartMiniGame();
    }

    public void CloseMiniGame()
    {
        if(CurrentMiniGame == null) { return; }
        CurrentMiniGame.CloseMiniGame();
    }
}
