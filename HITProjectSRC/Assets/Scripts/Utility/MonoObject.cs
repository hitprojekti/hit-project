﻿using System;
using System.Linq;
using UnityEngine;

public class MonoObject : MonoBehaviour
{
    public OnEmpty OnMouseUpEvent;
    public OnEmpty OnMouseDownEvent;
    public OnEmpty OnMouseExitEvent;
    public OnEmpty OnMouseEnterEvent;


    public OnEmpty OnUpdate;

    private Delegate[] dels;

    public virtual void Init() { }
    public virtual void Dispose() { transform.SetParent(null); gameObject.SetActive(false); }
    public virtual void UpdateLoop() { }

    public void OnMouseEnter() { TriggerOnMouseEnter(); }

    public void OnMouseExit() { TriggerOnMouseExit(); }

    public void OnMouseDown() { TriggerOnMouseDown(); }

    public void OnMouseUp() { TriggerOnMouseUp(); }

    public void TriggerOnUpdate()
    {
        if(OnUpdate == null) { return; }

        OnUpdate();
    }

    public void TriggerOnMouseUp()
    {
        if (OnMouseUpEvent == null) { return; }

        OnMouseUpEvent();
    }


    public void TriggerOnMouseDown()
    {
        if (OnMouseDownEvent == null) { return; }

        OnMouseDownEvent();
    }

    public void TriggerOnMouseEnter()
    {
        if (OnMouseEnterEvent == null) { return; }

        OnMouseEnterEvent();
    }


    public void TriggerOnMouseExit()
    {
        if (OnMouseExitEvent == null) { return; }

        OnMouseExitEvent();
    }



    public void AddUpdateListener(OnEmpty In)
    {
        if (AssureListener(In, OnUpdate))
        {
            OnUpdate -= In;
        }

        OnUpdate += In;
    }

    public void RemoveUpdateListener(OnEmpty In)
    {
        if (!AssureListener(In, OnUpdate)) { return; }

        OnUpdate -= In;
    }

    private bool AssureListener(OnEmpty IN, OnEmpty events)
    {
        if(events == null) { return false; }
        dels = events.GetInvocationList();

        return dels.Contains(IN);

    }
}