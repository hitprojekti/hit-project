﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPool {

    public List<Poolable> Pools;

    public ObjectPool()
    {
        CreatePoolables();
    }

    public Poolable GetPoolable(string name)
    {
        for (int i = 0; i < Pools.Count; i++)
        {
            if(Pools[i].poolName == name)
            {
                return Pools[i];
            }
        }

        return Pools[0];
    }

    public void CreateInitialPoolables()
    {
        for (int i = 0; i < Pools.Count; i++)
        {
            for (int j = 0; j < Pools[i].initialAmount; j++)
            {
                Pools[i].Add();
            }
        }

        Debug.Log("Object Pooler: Object Pooler Created Successfully.");
        Debug.Log(string.Format("Object Pooler: Created {0} 'Poolables'.", Pools.Count));
    }

    private void CreatePoolables()
    {
        Pools = new List<Poolable>();

    }
 
    
}
