﻿using System.Collections.Generic;
using UnityEngine;

public class Poolable
{
    public string poolName
    {
        get
        {
            return Prefab.name;
        }
    }

    public GameObject Prefab;
    public int initialAmount;

    public List<GameObject> Objects;

    public Poolable(GameObject _go, int _initialCount)
    {
        Prefab = _go;
        initialAmount = _initialCount;
    }

    public GameObject Spawn(Vector3 pos, Quaternion rotation)
    {
        int selected = 0;
        for (int i = 0; i < Objects.Count; i++)
        {
            if (Objects[i].activeSelf)
            {
                if (i >= Objects.Count - 1)
                {
                    Add();
                }

                continue;
            }

            Objects[i].transform.position = pos;
            Objects[i].transform.rotation = rotation;
            Objects[i].SetActive(true);

            selected = i;
            break;
        }


        return Objects[selected];
    }

    public void Add()
    {
        if (Objects == null)
        {
            Objects = new List<GameObject>();
        }

        GameObject go = MonoBehaviour.Instantiate(Prefab);

        if (go.GetComponent<MonoObject>())
        {
            go.GetComponent<MonoObject>().Init();
        }

        go.SetActive(false);
       


        Objects.Add(go);


    }

}