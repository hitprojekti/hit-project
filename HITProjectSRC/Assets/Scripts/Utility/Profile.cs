﻿using System;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class Profile
{
    public OnEmpty OnScoreUpdated;

    public string ProfileName;
    public string ProfilePicName;

    [System.NonSerialized]
    public RankEnum Rank;

    public int[] HiScore;
  
    public int[] TotalScore;

    public int[] TimesPlayed;
  
    public long StartDate;
    public long PlayTime;

    public bool Music;
    public bool SFX;

    public bool[] AchievementsCompleted;

    
    public void Validate()
    {
        GameManager m = GameManager.Instance;

        int minigames = 5;

        if (HiScore == null || HiScore.Length < minigames)
        {
            HiScore = new int[minigames];
        }

        if (TotalScore == null || TotalScore.Length < minigames)
        {
            TotalScore = new int[minigames];
        }

        if (TimesPlayed == null || TimesPlayed.Length < minigames)
        {
            TimesPlayed = new int[minigames];
        }


        int achievements = 200;

        if(m != null) { achievements = achievements = m.AchievementManager.Achievements.Length; }

        if (AchievementsCompleted == null || AchievementsCompleted.Length < achievements)
        {
            AchievementsCompleted = new bool[achievements];

            for (int i = 0; i < AchievementsCompleted.Length; i++)
            {
                AchievementsCompleted[i] = false;
            }
        }
       
    }

    public void ModifyTotalScore(ModifiactionType type, int value, int index)
    {
        switch (type)
        {
            case ModifiactionType.ADD:
                TotalScore[index] += value;
                break;

            case ModifiactionType.SET:
                TotalScore[index] = value;
                break;
        }

        SetRank();
        TriggerOnScoreUpdated();
    }

    public void ModifyHiScore(ModifiactionType type, int value, int index)
    {
        switch (type)
        {
            case ModifiactionType.ADD:
                HiScore[index] += value;
                break;

            case ModifiactionType.SET:
                HiScore[index] = value;
                break;
        }
    }

    public void ModifyTimesPlayed(ModifiactionType type, int value, int index)
    {
        switch (type)
        {
            case ModifiactionType.ADD:
                TimesPlayed[index] += value;
                break;

            case ModifiactionType.SET:
                TimesPlayed[index] = value;
                break;
        }
    }


    public string ConvertRankEnumToString()
    {
        switch (Rank)
        {
            case RankEnum.NONE:
                return "Rank 0";

            case RankEnum.RANK_1:
                return "Rank 1";

            case RankEnum.RANK_2:
                return "Rank 2";

            case RankEnum.RANK_3:
                return "Rank 3";

            case RankEnum.RANK_4:
                return "Rank 4";

            case RankEnum.RANK_5:
                return "Rank 5";

            case RankEnum.RANK_6:
                return "Rank 6";

            case RankEnum.RANK_7:
                return "Rank 7";

            case RankEnum.RANK_8:
                return "Rank 8";

            case RankEnum.RANK_9:
                return "Rank 9";

            case RankEnum.RANK_10:
                return "Rank 10";
        }
        return "";
    }

    public string GetPlayedTime()
    {
        long seconds = PlayTime;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;

        string temp = "";

        if (Mathf.Abs(seconds) < 60)
        {
            temp = string.Format("{0} Seconds", seconds);
            return temp;
        }

        if (Mathf.Abs(minutes) < 60)
        {
            temp = string.Format("{0} m {1} s", minutes, (Mathf.Floor((float)seconds % 59.999f)));
            return temp;
        }

        if(Mathf.Abs(hours) < 24)
        {
            temp = string.Format("{0} h {1} m",hours, (Mathf.Floor((float)minutes % 59.999f)));
            return temp;
        }

        temp = string.Format("{0} d {1} h", days, (Mathf.Floor((float)hours % 23.999f)));
        return temp;

    }

    public string GetStartDate()
    {
        DateTime StartDateTime = DateTime.FromBinary(StartDate);
        return StartDateTime.ToString("dd/MM/yyyy");
    }

    public long GetTotalScore()
    {
        return (long)TotalScore.Sum();
    }

    public void SetRank()
    {
        long tot = GetTotalScore();
 
        if (tot >= 3000000)
        {
            Rank = RankEnum.RANK_10;
            return;
        }

        if (tot >= 2000000)
        {
            Rank = RankEnum.RANK_9;
            return;
        }

        if (tot >= 1500000)
        {
            Rank = RankEnum.RANK_8;
            return;
        }


        if (tot >= 1000000)
        {
            Rank = RankEnum.RANK_7;
            return;
        }

        if (tot >= 500000)
        {
            Rank = RankEnum.RANK_6;
            return;
        }

        if (tot >= 250000)
        {
            Rank = RankEnum.RANK_5;
            return;
        }

        if (tot >= 100000)
        {
            Rank = RankEnum.RANK_4;
            return;
        }

        if (tot >= 50000)
        {
            Rank = RankEnum.RANK_3;
            return;
        }

        if (tot >= 25000)
        {
            Rank = RankEnum.RANK_2;
            return;
        }

        if (tot >= 5000)
        {
            Rank = RankEnum.RANK_1;
            return;
        }

        if (tot <= 0)
        {
            Rank = RankEnum.NONE;
            return;
        }

    }

    private void TriggerOnScoreUpdated()
    {
        if(OnScoreUpdated == null) { return; }

        OnScoreUpdated();
    }
}