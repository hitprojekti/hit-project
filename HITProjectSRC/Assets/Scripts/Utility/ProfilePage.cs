﻿using UnityEngine.UI;
using System;
using UnityEngine;
using System.Globalization;

public class ProfilePage
{
    public Text RankText;
    public Text InfoBoxText;

    public MobileToggle MusicOn;
    public MobileToggle SFXOn;


  
    private Profile profile;
    private GameManager Manager;

    private string _baseInfoBoxText = "Total Score\n{0}\n\nStart Date\n{1}\n\nPlay Time\n{2}";

  


    public ProfilePage(InputField _profName, Text _rankText, Text _infoBox, Image _profImg, MobileToggle _mus, MobileToggle _sfx)
    {
     
        Manager = GameManager.Instance;

        RankText = _rankText;
        InfoBoxText = _infoBox;



        MusicOn = _mus;
        SFXOn = _sfx;


    }

    public void SetupProfilePage(Profile _profile)
    {
        profile = _profile;
        RankText.text = profile.ConvertRankEnumToString();
    
        MusicOn.isOn = profile.Music;
        SFXOn.isOn = profile.SFX;


        UpdateInfoBox();
    }



    public void UpdateInfoBox()
    {
        string tempString = _baseInfoBoxText;

        NumberFormatInfo f = new NumberFormatInfo { NumberGroupSeparator = " " };
        string totScore = profile.GetTotalScore().ToString("n", f);

        totScore = totScore.Remove(totScore.LastIndexOf("."));

        InfoBoxText.text = string.Format(tempString, totScore, profile.GetStartDate(), profile.GetPlayedTime());
    }



}