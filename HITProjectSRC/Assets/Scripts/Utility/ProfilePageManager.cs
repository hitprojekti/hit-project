﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class ProfilePageManager {

    public Profile Profile;
    public ProfilePage ProfilePage;

    public bool ProfilePageOpen;

    private GameManager Manager;

    public ProfilePageManager()
    {
        Manager = GameManager.Instance;

        ProfilePage = new ProfilePage
            (
              null,
              GameObject.Find("RankTextField").GetComponent<Text>(),
              GameObject.Find("InfoBoxField").GetComponent<Text>(),
              null,
              GameObject.Find("MusicON").GetComponent<MobileToggle>(),
              GameObject.Find("SFXON").GetComponent<MobileToggle>()
            );
    }

    public void CreateBaseProfile()
    {
        Profile = new Profile();

        Profile.ProfileName = "";
        Profile.ProfilePicName = "SelectPicture";
        Profile.Rank = RankEnum.NONE;

        Profile.StartDate = DateTime.Now.ToBinary();

        Profile.Validate();
 
        Profile.SFX = true;
        Profile.Music = true;

        Profile.PlayTime = 0;
      
    }

    public void UpdateUI()
    {
        ProfilePage.SetupProfilePage(Profile);
        UpdateTime();     
    }

    public void PlayTimeCalculatorStart()
    {
        Manager.GamePlayTimeCalculator.StartClock();
    }

    public void UpdateTime()
    {
        ProfilePage.UpdateInfoBox();
    }

    public void SaveProfileToFile()
    {
        ExtensionMethods.ToJson(Profile, Manager.GetSaveRoot() + "Profile.json", true);

    }


    public IEnumerator GetSaveFiles()
    {

        FileInfo inf = ExtensionMethods.GetFirstFile(Manager.GetSaveRoot());

        if (inf != null && inf.Exists)
        {
            
            ToggleProfilePageInstant(false);
            Manager.UIManager.ToggleMainMenuInstant(true);
            LoadProfile(inf.FullName);
            yield break;
        }

        ToggleProfilePageInstant(false);
        Manager.UIManager.ToggleMainMenuInstant(true);

        CreateBaseProfile();

        while(Profile == null)
        {
            yield return null;
        }
        yield return null;
        SaveProfileToFile();

        inf = ExtensionMethods.GetFirstFile(Manager.GetSaveRoot());

        LoadProfile(inf.FullName);

    }

    public void ToggleProfilePage(bool toggle)
    {
        ProfilePageOpen = toggle;


        if (toggle)
        {
            UpdateUI();
            Manager.UIManager.PlayAnimation("ProfilePage", "ProfilePage_In");
            return;
        }

        Manager.UIManager.PlayAnimation("ProfilePage", "ProfilePage_Out");
    }

    public void ToggleCamera(bool b)
    {
        Manager.MonoManager.StartCoroutine(ToggleCamSequence(b));
    }


    public void SetSFX(bool b)
    {
        Profile.SFX = b;

        UpdateUI();
    }

    public void SetMusic(bool b)
    {
        Profile.Music = b;

        UpdateUI();
    }

    public void OpenProfileImageSelector(bool b)
    {
        ToggleProfilePage(!b);

        if (b)
        {
            Manager.UIManager.PlayAnimation("CameraUI", "CameraUI_In");
            return;
        }
        Manager.UIManager.PlayAnimation("CameraUI", "CameraUI_Out");
    }

    private IEnumerator ToggleCamSequence(bool b)
    {

        if (!b)
        {
            Manager.UIManager.PlayAnimation("CameraUI", "CameraUI_Flip_To_Select"); 
 
            yield break;
        }


        Manager.UIManager.PlayAnimation("CameraUI", "CameraUI_Flip_To_Cam");

        yield return new WaitForSeconds(0.15f);
 
    }

  

    public void ToggleProfilePageInstant(bool toggle)
    {
        ProfilePageOpen = toggle;

        if (toggle)
        {
            Manager.UIManager.PlayAnimation("ProfilePage", "ProfilePage_In_Instant");

            return;
        }

        Manager.UIManager.PlayAnimation("ProfilePage", "ProfilePage_Out_Instant");
    }

    public void StopGamePlayTime()
    {
        Manager.GamePlayTimeCalculator.StopC();
    }

    public void LoadProfile(string input)
    {
        Manager.MonoManager.StartCoroutine(LoadFile(input));
    }

    IEnumerator LoadFile(string input)
    {
        Profile = ExtensionMethods.FromJson<Profile>(input);
      
        yield return null;

        Profile.Validate();

        yield return null;

        StopGamePlayTime();

        Profile.SetRank();

        UpdateUI();

        if(Manager.GamePlayTimeCalculator == null)
        {
            Manager.GamePlayTimeCalculator = new GamePlayTimeCalculator(Profile, Manager.MonoManager);
        }
    }

}
