﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ProfilePictureDatabase
{


    public List<ProfileImage> AllPortraits
    {
        get
        {
            List<ProfileImage> temp = new List<ProfileImage>();
            temp = DefaultPortraits.ToList();

            if(PlayerPortraits == null) { return temp; }

            for (int i = 0; i < PlayerPortraits.Count; i++)
            {
                temp.Add(PlayerPortraits[i]);
            }

            return temp;
        }
    }

    private List<ProfileImage> AllPortraitsCleared
    {
        get
        {
            List<ProfileImage> temp = new List<ProfileImage>();
            temp = AllPortraits;

            for (int i = 0; i < temp.Count; i++)
            {
                if(!temp[i].HasEntry)
                {
                    temp.RemoveAt(i);
                    break;
                }
              
            }

            return temp;
        }
    }

    public List<GameObject> GameObjects;
    public ProfileImage[] DefaultPortraits;
    public List<ProfileImage> PlayerPortraits;

    public GameManager Manager;

    private Transform ParentPics;

    public ProfilePictureDatabase()
    {
        GameObjects = new List<GameObject>();
        ParentPics = GameObject.Find("ProfilePicturesRoot").GetComponent<Transform>();
        ParentPics.GetComponent<GridLayoutGroup>().enabled = true;
        Manager = GameManager.Instance;

        UpdateDefaults();
        ReloadPlayerPortraits();

        UpdateGameObjects();
    }

    public void UpdateGameObjects()
    {
        int isS = GameObjects.Count - AllPortraitsCleared.Count;
        if (isS < 0)
        {
            for (int i = 0; i < Mathf.Abs(isS); i++)
            {
                GameObject gO = Manager.Pooler.GetPoolable("ProfilePictureIObject").Spawn(Vector2.zero, Quaternion.identity);
                gO.transform.SetParent(ParentPics);
                gO.transform.localScale = Vector3.one;
                gO.transform.localPosition = Vector2.zero;
                gO.transform.rotation = ParentPics.rotation;
                GameObjects.Add(gO);
            }
        }
        if(isS > 0)
        {
            for (int i = 0; i < Mathf.Abs(isS); i++)
            {
                int index = GameObjects.Count - 1;
                GameObjects[index].GetComponent<MonoObject>().Dispose();
                GameObjects.RemoveAt(index);
            }
        }


        for (int i = 0; i < GameObjects.Count; i++)
        {
            GameObjects[i].GetComponent<MonoProfilePictureObject>().SetSprite(AllPortraitsCleared[i]);
        }
    }

    public void UpdateDefaults()
    {
        Sprite[] temp = Manager.SpriteBank.GetSprites("ProfilePictures");

        DefaultPortraits = new ProfileImage[temp.Length];
        for (int i = 0; i < temp.Length; i++)
        {
            bool entry = temp[i].name != "SelectPicture";
            DefaultPortraits[i] = new ProfileImage(temp[i].name, temp[i], entry, false);
        }
    }

    public void RemoveImage(string img)
    {
        for (int i = 0; i < PlayerPortraits.Count; i++)
        {
            if(PlayerPortraits[i].Path == img)
            {
                ExtensionMethods.RemoveFile(Manager.GetProfilePicSaveRoot() + img);
                break;
            }
        }
    }

    public void ReloadPlayerPortraits()
    {
        PlayerPortraits = new List<ProfileImage>();


       FileInfo[] infs = ExtensionMethods.GetFiles(Manager.GetProfilePicSaveRoot(), "*.jpg");

       Texture2D[] textures = new Texture2D[infs.Length];

        for (int i = 0; i < textures.Length; i++)
        {
            textures[i] = ExtensionMethods.LoadTexture(infs[i].FullName);
        }

        Sprite temp = null;
        for (int i = 0; i < textures.Length; i++)
        {
            temp = Sprite.Create(textures[i], new Rect(0,0, textures[i].width, textures[i].height), (Vector2.one / 2.0f));
            PlayerPortraits.Add(new ProfileImage(infs[i].Name, temp, true, true));
        }

    }


    public Sprite GetSprite(string s)
    {
        Sprite sprtDef = null;
        for (int i = 0; i < AllPortraits.Count; i++)
        {
            if(AllPortraits[i].Path == "SelectPicture")
            {
                sprtDef = AllPortraits[i].ActualSprite;
            }
            if(AllPortraits[i].Path == s)
            {
                return AllPortraits[i].ActualSprite;
            }
        }

        return sprtDef;
    }
}