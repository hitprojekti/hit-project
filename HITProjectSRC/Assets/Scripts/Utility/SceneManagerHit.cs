﻿using System.Collections;
using UnityEngine.SceneManagement;

public static class SceneManagerHit
{

    private static GameManager Manager
    {
        get
        {
            if(_manager == null)
            {
                _manager = GameManager.Instance;
            }

            return _manager;
        }
    }

    private static GameManager _manager;

    public static void ChangeScene(int sceneID)
    {
      
        SceneManager.LoadScene(sceneID);


    }

  
}