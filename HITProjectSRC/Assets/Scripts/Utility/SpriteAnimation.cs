﻿using UnityEngine;

public class SpriteAnimation
{

    public string animationName;

    public int LoopCount;

    public Sprite[] Frames;

    public float AnimationRate
    {
        get
        {
            if(Frames == null || Frames.Length <= 0) { return 0.3333f * AnimationSpeed; }
            return (Frames.Length / 30.0f) * AnimationSpeed;
        }
    }

    public float AnimationSpeed = 1;


    public int FrameCount
    {
        get
        {
            return Frames.Length;
        }
    }


    public SpriteAnimation() { }

    public SpriteAnimation(string _animationName, Sprite[] _frames, float _animationSpeed, int _loopCount)
    {
        animationName = _animationName;
        Frames = _frames;
        AnimationSpeed = _animationSpeed;
        LoopCount = _loopCount;
    }

    public SpriteAnimation Clone()
    {
        SpriteAnimation temp = new SpriteAnimation();

        temp.animationName = animationName;
        temp.Frames = Frames;
        temp.AnimationSpeed = AnimationSpeed;
        temp.LoopCount = LoopCount;

        return temp;
    }

    public Sprite GetFrame(int index)
    {
        index = Mathf.Clamp(index, 0, FrameCount);

        return Frames[index];
    }

}