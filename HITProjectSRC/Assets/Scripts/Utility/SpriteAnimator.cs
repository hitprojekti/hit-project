﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteAnimator {

    public MonoObject MonoObject;

    public SpriteAnimation CurrenAnimation;

    public SpriteRenderer renderer;

    public bool IsPlaying;

    public float AnimatorSpeed
    {
        get
        {
            
         return animatorSpeed; 

           
        }
        set
        {
            animatorSpeed = value;
      
        }

    }

    public float AnimatorAnimationSpeed
    {
        set
        {
            waitForScaled = new WaitForSeconds(value * CurrenAnimation.AnimationRate);
            waitForUnScaled = new WaitForSecondsRealtime(value * CurrenAnimation.AnimationRate);
        }
    }


    public bool useScaledTime;

    private bool _looping;

    private float animatorSpeed;

    private IEnumerator ien;

    private WaitForSeconds waitForScaled;
    private WaitForSecondsRealtime waitForUnScaled;

    public SpriteAnimator()
    {
        IsPlaying = false;
    }

    public SpriteAnimator(MonoObject obj, SpriteRenderer rend, bool scaled, float speed = 1)
    {

        AnimatorSpeed = speed;
        MonoObject = obj;
        renderer = rend;
        useScaledTime = scaled;
        IsPlaying = false;
    }



    public void PlayAnimation(SpriteAnimation anim)
    {
        if(anim == null) { return; }
        StopAnimation();
        CurrenAnimation = anim.Clone();

        AnimatorAnimationSpeed = AnimatorSpeed;
        Play();

    }

    public void StopAnimation()
    {
        Stop();
    }

    private void Play()
    {
        if(MonoObject == null) { return; }

        Stop();

        ien = AnimationPlayer();

        MonoObject.StartCoroutine(ien);
    }

    private void Stop()
    {
        if(ien == null || MonoObject == null) { return; }

        MonoObject.StopCoroutine(ien);
        IsPlaying = false;
    }

    private IEnumerator AnimationPlayer()
    {
    
        int loopCount = CurrenAnimation.LoopCount;
        int FrameCount = CurrenAnimation.Frames.Length;

        float SpdRef = AnimatorSpeed;

        int currentLoops = -1;

        _looping = loopCount < 0;

        IsPlaying = true;
   
        while (true)
        {
            if (currentLoops >= loopCount && !_looping)
            {

                break;
            }
          
            for (int i = 0; i < FrameCount; i++)
            {

                Render(CurrenAnimation.GetFrame(i));
                if (useScaledTime)
                {
                    yield return waitForScaled;
                }
                else
                {
                    yield return waitForUnScaled;
                }
            }
            if (!_looping)
            { 
            currentLoops++;
            }
        }
        Stop();
    }

    private void Render(Sprite spr)
    {
        if(renderer == null) { return; }
        renderer.sprite = spr;
    }
}