﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ToolSelector : MonoBehaviour {

    public int Points = 2500;

   

    public Transform Root;

    public CleaningTool[] Tools;

    public GameObject[] ReferencesNeeded, ReferenceNotNeeded;

    public Sprite SpriteWrong;

    public Button Btn;

    public GameObject Main, Items;

    public GameObject Game;

    public int CurrentIndex;
   

    public bool Correct
    {
        get
        {
            for (int i = 0; i < Tools.Length; i++)
            {
                if (Tools[i].isNeeded && !Tools[i].isSelected)
                {
                    Debug.Log(Tools[i].Toggle.transform.parent.name);
                    return false;
                }

                if (!Tools[i].isNeeded && Tools[i].isSelected)
                {
                    return false;
                }
        
            }



            return true;
        }
    }

    // Use this for initialization
    void Start() {

        Items.SetActive(false);
        Game.SetActive(false);

        StopCoroutine("Randomize");
        StartCoroutine("Randomize");


    }

    public void Continue()
    {
        switch (Correct)
        {
            case true:

                StopCoroutine("End");
                StartCoroutine("End");

                break;

            case false:
                for (int i = 0; i < Tools.Length; i++)
                {
                    if (!Tools[i].isNeeded && Tools[i].isSelected)
                    {
                        (Tools[i].Toggle.graphic as Image).sprite = SpriteWrong;
                    }
                }

                Points /= 2;
                Tools = null;
                StopCoroutine("Reset");
                StartCoroutine("Reset");
                break;
        }
    }

    private IEnumerator Reset()
    {
        StopCoroutine("Randomize");

        Btn.interactable = false;
        yield return new WaitForSeconds(1.0f);

        StopCoroutine("Randomize");
        StartCoroutine("Randomize");
    }


    public IEnumerator End()
    {
        Btn.interactable = false;
        bool pl;

        for (int i = 0; i < Tools.Length; i++)
        {
            if (Tools[i].isNeeded) { continue; }

            Root.GetChild(i).GetComponent<Animation>().Stop();
            Root.GetChild(i).GetComponent<Animation>().Play("Tool_Out");
            for (int j = 0; j < Random.Range(5, 7); j++)
            {
                yield return null;
            }

        }

        while (true)
        {

            pl = false;
            for (int i = 0; i < Tools.Length; i++)
            { 
                if (Root.GetChild(i).GetComponent<Animation>().isPlaying)
                {
                    pl = true;
                }

            }

            yield return null;
            if (!pl)
            {
                break;
            }

        }


        for (int i = 0; i < Tools.Length; i++)
        {
            if (!Tools[i].isNeeded) { continue; }

            Root.GetChild(i).GetComponent<Animation>().Stop();
            Root.GetChild(i).GetComponent<Animation>().Play("Tool_Out");
           

        }

        while (true)
        {

            pl = false;
            for (int i = 0; i < Tools.Length; i++)
            {
                if (Root.GetChild(i).GetComponent<Animation>().isPlaying)
                {
                    pl = true;
                }

            }

            yield return null;
            if (!pl)
            {
                break;
            }

        }

        Main.SetActive(false);

        Game.SetActive(true);
        if(!(SceneManager.GetActiveScene().name == "WCCleaning"))
        {
            Items.SetActive(true);
        }

    }

    public IEnumerator Randomize()
    {
        Main.SetActive(true);
        List<GameObject> needed = ReferencesNeeded.ToList();

        for (int i = 0; i < ReferenceNotNeeded.Length; i++)
        {
            needed.Add(ReferenceNotNeeded[i]);
        }
        List<GameObject> ordered = new List<GameObject>();
        int l = Root.childCount;

        yield return null;

        for (int i = 0; i < l; i++)
        {
            Root.GetChild(i).GetComponent<Animation>().Stop();
            Root.GetChild(i).GetComponent<Animation>().Play("Tool_Out");
            for (int j = 0; j < Random.Range(5, 7); j++)
            {
                yield return null;
            }

        }
        bool pl;


        while (true)
        {

            pl = false;
            for (int i = 0; i < l; i++)
            {
                l = Root.childCount;
                if (Root.GetChild(i).GetComponent<Animation>().isPlaying)
                {
                    pl = true;
                }

            }

            yield return null;
            if (!pl)
            {
                break;
            }

        }


        for (int i = 0; i < l; i++)
        {
        
            Destroy(Root.GetChild(i).gameObject);
            l = Root.childCount;

        }




        yield return new WaitForSeconds(1.0f);

        int neededCount = needed.Count;
        int rnd;
        for (int i = 0; i < neededCount; i++)
        {
            rnd = Random.Range(0, needed.Count);
            ordered.Add(needed[rnd]);

            needed.RemoveAt(rnd);
            
        }

        yield return null;

        for (int i = 0; i < ordered.Count; i++)
        {
            GameObject gos = Instantiate(ordered[i]);

            gos.transform.SetParent(Root);
            gos.transform.localScale = Vector2.zero;   
        }
        yield return null;

        Tools = new CleaningTool[Root.childCount];

        GameObject go;
        for (int i = 0; i < Tools.Length; i++)
        {
            go = Root.GetChild(i).gameObject;

            Tools[i] = new CleaningTool(go.name.Remove(0, go.name.LastIndexOf("_") + 1), false, go.GetComponent<MonoTool>().IsUsed, go, i, this);

      
        }

        for (int i = 0; i < Tools.Length; i++)
        {
            Tools[i].SelectToggle(false);

        }

        l = Root.childCount;

        yield return null;

        for (int i = 0; i < l; i++)
        {
            Root.GetChild(i).GetComponent<Animation>().Stop();
            Root.GetChild(i).GetComponent<Animation>().Play("Tool_In");
            for (int j = 0; j < Random.Range(5, 7); j++)
            {
                yield return null;
            }

        }

        while (true)
        {

            pl = false;
            for (int i = 0; i < l; i++)
            {
                l = Root.childCount;
                if (Root.GetChild(i).GetComponent<Animation>().isPlaying)
                {
                    pl = true;
                }

            }

            yield return null;
            if (!pl)
            {
                break;
            }

        }

        yield return null;



        Btn.interactable = true;
    }


    public void Update()
    {
        if(Tools == null || Tools.Length <= 0) { return; }

        for (int i = 0; i < Tools.Length; i++)
        {
            Tools[i].Update();
        }

    }

    public void Select(bool b)
    {
        if(Tools == null) { return; }
        Tools[CurrentIndex].Select(b);
    }
}
