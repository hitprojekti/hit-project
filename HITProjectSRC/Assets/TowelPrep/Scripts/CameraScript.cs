﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {

    public float PosX;
    public float speed;

    Vector3 originalPos;

    GameObject Pot;

    void Start()
    {
        originalPos = transform.position;
        Pot = GameObject.Find("MeasuringPot");
    }

	void FixedUpdate()
    {
        if (!NewDragScript.LockMeasuringPot && Mathf.Abs(transform.position.x - Pot.transform.position.x) > 1)
        {
            PosX = Mathf.Clamp(Pot.transform.position.x, -3f, 0);
        }
        transform.position = Vector3.Lerp(transform.position, new Vector3(PosX, originalPos.y, originalPos.z), speed);
    }
}
