﻿public enum DragObjectEnum
{
    None = 0,
    CleaningAgent = 1,

    MeasuringPot = 2,
    MeasuringPot_2 = 4,

    Measure = MeasuringPot | MeasuringPot_2
   
}