﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragScript : MonoObject {

    public float MaxObjDist = 1.0f;                 //Maximum distance for object from this object
    public float MaxTouchDist = 1.0f;                 //Maximum distance for touch from this object
    public float MaxRotation = 75.0f;               //Maximum rotation for this object
    public float RotationMultiplier = 2.0f;         //Multiplier for rotation speed

    

    public float ParticleDistanceThresholdPercent = 1.0f;

    public DragObjectEnum ObjectType;               //Object's type

    public Transform pouringPoint;                    //Position for first pouring point

    public ParticleSystem System;


    private Vector3 originalPos;                    //Original position of this object
    private Vector3 offSet;                         //Offset between finger position and this object

    private UniversalTutorialScript Tutorial;

    private float _currentZRotation                 //This object's current z rotation
    {
        get
        {
            return _z;
        }

        set
        {
            _z = value;

            transform.rotation = Quaternion.Euler(0, 0, _z);
        }
    }

    private float _z;                               //Current rotation's private field

    private ObjectState _state
    {
        get
        {
            return _s;
        }

        set
        {
            _s = value;

            switch (_s)
            {
                case ObjectState.Pouring:
                    break;
                     default:
                    System.Stop();
                    break;
            }
        }
    }

    private ObjectState _s;

    private float _currentDistance;

    private MeasuringScript measuringScript;        //Reference to measuringscript

    private ScoreManagerMono scoreManager;          //Reference to scoremanager


    private bool _pressed;


    /// <summary>
    /// Initialize this component
    /// </summary>
    void Awake()
    {
        _pressed = false;
        scoreManager = GameObject.Find("_ScoreManager_").GetComponent<ScoreManagerMono>();
        measuringScript = GameObject.Find("GameManager").GetComponent<MeasuringScript>();
        Tutorial = GameObject.Find("TutorialButton").GetComponent<UniversalTutorialScript>();

        InitializeTransform();
        InitializeOtherVariables();
    }

    private void StartPS()
    {

        bool isMeasuring = ((ObjectType | DragObjectEnum.Measure) != 0);

        if(isMeasuring && measuringScript.TotalLiquid <= 0)
        {
            if (System.isPlaying)
            {
                System.Stop();
            }

            return;
        }

        if (_currentZRotation/ MaxRotation < ParticleDistanceThresholdPercent)
        {

            if (System.isPlaying)
            {
                System.Stop();
            }

            return;
        }
        if (isMeasuring)
        {
            ParticleSystem.MainModule main = System.main;
            main.startColor = measuringScript.GetWaterColor(new Color(0, 0.5f, 0.65f),  measuringScript.c);

        }
        if (System.isPlaying) { return; }
       
        System.Play();
    }

    /// <summary>
    /// Initializes this objects transform
    /// </summary>
    private void InitializeTransform()
    {
        originalPos = transform.position;
        ResetObjectRotation();
        ResetObjectPosition();

    }


    public void ResetObj()
    {
        ResetObjectPosition();
        ResetObjectRotation();

        _state = ObjectState.Idle;
    }

    /// <summary>
    /// Set default values for other variables
    /// </summary>
    private void InitializeOtherVariables()
    {
        _state = ObjectState.Idle;
    }

    private void GetTouch()
    {
        if (Input.GetMouseButtonUp(0))
        {
            if (measuringScript.CurrentObject != ObjectType && measuringScript.CurrentObject != DragObjectEnum.None) { return; }

            if (_state == ObjectState.Idle) { return; }


            _pressed = false;
         

            if(ObjectType == DragObjectEnum.MeasuringPot && measuringScript.TotalLiquid > 0) { return; }

                ResetObjectRotation();
                measuringScript.CurrentObject = DragObjectEnum.None;
                _state = ObjectState.Idle;
                ResetObjectPosition();

                CheckForReset();
            

        }

        if (measuringScript.CurrentObject != ObjectType && measuringScript.CurrentObject != DragObjectEnum.None) { return; }

        if (!Input.GetMouseButton(0)) { return; }


        if(Vector3.Distance(GetFingerPosition(), transform.position) > MaxTouchDist && !_pressed)
        {


            return;
        }

        if (Input.GetMouseButtonDown(0) && !_pressed)
        {
            _pressed = true;
        }

        switch (_state)
        {
            case ObjectState.Idle:

             
                offSet = GetFingerPosition() - transform.position;

                _state = ObjectState.Dragging;
                transform.position = GetFingerPosition() - offSet;

                break;

            case ObjectState.Dragging:

                measuringScript.CurrentObject = ObjectType;

                transform.position = GetFingerPosition() - offSet;

                break;

            case ObjectState.Pouring:
                if (Input.GetMouseButtonDown(0))
                {
                    offSet = GetFingerPosition() - transform.position;
                }

                measuringScript.CurrentObject = ObjectType;

                transform.position = GetFingerPosition() - offSet;

                break;
        }

    }

    private void Update()
    {
        if (!measuringScript.isDone)
        {
            if (Tutorial.inTutorial)
            {
                return;
            }
                GetDistance();

            GetTouch();     
        }
    }

    private void GetDistance()
    {

        _currentDistance = Vector2.Distance(pouringPoint.position, transform.position);


        if(_currentDistance > MaxObjDist)
        {
            if(_state == ObjectState.Pouring)
            {
                _state = ObjectState.Dragging;

                ResetObjectRotation();
            }
            measuringScript.PourAgent();
            return;
        }

        if (_state != ObjectState.Pouring)
        {

            _state = ObjectState.Pouring;
        }


        _currentZRotation = Mathf.Lerp(0, MaxRotation, (MaxObjDist - _currentDistance) * RotationMultiplier);

        StartPS();

        switch (ObjectType)
        {
            case DragObjectEnum.CleaningAgent:
                float f = _currentZRotation / (MaxRotation);

                f = Mathf.Log( (1+ParticleDistanceThresholdPercent) - f, ParticleDistanceThresholdPercent);

                f = Mathf.Clamp(f, 0, 1);

                measuringScript.PouringMultiplier = f;

                break;

            case DragObjectEnum.MeasuringPot:

                if (measuringScript.TotalLiquid <= 0) { break; }
                measuringScript.GameState = MinigameStateEnum.MidGame;
                measuringScript.PouringMultiplier = -(_currentZRotation / MaxRotation) * 2.5f;

                break;

            case DragObjectEnum.MeasuringPot_2:

                if(measuringScript.TotalLiquid <= 0) { break; }
                measuringScript.GameState = MinigameStateEnum.Start;

                measuringScript.PouringMultiplier = -(_currentZRotation / MaxRotation) * 1.5f;
                measuringScript.score -= 50 * measuringScript.PouringMultiplier + 1;
                measuringScript.SavedValue = measuringScript.TotalLiquid;
             
                break;
        }


        measuringScript.PourAgent();
    }

    /// <summary>
    /// Returns fingerposition in worldspace 
    /// </summary>
    /// <returns></returns>
    private Vector3 GetFingerPosition()
    {
        Vector3 tempPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        tempPos.z = 0;

        return tempPos;
    }

    public void CheckForReset()
    {
        bool isMeasuring = ((ObjectType | DragObjectEnum.Measure) != 0);

        if (ObjectType != DragObjectEnum.MeasuringPot || measuringScript.GameState == MinigameStateEnum.Start) { return; }

        if(measuringScript.TotalLiquid > 0)
        {
            return;
        }

        if(_state != ObjectState.Idle) { return; }

        measuringScript.EndGame();
    }

    /// <summary>
    /// Resets object values
    /// </summary>
    private void ResetObjectRotation()
    {
        _currentZRotation = 0;
        measuringScript.PouringMultiplier = 0;
    }

    /// <summary>
    /// Resets object values
    /// </summary>
    private void ResetObjectPosition()
    {
        transform.position = originalPos;
    }

}
