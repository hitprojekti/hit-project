﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MeasuringScript : MonoBehaviour {

    public float Water
    {
        get
        {
            return _water;
        }

        set
        {
            _water = value;

            waterTxt.text = "Water: " + Water.ToString("F1") + "l";
        }
    }

    private float _water;
    public float CleaningAgent
    {
        get
        {
            return _cleanF;
        }

        set
        {
            _cleanF = value;
            if(SavedValue < _cleanF)
            {
                SavedValue = _cleanF;
            }
            waterTr.GetComponent<SpriteRenderer>().color = GetWaterColor(Color.white, c);
        }
    }

    public Color c;
    private float _cleanF;
    public float PourSpeed = 1;
    public float RatioToLitre;
    public float MaxVariance = 0.5f;
    public float PourDist = 0.5f;
    public float PouringMultiplier = 0;

    public MinigameStateEnum GameState;

    public int CurrentIndex;

    public int MaxIndexes = 5;

    public DragObjectEnum CurrentObject;

    public float score;
    float maxScore = 10000;
    float targetAgentAmount;

    public DragScript[] Draggables;

    public float TotalLiquid
    {
        get
        {

            float f = (CleaningAgent);

            return f;
        }
    }

    public float SavedValue;

    public ProgressItemMono[] Progress;

    public GameObject ReferenceProgress;

    public Transform Parent;

    Transform waterTr;

    Text instructionsTxt;
    Text agentTxt;
    Text waterTxt;
    Text scoreTxt;

    AnimationScript animationScript;

    ScoreManagerMono manager;
    Manager mngr;
    UIScript uiScript;
    public GameObject SummaryScreen;
    //public float RngMax = 10;
    public float[] ScoreArray;
    public bool isDone;
	// Use this for initialization
	void Start () {
        Parent.GetComponent<ContentSizeFitter>().enabled = true;

        CreateProgress();
        manager = GameObject.Find("_ScoreManager_").GetComponent<ScoreManagerMono>();
        mngr = GameObject.Find("GameManager").GetComponent<Manager>();
        uiScript = GameObject.Find("GameManager").GetComponent<UIScript>();
        instructionsTxt = GameObject.Find("Instructions").GetComponent<Text>();
        agentTxt = GameObject.Find("Agent").GetComponent<Text>();
        waterTxt = GameObject.Find("Water").GetComponent<Text>();
        scoreTxt = GameObject.Find("Score Amount_Pause").GetComponent<Text>();
        waterTr = GameObject.Find("WaterSprite").GetComponent<Transform>();
        animationScript = GetComponent<AnimationScript>();
        agentTxt.text = "Cleaning agent: 0ml";
        FullReset();
        ScoreArray = new float[MaxIndexes];
        SummaryScreen.SetActive(false);
        isDone = false;
    }

    public void FullReset()
    {
        ResetScore();
        ResetProgress();
        CurrentIndex = 1;
        Reset();
    }

    public void CreateProgress()
    {
        Progress = new ProgressItemMono[MaxIndexes];

        for (int i = 0; i < MaxIndexes; i++)
        {

            GameObject go = Instantiate(ReferenceProgress, Parent.position, Quaternion.identity);

            go.transform.SetParent(Parent);
            go.transform.localScale = Vector3.one;
            Progress[i] = go.GetComponent<ProgressItemMono>();
        }

        ResetProgress();
    }

    public void IsDone(bool tru)
    {
        if (tru)
        {
            Progress[CurrentIndex-1].State = ProgressStateEnum.Success;
            return;
        }

        Progress[CurrentIndex-1].State = ProgressStateEnum.Fail;
    }

    public void ResetProgress()
    {
        for (int i = 0; i < Progress.Length; i++)
        {
            Progress[i].State = ProgressStateEnum.Empty;
        }
    }

    public Color GetWaterColor(Color cBase, Color cc = default(Color))
    {
        return cc;
    }

    public void EndGame()
    {
        float ff = 0;
        float value = Water * RatioToLitre;

        if(SavedValue < value)
        {
            //ff = SavedValue / value;
            ff = value - SavedValue;
        }else
        {
            //ff = (value / SavedValue);
            ff = SavedValue - value;
        }


        float f = AddScore(ff);
        Debug.Log(f);
        IsDone(IsScoreTooLow(f));
        uiScript.PlayScoreAnimation(f.ToString());
        Reset();
        CurrentIndex++;
        if ((CurrentIndex) > MaxIndexes) { CloseGame(false);}
    }

    public void CloseGame(bool instant)
    {
        isDone = true;
        Time.timeScale = 1;
        if(CurrentIndex+1 < MaxIndexes) { score = 0; }

        manager.GiveScore(Mathf.RoundToInt(score), MiniGameEnum.TOWEL_PREP);

        if (instant)
        {
            ChangeScene();
            return;
        }
        CancelInvoke("OpenSummary");
        Invoke("OpenSummary", 2.0f);
        /*CancelInvoke("ChangeScene");
        Invoke("ChangeScene", 0.5f);*/
    }

    void ChangeScene()
    {
        SceneManagerHit.ChangeScene(0);
    }

    public bool IsScoreTooLow(float f)
    {
        if(/*f <= 0.775f*/ f <= 0)
        {
            return false;           
        }
        return true;
    }

	public void PourAgent()
    {
        //CleaningAgent = Mathf.Clamp(0, 100, CleaningAgent + (PourSpeed * Time.deltaTime * PouringMultiplier));

      

   
        CleaningAgent += PourSpeed * Time.deltaTime * PouringMultiplier;
        CleaningAgent = Mathf.Clamp(CleaningAgent, 0, 5000);

        animationScript.CalculateAmount(); 

        agentTxt.text = "Cleaning agent: " + CleaningAgent.ToString("F1") + "ml";
    }

    public void UpdateWater()
    {
        waterTxt.text = "Water: " + Water + "l";
    }

    public void ResetScore()
    {
        score = 0;
    }

    public float AddScore(float multiplier = 1)
    {

        /*multiplier = Mathf.Clamp(multiplier, 0, 100000);

        float f = Mathf.Clamp((1010 * multiplier), 0, 1000);
        ScoreArray[CurrentIndex - 1] = f;
        Debug.Log(ScoreArray[CurrentIndex - 1]);
        score += f;*/
        float f;
        if (multiplier > 1)
        {
            f = 0;
        }
        else
        {
            float l = Mathf.Lerp(0, 1, multiplier);
            f = 1050f * Mathf.Abs(l - 1);
        }
        f = Mathf.Clamp(f, 0, 1000);
        f = Mathf.Round(f);
        ScoreArray[CurrentIndex - 1] = f;
        
        score += f;
        return f;
        //score = Mathf.Clamp(score, 0, 10000);
        //return multiplier;
    }

    public void Reset()
    {
        for (int i = 0; i < Draggables.Length; i++)
        {
            Draggables[i].ResetObj();
        }

        SavedValue = 0;

        GameState = MinigameStateEnum.Start;
        CurrentObject = DragObjectEnum.None;
     
        scoreTxt.text = "Score: " + score.ToString("F0");

        Water = Mathf.Round(Random.Range(1, 7));
       // Debug.Log("Water: " + Water);
        RatioToLitre = Mathf.Round(Random.Range(2, 7) * 2) / 4;
       // Debug.Log("Ratio: " + RatioToLitre);
        targetAgentAmount = RatioToLitre * Water;
       // Debug.Log("Target amount: " + targetAgentAmount);
        CleaningAgent = 0;
        //Debug.Log(CleaningAgent);
        agentTxt.text = "Cleaning agent: " + CleaningAgent.ToString("F1") + "ml";

        animationScript.Reset();
        //waterTr.localScale = new Vector2(0.8f, Water / 10);



        instructionsTxt.text = RatioToLitre + "ml/ 1l";
   
    }

    void OpenSummary()
    {
        mngr.Pause.Play("PauseMenuOutInstant_Empty", true);
        uiScript.SummaryScreen(ScoreArray);
        SummaryScreen.SetActive(true);
    }
}
