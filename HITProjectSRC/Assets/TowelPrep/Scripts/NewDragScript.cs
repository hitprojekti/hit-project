﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewDragScript : MonoBehaviour
{

    public bool useTouchscreen;
    public float MaxDistTouch = 1;
    public float MaxDistTarget;
    public float MaxRot = 75;
    public float Multiplier = 1;

    float z;

    bool canDrag;
    public static bool LockMeasuringPot;

    Vector2 targetPos;
    Vector2 offset;
    Vector2 originalPos;

    Touch touch;

    public Vector2 pouringPoint;
    public Vector2 pouringPoint2;

    MeasuringScript measuringScript;

    AnimationScript animationScript;

    // Use this for initialization
    void Start()
    {
        measuringScript = GameObject.Find("GameManager").GetComponent<MeasuringScript>();
        animationScript = GameObject.Find("GameManager").GetComponent<AnimationScript>();
        originalPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        GetTouch();
        GetDistance();
    }

    void GetTouch()
    {
            
            if (Input.touchCount != 1 && canDrag)
            {
                if (!LockMeasuringPot)
                {
                    transform.position = originalPos;
                }
                canDrag = false;
            
                ResetRot();
            }
            else if (!canDrag && Input.touchCount == 1)
            {
                touch = Input.GetTouch(0);

                if (Vector2.Distance(Camera.main.ScreenToWorldPoint(touch.position), transform.position) <= MaxDistTouch)
                {
                    canDrag = true;
                
                    offset = Camera.main.ScreenToWorldPoint(touch.position) - transform.position;
                    StartCoroutine("Drag");
                }
            }
     
    }

    void GetDistance()
    {
        float dist = Vector2.Distance(pouringPoint, transform.position);
        float dist2 = Vector2.Distance(pouringPoint2, transform.position);

        //Rotate object if close enough
        if (dist <= MaxDistTarget)
        {
            z = Mathf.Lerp(0, MaxRot, (MaxDistTarget - dist) * Multiplier);
            transform.rotation = Quaternion.Euler(0, 0, z);
            if (z >= MaxRot)
            {
                if(name == "CleaningAgent")
                {
                    measuringScript.PouringMultiplier = 1;
                }
                else
                {
                    measuringScript.PouringMultiplier = -1;
                }
            }
            else
            {
                measuringScript.PouringMultiplier = 0;
            }
        }
        else if(dist2 <= MaxDistTarget && name == "MeasuringPot")
        {
            z = Mathf.Lerp(0, MaxRot, (MaxDistTarget - dist2) * Multiplier);
            transform.rotation = Quaternion.Euler(0, 0, z);

            if (z >= MaxRot && measuringScript.CleaningAgent > 0)
            {
                if(!LockMeasuringPot)
                {
                    LockMeasuringPot = true;
                }
                measuringScript.PouringMultiplier = -1;
            }
            else if (z >= MaxRot && measuringScript.CleaningAgent == 0)
            {
                LockMeasuringPot = false;
                measuringScript.PouringMultiplier = 0;
            }

        }
        else if(z != 0)
        {
            ResetRot();
        }
    }

    void ResetRot()
    {
        z = 0;
        transform.rotation = Quaternion.Euler(0, 0, z);
        measuringScript.PouringMultiplier = 0;
    }

    IEnumerator Drag()
    {
        while (Input.touchCount == 1)
        {
            touch = Input.GetTouch(Input.touchCount - 1);
            targetPos = Camera.main.ScreenToWorldPoint(touch.position);
            transform.position = targetPos - offset;
            yield return null;
        }
    }
}
