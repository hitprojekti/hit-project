﻿public enum ObjectState
{
    Idle,
    Dragging,
    Pouring
}