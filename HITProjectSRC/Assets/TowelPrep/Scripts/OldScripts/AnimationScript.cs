﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimationScript : MonoBehaviour {

    MeasuringScript measuringScript;

    float tempWater;

    public bool canMoveCam;

    Vector2 waterScale;

    Transform waterTr;

    public Transform cam;

	// Use this for initialization
	void Awake () {
        measuringScript = GetComponent<MeasuringScript>();
        waterTr = GameObject.Find("WaterSprite").GetComponent<Transform>();
        cam = GameObject.Find("Main Camera").GetComponent<Transform>();
	}

    public void Reset()
    {
        CalculateAmount();
        canMoveCam = true;
    }

    public void CalculateAmount()
    {
        tempWater = (measuringScript.CleaningAgent/1.6f);
        waterScale = new Vector2(0.8f, tempWater / 5);
        waterTr.localScale = waterScale;
    }

    public void Spill()
    {
        if(tempWater > 0 && measuringScript.CleaningAgent > 0)
        {
            tempWater -= 2 * Time.deltaTime;
        }
        else
        {
       
            PlayAnimation("Towel", "TowelAnimation");
            StartCoroutine(MoveCamera(new Vector3(0, 0, -10), 4f, true));
        }
        waterScale.y = tempWater / 10;
        waterTr.localScale = waterScale;
        measuringScript.Water = Mathf.Round((waterScale.y * 10) * 10) / 10;
        measuringScript.UpdateWater();
    }

    public void PlayAnimation(string objName, string anim)
    {
        GameObject.Find(objName).GetComponent<Animation>().Play(anim);
    }

    public IEnumerator MoveCamera(Vector3 pos, float delay = 0, bool reset = false)
    {
        Vector3 startPos = cam.position;
        float t = 0;

        yield return new WaitForSeconds(delay);

        while (t < 1)
        {
            t += 2 * Time.deltaTime;
            cam.position = Vector3.Lerp(startPos, pos, t);
            yield return null;
        }

        if (reset)
        {
            measuringScript.Reset();
        }
    }
}
