﻿using UnityEngine;

[System.Serializable]
public class PouringConfiguration
{
    public Transform pouringPoint;

    public float MaxObjDist = 1.0f;                 //Maximum distance for object from this object
    public float MaxTouchDist = 1.0f;                 //Maximum distance for touch from this object
    public float MaxRotation = 75.0f;               //Maximum rotation for this object
    public float RotationMultiplier = 2.0f;         //Multiplier for rotation speed

    public float ParticleDistanceThresholdPercent = 1.0f;

    private ObjectState _s;

    public float _currentDistance;

    public ParticleSystem System;

    public ObjectState _state
    {
        get
        {
            return _s;
        }

        set
        {
            _s = value;

            switch (_s)
            {
                case ObjectState.Pouring:
                    break;
                default:
                    System.Stop();
                    break;
            }
        }
    }
}