﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowelTutorialScript : MonoBehaviour {

    public bool inTutorial;
    void Start()
    {
        inTutorial = true;
    }

    public void Click()
    {
        gameObject.SetActive(false);
        inTutorial = false;
        Debug.Log("clicked");
    }
}
