﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIScript : MonoBehaviour {

    bool isPauseMenuOpen;

    float faderOpacity;

    Image fader;

    Color faderClr;

    GameObject pauseButton;

    MeasuringScript measuringScript;

    AnimationScript animationScript;

    UnscaledTimeAnimator unscaled;

    public Text SummaryText1;
    public Text SummaryText2;
    public Text ScoreText;
    public Animation ScoreAnim;

    private void Awake()
    {
        unscaled = GameObject.Find("PauseMenu").GetComponent<UnscaledTimeAnimator>();
        PlayAnimation("PauseMenuOutInstant", true, true);
    }
    // Use this for initialization
    void Start () {

       
        measuringScript = GetComponent<MeasuringScript>();
        animationScript = GetComponent<AnimationScript>();
   
        fader = GameObject.Find("Fader").GetComponent<Image>();
        faderClr = fader.color;
    }

    public void PauseButton()
    {
        isPauseMenuOpen = true;

        PlayAnimation("PauseMenuIn", false);
        Time.timeScale = 0;
       
  
    
    }

    public void Resets()
    {
        Time.timeScale = 1;
        SceneManagerHit.ChangeScene(0);
    }

    public void ContinueButton()
    {
        isPauseMenuOpen = false;

        Time.timeScale = 1;
        PlayAnimation("PauseMenuOut_Anim", false);
     

    }

    public void RestartButton()
    {
        measuringScript.FullReset();
        animationScript.cam.position = new Vector3(0, 0, -10);
        ContinueButton();   
    }

    public void PlayAnimation(string animName, bool useTimescale = true, bool instant = false)
    {
      unscaled.Play(animName, useTimescale, instant);
    }

    public void SummaryScreen(float[] scores)
    {
        SummaryText1.text = string.Format("1st\n2nd\n3rd\n4th\n5th\n\nTotal");

        SummaryText2.text = string.Format("{0}\n{1}\n{2}\n{3}\n{4}\n\n{5}", Mathf.RoundToInt(scores[0]), Mathf.RoundToInt(scores[1]), Mathf.RoundToInt(scores[2]), Mathf.RoundToInt(scores[3]), Mathf.RoundToInt(scores[4]), Mathf.RoundToInt(scores.Sum()));
    }

    public void PlayScoreAnimation(string score)
    {
        Vector2 vec = Input.mousePosition + new Vector3(0, 100, 0);
        ScoreText.text = score;
        transform.position = vec;
        ScoreAnim.Stop();
        ScoreAnim.Play("ScoreText");
    }

}
