﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragManager : MonoBehaviour {

    public bool isDragging;
    public bool isMouseDown;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        GetMouseInput();
	}

    void GetMouseInput()
    {
        if(Input.GetMouseButtonDown(0) && !isMouseDown)
        {
            isMouseDown = true;
        }
        else if(Input.GetMouseButtonUp(0) && isMouseDown)
        {
            isMouseDown = false;
        }
    }
}
