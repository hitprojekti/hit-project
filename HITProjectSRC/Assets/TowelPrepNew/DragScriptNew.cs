﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragScriptNew : MonoBehaviour {

    public float grabDist, maxPourDist;
    float pourDist = 1;
    float dist;
    public float maxRot = 140;

    public bool isActive;

    Vector3 offset;
    Vector3 rot;

    public Vector2 pouringPos;
    public Vector2 pouringPos2;
    Vector2 originalPos;

    DragManager dragManager;
    MeasuringScriptNew measuringScript;

	// Use this for initialization
	void Start () {
        dragManager = GameObject.Find("GameManager").GetComponent<DragManager>();
        measuringScript = GameObject.Find("GameManager").GetComponent<MeasuringScriptNew>();
        originalPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        Move();
        Rotate();
	}

    void Move()
    {
        //Do nothing if mouse isn't pressed or finger isn't touching screen
        if (!dragManager.isMouseDown)
        {
            if (isActive)
            {
                isActive = false;
                dragManager.isDragging = false;
                transform.position = originalPos;
                transform.rotation = Quaternion.Euler(Vector3.zero);    
            }
            return;
        }
        //Change position if distance between cursor and this object is small enough or object is being dragged
        if(GetDistance() <= grabDist && !isActive && !dragManager.isDragging)
        {
            Debug.Log("Get offset");
            isActive = true;
            dragManager.isDragging = true;
            offset = transform.position - Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }

        if (isActive)
        {
            transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition) + offset;
        }
    }

    float GetDistance()
    {
        return Vector2.Distance(transform.position, Camera.main.ScreenToWorldPoint(Input.mousePosition));
    }

    void Rotate()
    {
        if (!isActive) { return; }
        dist = Vector2.Distance(transform.position, pouringPos);
        rot.z = maxRot * Mathf.Clamp((1 - dist / maxPourDist), 0, 1);
        transform.rotation = Quaternion.Euler(rot);
    }
}
