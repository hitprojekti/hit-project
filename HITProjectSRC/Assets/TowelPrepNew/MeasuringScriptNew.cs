﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MeasuringScriptNew : MonoBehaviour
{

    public float pourSpeed = 1;
    public float rotationSpeed = 1;
    public float moveSpeed = 1;
    public float sinMultiplier = 4;
    float cleaningAgent;
    float tempAgent;
    float water;
    float ratio;
    public float[] score;

    int gameNumber;
    int maxGames = 5;

    bool canPlay;
    bool isBucketActive;

    public Text ratioTxt;
    public Text waterTxt;
    public Text cleaningAgentTxt;
    public Text summaryText1;
    public Text summaryText2;

    public GameObject bottle;
    public GameObject pot;
    public GameObject waterSprite;
    public GameObject checkMark;
    public GameObject xMark;
    public GameObject summaryScreen;

    public Vector3 sinkPos;
    public Vector3 bucketPos;
    public Vector3 markPos;
    Vector3 bottleRot;
    Vector3 potRot;
    Vector3 potPos;
    Vector3 originalPotPos;
    Vector2 bottleScale = new Vector2(1, 1);
    Vector2 waterScale = new Vector2(0.8f, 0);

    ScoreManagerMono scoreManager;

    //Reference velocities
    Vector3 vel1;
    Vector3 vel3;
    Vector3 vel2;

    // Use this for initialization
    void Start()
    {
        scoreManager = GameObject.Find("_ScoreManager_").GetComponent<ScoreManagerMono>();
        gameNumber = 0;
        originalPotPos = pot.transform.position;
        potPos = originalPotPos;
        canPlay = true;
        SetUpGame();
    }

    void SetUpGame()
    {
        Debug.Log("Setup game");
        ratio = (float)Random.Range(1, 10) / 2;
        water = Mathf.Round(Random.Range(1, Mathf.Clamp(5 - ratio, 1, 3)));
        gameNumber++;
    }

    // Update is called once per frame
    void Update()
    {
        SetRotAndPos();
        if (!canPlay) { return; }
        Pour();
        ChangeSize();
        UpdateUI();
    }

    void ChangeSize()
    {
        if(cleaningAgent < 0.5f)
        {
            bottleScale.x = 1 - (Mathf.Sin(Time.time * sinMultiplier) * 0.15f);
            bottleScale.y = 1 - (Mathf.Sin(Time.time * sinMultiplier) * 0.15f);
        }
        else
        {
            bottleScale.x = 1;
            bottleScale.y = 1;
        }
        bottle.transform.localScale = bottleScale;
    }

    void Pour()
    {
        cleaningAgent = Mathf.Clamp(cleaningAgent += pourSpeed * Time.deltaTime, 0, Mathf.Infinity);
        waterScale.y = Mathf.Clamp(cleaningAgent / 15, 0, 1);
        waterSprite.transform.localScale = waterScale;
    }

    void SetRotAndPos()
    {
        if (!isBucketActive)
        {
            if (pourSpeed > 0)
            {
                bottleRot.z = 140;
                potPos = originalPotPos;
                potRot.z = 0;
            }
            else if (pourSpeed == 0)
            {
                bottleRot.z = 0;
                potPos = originalPotPos;
                potRot.z = 0;
            }
            else if (pourSpeed < 0)
            {
                bottleRot.z = 0;
                potPos = sinkPos;
                potRot.z = 100;
            }
        }
        else
        {
            if(cleaningAgent > 0)
            {
                bottleRot.z = 0;
                potPos = bucketPos;
                potRot.z = 100;
            }
            else
            {
                bottleRot.z = 0;
                potPos = originalPotPos;
                potRot.z = 0;

                if(cleaningAgent == 0)
                {
                    isBucketActive = false;
                    ChangePourSpeed(0);
                    CalculateScore();
                    if (gameNumber < maxGames)
                    {
                        SetUpGame();
                    }
                    else
                    {
                        canPlay = false;
                        summaryScreen.SetActive(true);
                        int finalScore = (int)score.Sum();
                        summaryText1.text = string.Format("1st\n2nd\n3rd\n4th\n5th\n\nTotal");
                        summaryText2.text = string.Format("{0}\n{1}\n{2}\n{3}\n{4}\n\n{5}", score[0], score[1], score[2], score[3], score[4], finalScore);

                        scoreManager.GiveScore(finalScore, MiniGameEnum.TOWEL_PREP);

                        Debug.Log("Load endscreen");
                    }
                }
            }
        }
        
        bottle.transform.rotation = Quaternion.Euler(Vector3.SmoothDamp(bottle.transform.eulerAngles, bottleRot, ref vel1, rotationSpeed * Time.deltaTime));
        pot.transform.rotation = Quaternion.Euler(Vector3.SmoothDamp(pot.transform.eulerAngles, potRot, ref vel2    , rotationSpeed * Time.deltaTime));
        pot.transform.position = Vector3.SmoothDamp(pot.transform.position, potPos, ref vel3, moveSpeed * Time.deltaTime);
    }

    void CalculateScore()
    {
        //Haha yes :-D
        float dist = Mathf.Abs(tempAgent - (ratio * water));
        Debug.Log("Game no. " + gameNumber + ", Water: " + water + ", ratio: " + ratio + ", cleaning agent: " + tempAgent + ", Dist: " + dist);

        if(dist == 0)
        {
            score[gameNumber - 1] = 1000;
            Instantiate(checkMark, markPos, Quaternion.identity);
        }
        else
        {
            score[gameNumber - 1] = 0;
            Instantiate(xMark, markPos, Quaternion.identity);
        }

        Debug.Log("Score " + score[gameNumber - 1]);
    }

    public void ChangePourSpeed(float _pourSpeed)
    {
        if (!isBucketActive && canPlay)
        {
            pourSpeed = _pourSpeed;
        }
    }

    public void ActivateBucket()
    {
        if(cleaningAgent >= 0.5f)
        {
            tempAgent = Mathf.Round(cleaningAgent) / 2;
            ChangePourSpeed(-4);
            isBucketActive = true;
        }
    }

    void UpdateUI()
    {
        ratioTxt.text = ratio + "ml/l";
        waterTxt.text = water + "l";
        cleaningAgentTxt.text = Mathf.Round(cleaningAgent) / 2 + "ml";  
    }
}