﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseToolsScript : MonoBehaviour
{

    public Tool[] tools;
    public GameObject selectedIndicator;
    public GameObject[] inds;

    public ChooseToolsScript()
    {
        tools = new Tool[]
        {
            new Tool("tool1",false,false),
            new Tool("tool2",false,true),
            new Tool("tool3",false,true),
            new Tool("tool4",false,false),
            new Tool("tool5",false,false),
            new Tool("tool6",false,false),
            new Tool("tool7",false,true),
            new Tool("tool8",false,false),
            new Tool("tool9",false,false),
            new Tool("tool10",false,true),
            new Tool("tool11",false,true),
            new Tool("tool12",false,true),
        };

        inds = new GameObject[tools.Length];
    }

    public void Toggle(int index, Transform parent)
    {
        tools[index].isSelected = !tools[index].isSelected;

        if (tools[index].isSelected)
        {
            if (inds[index] == null)
            {
                inds[index] = Instantiate(selectedIndicator, parent.position, Quaternion.identity, parent);
            }
            else
            {
                inds[index].SetActive(true);
            }
        }
        else
        {
            inds[index].SetActive(false);
        }
    }

    public bool CheckSelected(int index)
    {
        bool isCorrect = false;

        if(tools[index].isNeeded == tools[index].isSelected)
        {
            isCorrect = true;
        }
        else
        {
            isCorrect = false;
        }

        return isCorrect;
    }
}
