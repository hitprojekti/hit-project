﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextPhaseScript : MonoBehaviour {

    GameObject ToolPhase, OverviewPhase;
    public ChooseToolsScript CTScript;

    void Start()
    {
        ToolPhase = GameObject.Find("ChooseToolsPhase");
        CTScript = ToolPhase.GetComponent<ChooseToolsScript>();
        OverviewPhase = GameObject.Find("OverviewPhase");
        OverviewPhase.SetActive(false);
    }

    private void OnMouseDown()
    {
        bool isPassed = true;
        for (int i = 0; i < CTScript.tools.Length; i++)
        {
            if(!CTScript.CheckSelected(i))
            {
                isPassed = false;
            }
        }
        if (isPassed)
        {
            Continue();
        }
        else
        {
            Retry();
        }
    }

    void Continue()
    {
        ToolPhase.SetActive(false);
        OverviewPhase.SetActive(true);
    }

    void Retry()
    {
        for (int i = 0; i < CTScript.inds.Length; i++)
        {
            if(CTScript.inds[i] != null)
            {
                CTScript.inds[i].SetActive(false);
                CTScript.tools[i].isSelected = false;
            }
        }
    }
}
