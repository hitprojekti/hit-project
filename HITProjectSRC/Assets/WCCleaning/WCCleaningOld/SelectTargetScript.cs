﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectTargetScript : MonoBehaviour
{

    public GameObject Zoom, Overview, Frame;
    public int StageIndex;
    private void OnMouseDown()
    {
        Overview.SetActive(false);
        Zoom.SetActive(true);
        Frame.SetActive(true);
        GameObject.Find("ZoomedPhase").GetComponent<WCCleaningScript>().RefreshTools(StageIndex);
    }
}
