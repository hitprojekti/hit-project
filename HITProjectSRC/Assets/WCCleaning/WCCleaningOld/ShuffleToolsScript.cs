﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShuffleToolsScript : MonoBehaviour {

    public GameObject[] tools = new GameObject[12];
    public Vector2[] positions;

    public ShuffleToolsScript()
    {
        positions = new Vector2[]
        {
            new Vector2(-2, 4.2f),
            new Vector2(0, 4.2f),
            new Vector2(2, 4.2f),
            new Vector2(-2, 2.2f),
            new Vector2(0, 2.2f),
            new Vector2(2, 2.2f),
            new Vector2(-2, .2f),
            new Vector2(0, .2f),
            new Vector2(2, .2f),
            new Vector2(-2, -1.8f),
            new Vector2(0, -1.8f),
            new Vector2(2, -1.8f)
        };
    }

    private void Start()
    {
        Shuffle(positions);
    }

    void Shuffle(Vector2[] p)
    {

        for (int i = p.Length - 1; i > 0; i--)
        {
            int rnd = Random.Range(0, i);
            Vector2 temp = p[i];
            p[i] = p[rnd];
            p[rnd] = temp;

            
        }
        for (int i = 0; i < tools.Length; i++)
        {
            tools[i].transform.position = positions[i];
        }
        
    }
}
