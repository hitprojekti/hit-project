﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleScript : MonoBehaviour {

    public ChooseToolsScript CTScript;
    public int index;

    public void Start()
    {
        CTScript = transform.parent.GetComponent<ChooseToolsScript>();
    }

    private void OnMouseDown()
    {
        CTScript.Toggle(index, this.transform);
    }
}
