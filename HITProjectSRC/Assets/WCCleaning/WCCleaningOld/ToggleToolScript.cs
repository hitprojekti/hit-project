﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleToolScript : MonoBehaviour {

    public WCCleaningScript WCCScript;
    public int index;

    public void Start()
    {
        WCCScript = GameObject.Find("ZoomedPhase").GetComponent<WCCleaningScript>();
    }

    private void Update()
    {
        this.GetComponent<SpriteRenderer>().sprite = WCCScript.tools[index].toolSprite;
    }

    private void OnMouseDown()
    {
       
    }
}
