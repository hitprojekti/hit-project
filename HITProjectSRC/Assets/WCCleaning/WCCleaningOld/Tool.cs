﻿public class Tool
{
    public string toolName;
    public bool isSelected;
    public bool isNeeded;

    public Tool(string name, bool selected, bool needed)
    {
        toolName = name;
        isSelected = selected;
        isNeeded = needed;
    }
}