﻿public class Tool2
{
    public string toolName;
    public UnityEngine.Sprite toolSprite;
    public bool isSelected;
    public int orderIndex;

    public Tool2(string name, UnityEngine.Sprite sprite, bool selected, int index)
    {
        toolName = name;
        toolSprite = sprite;
        isSelected = selected;
        orderIndex = index;
    }
}