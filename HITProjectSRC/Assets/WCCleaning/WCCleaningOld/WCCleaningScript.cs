﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WCCleaningScript : MonoBehaviour
{

    public Tool2[] tools;
    public Sprite[] toolSprites;

    public WCCleaningScript()
    {
        toolSprites = new Sprite[6];
    }

    void SelectFromToolbar()
    {

    }

    public void RefreshTools(int index)
    {
        switch (index)
        {
            case 1 :
                tools = new Tool2[]
                    {
                        new Tool2("tool1", toolSprites[1], false, 1),
                        new Tool2("tool2", toolSprites[2], false, 2),
                        new Tool2("tool3", toolSprites[3], false, 3),
                        new Tool2("tool4", toolSprites[4], false, 4),
                        new Tool2("tool5", toolSprites[5], false, 5)
                    };
                break;
            case 2 :
                tools = new Tool2[]
                    {
                        new Tool2("tool6", toolSprites[6], false, 1),
                        new Tool2("tool5", toolSprites[5], false, 2),
                        new Tool2("tool4", toolSprites[4], false, 3),
                        new Tool2("tool3", toolSprites[3], false, 4),
                        new Tool2("tool2", toolSprites[2], false, 5)
                    };
                break;
        }     
    }
}