﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ZoomScript : MonoBehaviour {

    public GameObject Overview, SummaryScrn;
    public GameObject[] Zoomables, Items, Targets;
    public int totalIndex = 0, totalScore, index;
    public int[] Scores;
    public float[] ScoreMultipliers =
    {
        1,
        1
    };

    public Text SummaryText;

    public ScoreManagerMono ScoreM;

    public void Zoom(int ind)
    {
        index = ind;
        if(ind == totalIndex)
        {
            Overview.SetActive(false);
            Zoomables[ind].SetActive(true);
            Items[ind].SetActive(true);
            
        }
        else
        {
            ScoreMultipliers[totalIndex] = ScoreMultipliers[totalIndex] * 0.9f;          
            StopCoroutine("Wait");
            StartCoroutine("Wait");
            
        }
    }

    public void ReturnToOverview(int score)
    {
        Zoomables[totalIndex].SetActive(false);
        Items[totalIndex].SetActive(false);
        Targets[totalIndex].GetComponent<BoxCollider2D>().enabled = false;
        Overview.SetActive(true);
        Scores[totalIndex] = score;
        ChangeColor(new Color(0, 1, 0, .5f), totalIndex);
        totalIndex++;
        if (totalIndex >= Zoomables.Length)
        {
            ScoreM.GiveScore(Scores.Sum(), MiniGameEnum.TOILET_CLEANING);
            Summary();
            SummaryScrn.SetActive(true);
        }
    }

    public void Summary()
    {
        SummaryText.text = string.Format("\n{0}\n{1}\n{2}\n{3}\n", (int)(Scores[0] * ScoreMultipliers[0]), (int)(Scores[1] * ScoreMultipliers[1]), (int)(Scores[2] * ScoreMultipliers[2]), Scores.Sum());
    }

    public void ChangeColor(Color c, int i)
    {
        SpriteRenderer glow = Targets[i].GetComponentsInChildren<SpriteRenderer>()[1];
        glow.color = c;
    }

    IEnumerator Wait()
    {
        
            ChangeColor(new Color(1, 0, 0, .5f), index);
            yield return new WaitForSeconds(.25f);
            ChangeColor(new Color(1, 1, 0, .5f), index);
        
    }
}
